﻿namespace Game
{
    partial class ArendaSam
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lb_id = new System.Windows.Forms.Label();
            this.lb_nameSam = new System.Windows.Forms.Label();
            this.lb_typeSam = new System.Windows.Forms.Label();
            this.lb_probegSam = new System.Windows.Forms.Label();
            this.lb_arendaSam = new System.Windows.Forms.Label();
            this.lb_stoimostSam = new System.Windows.Forms.Label();
            this.btn_buy = new System.Windows.Forms.Button();
            this.btn_exit = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.lb_AllCost = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Идентификатор";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Самолет";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(12, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Тип";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(12, 109);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Пробег";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(12, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(124, 16);
            this.label5.TabIndex = 4;
            this.label5.Text = "Срок аренды(мес)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(12, 162);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(106, 16);
            this.label6.TabIndex = 5;
            this.label6.Text = "Стоимость/мес";
            // 
            // lb_id
            // 
            this.lb_id.AutoSize = true;
            this.lb_id.Location = new System.Drawing.Point(168, 23);
            this.lb_id.Name = "lb_id";
            this.lb_id.Size = new System.Drawing.Size(0, 13);
            this.lb_id.TabIndex = 6;
            // 
            // lb_nameSam
            // 
            this.lb_nameSam.AutoSize = true;
            this.lb_nameSam.Location = new System.Drawing.Point(168, 51);
            this.lb_nameSam.Name = "lb_nameSam";
            this.lb_nameSam.Size = new System.Drawing.Size(0, 13);
            this.lb_nameSam.TabIndex = 7;
            // 
            // lb_typeSam
            // 
            this.lb_typeSam.AutoSize = true;
            this.lb_typeSam.Location = new System.Drawing.Point(168, 81);
            this.lb_typeSam.Name = "lb_typeSam";
            this.lb_typeSam.Size = new System.Drawing.Size(0, 13);
            this.lb_typeSam.TabIndex = 8;
            // 
            // lb_probegSam
            // 
            this.lb_probegSam.AutoSize = true;
            this.lb_probegSam.Location = new System.Drawing.Point(168, 109);
            this.lb_probegSam.Name = "lb_probegSam";
            this.lb_probegSam.Size = new System.Drawing.Size(0, 13);
            this.lb_probegSam.TabIndex = 9;
            // 
            // lb_arendaSam
            // 
            this.lb_arendaSam.AutoSize = true;
            this.lb_arendaSam.Location = new System.Drawing.Point(168, 136);
            this.lb_arendaSam.Name = "lb_arendaSam";
            this.lb_arendaSam.Size = new System.Drawing.Size(0, 13);
            this.lb_arendaSam.TabIndex = 10;
            // 
            // lb_stoimostSam
            // 
            this.lb_stoimostSam.AutoSize = true;
            this.lb_stoimostSam.Location = new System.Drawing.Point(168, 162);
            this.lb_stoimostSam.Name = "lb_stoimostSam";
            this.lb_stoimostSam.Size = new System.Drawing.Size(0, 13);
            this.lb_stoimostSam.TabIndex = 11;
            // 
            // btn_buy
            // 
            this.btn_buy.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_buy.Location = new System.Drawing.Point(40, 214);
            this.btn_buy.Name = "btn_buy";
            this.btn_buy.Size = new System.Drawing.Size(128, 35);
            this.btn_buy.TabIndex = 12;
            this.btn_buy.Text = "Подтвердить";
            this.btn_buy.UseVisualStyleBackColor = true;
            this.btn_buy.Click += new System.EventHandler(this.btn_buy_Click);
            // 
            // btn_exit
            // 
            this.btn_exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_exit.Location = new System.Drawing.Point(174, 214);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(142, 35);
            this.btn_exit.TabIndex = 13;
            this.btn_exit.Text = "Отмена";
            this.btn_exit.UseVisualStyleBackColor = true;
            this.btn_exit.Click += new System.EventHandler(this.btn_exit_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(12, 186);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(124, 16);
            this.label7.TabIndex = 14;
            this.label7.Text = "Общая стоимость:";
            // 
            // lb_AllCost
            // 
            this.lb_AllCost.AutoSize = true;
            this.lb_AllCost.Location = new System.Drawing.Point(168, 186);
            this.lb_AllCost.Name = "lb_AllCost";
            this.lb_AllCost.Size = new System.Drawing.Size(0, 13);
            this.lb_AllCost.TabIndex = 15;
            // 
            // ArendaSam
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(345, 261);
            this.Controls.Add(this.lb_AllCost);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btn_exit);
            this.Controls.Add(this.btn_buy);
            this.Controls.Add(this.lb_stoimostSam);
            this.Controls.Add(this.lb_arendaSam);
            this.Controls.Add(this.lb_probegSam);
            this.Controls.Add(this.lb_typeSam);
            this.Controls.Add(this.lb_nameSam);
            this.Controls.Add(this.lb_id);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ArendaSam";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ArendaSam";
            this.Load += new System.EventHandler(this.ArendaSam_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btn_buy;
        private System.Windows.Forms.Button btn_exit;
        public System.Windows.Forms.Label lb_id;
        public System.Windows.Forms.Label lb_nameSam;
        public System.Windows.Forms.Label lb_probegSam;
        public System.Windows.Forms.Label lb_typeSam;
        public System.Windows.Forms.Label lb_arendaSam;
        public System.Windows.Forms.Label lb_stoimostSam;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lb_AllCost;
    }
}