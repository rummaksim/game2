﻿namespace Game
{
    partial class Shop
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listB_pass = new System.Windows.Forms.ListBox();
            this.listB_cargo = new System.Windows.Forms.ListBox();
            this.btn_buy = new System.Windows.Forms.Button();
            this.btn_arenda = new System.Windows.Forms.Button();
            this.btn_leasing = new System.Windows.Forms.Button();
            this.btn_exit = new System.Windows.Forms.Button();
            this.listB_info = new System.Windows.Forms.ListBox();
            this.tb_arendaMonths = new System.Windows.Forms.TextBox();
            this.tb_leasingMonths = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // listB_pass
            // 
            this.listB_pass.FormattingEnabled = true;
            this.listB_pass.Location = new System.Drawing.Point(22, 27);
            this.listB_pass.Name = "listB_pass";
            this.listB_pass.Size = new System.Drawing.Size(216, 95);
            this.listB_pass.TabIndex = 0;
            this.listB_pass.SelectedIndexChanged += new System.EventHandler(this.listB_pass_SelectedIndexChanged);
            // 
            // listB_cargo
            // 
            this.listB_cargo.FormattingEnabled = true;
            this.listB_cargo.Location = new System.Drawing.Point(285, 27);
            this.listB_cargo.Name = "listB_cargo";
            this.listB_cargo.Size = new System.Drawing.Size(216, 95);
            this.listB_cargo.TabIndex = 1;
            this.listB_cargo.SelectedIndexChanged += new System.EventHandler(this.listB_cargo_SelectedIndexChanged);
            // 
            // btn_buy
            // 
            this.btn_buy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_buy.Location = new System.Drawing.Point(35, 242);
            this.btn_buy.Name = "btn_buy";
            this.btn_buy.Size = new System.Drawing.Size(122, 28);
            this.btn_buy.TabIndex = 2;
            this.btn_buy.Text = "Купить";
            this.btn_buy.UseVisualStyleBackColor = true;
            this.btn_buy.Click += new System.EventHandler(this.btn_buy_Click);
            // 
            // btn_arenda
            // 
            this.btn_arenda.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_arenda.Location = new System.Drawing.Point(35, 276);
            this.btn_arenda.Name = "btn_arenda";
            this.btn_arenda.Size = new System.Drawing.Size(122, 29);
            this.btn_arenda.TabIndex = 3;
            this.btn_arenda.Text = "Взять в аренду";
            this.btn_arenda.UseVisualStyleBackColor = true;
            this.btn_arenda.Click += new System.EventHandler(this.btn_arenda_Click);
            // 
            // btn_leasing
            // 
            this.btn_leasing.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_leasing.Location = new System.Drawing.Point(35, 311);
            this.btn_leasing.Name = "btn_leasing";
            this.btn_leasing.Size = new System.Drawing.Size(122, 28);
            this.btn_leasing.TabIndex = 4;
            this.btn_leasing.Text = "Взять в лизинг";
            this.btn_leasing.UseVisualStyleBackColor = true;
            this.btn_leasing.Click += new System.EventHandler(this.btn_leasing_Click);
            // 
            // btn_exit
            // 
            this.btn_exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_exit.Location = new System.Drawing.Point(419, 307);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(82, 28);
            this.btn_exit.TabIndex = 5;
            this.btn_exit.Text = "Выход";
            this.btn_exit.UseVisualStyleBackColor = true;
            this.btn_exit.Click += new System.EventHandler(this.btn_exit_Click);
            // 
            // listB_info
            // 
            this.listB_info.FormattingEnabled = true;
            this.listB_info.Location = new System.Drawing.Point(133, 141);
            this.listB_info.Name = "listB_info";
            this.listB_info.Size = new System.Drawing.Size(267, 95);
            this.listB_info.TabIndex = 6;
            // 
            // tb_arendaMonths
            // 
            this.tb_arendaMonths.Location = new System.Drawing.Point(244, 281);
            this.tb_arendaMonths.MaxLength = 2;
            this.tb_arendaMonths.Name = "tb_arendaMonths";
            this.tb_arendaMonths.Size = new System.Drawing.Size(43, 20);
            this.tb_arendaMonths.TabIndex = 7;
            this.tb_arendaMonths.TextChanged += new System.EventHandler(this.tb_arendaMonths_TextChanged);
            this.tb_arendaMonths.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_arendaMonths_KeyPress);
            // 
            // tb_leasingMonths
            // 
            this.tb_leasingMonths.Location = new System.Drawing.Point(244, 313);
            this.tb_leasingMonths.MaxLength = 2;
            this.tb_leasingMonths.Name = "tb_leasingMonths";
            this.tb_leasingMonths.Size = new System.Drawing.Size(43, 20);
            this.tb_leasingMonths.TabIndex = 8;
            this.tb_leasingMonths.TextChanged += new System.EventHandler(this.tb_leasingMonths_TextChanged);
            this.tb_leasingMonths.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_leasingMonths_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(170, 317);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 16);
            this.label1.TabIndex = 9;
            this.label1.Text = "Месяцев:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(170, 282);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 16);
            this.label2.TabIndex = 10;
            this.label2.Text = "Месяцев:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(45, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(170, 16);
            this.label3.TabIndex = 11;
            this.label3.Text = "Пассажирские самолёты";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(311, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(139, 16);
            this.label4.TabIndex = 12;
            this.label4.Text = "Грузовые самолёты";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(212, 125);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 16);
            this.label5.TabIndex = 13;
            this.label5.Text = "Информация";
            // 
            // Shop
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Aquamarine;
            this.ClientSize = new System.Drawing.Size(515, 345);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb_leasingMonths);
            this.Controls.Add(this.tb_arendaMonths);
            this.Controls.Add(this.listB_info);
            this.Controls.Add(this.btn_exit);
            this.Controls.Add(this.btn_leasing);
            this.Controls.Add(this.btn_arenda);
            this.Controls.Add(this.btn_buy);
            this.Controls.Add(this.listB_cargo);
            this.Controls.Add(this.listB_pass);
            this.Name = "Shop";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Shop";
            this.Load += new System.EventHandler(this.Shop_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listB_pass;
        private System.Windows.Forms.ListBox listB_cargo;
        private System.Windows.Forms.Button btn_buy;
        private System.Windows.Forms.Button btn_arenda;
        private System.Windows.Forms.Button btn_leasing;
        private System.Windows.Forms.Button btn_exit;
        public System.Windows.Forms.ListBox listB_info;
        private System.Windows.Forms.TextBox tb_arendaMonths;
        private System.Windows.Forms.TextBox tb_leasingMonths;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}