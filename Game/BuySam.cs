﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.IO;
using System.Reflection;

namespace Game
{
    public partial class BuySam : Form
    {
        string pathToDB = "Data Source=" + string.Concat(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"\Planes.db");
        public static BuySam SelfRef
        {
            get;
            set;
        }
        public BuySam(Form form)
        {
            
            InitializeComponent();
            SelfRef = this;
        }

        private void BuySam_Load(object sender, EventArgs e)
        {

        }


        private void btn_buy_Click(object sender, EventArgs e)//покупка самолета
        {
            UserPlay.SelfRef.lb_budget.Text = Convert.ToString(int.Parse(UserPlay.SelfRef.lb_budget.Text) - int.Parse(lb_stoimostSam.Text));//обновление игрового бюджета (не в БД)
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            SQLiteCommand com = new SQLiteCommand();
            if (lb_probegSam.Text == "Без пробега")
            {
                com = new SQLiteCommand("Insert into BuyPlanes (currentLocation, idPlane, typeVladSam, vzlets) Values ('Пермь', '" + int.Parse(lb_id.Text) + "', '2', '0')",sqlite);
                com.ExecuteNonQuery();
                com = new SQLiteCommand("Select max(id) from BuyPlanes", sqlite);
                int idAsc = Convert.ToInt32(com.ExecuteScalar()); //получили id последнего купленного самолета
                //получим число экипажа для самолета
                com = new SQLiteCommand("select distinct needWorkers from PassengerPlanes, BuyPlanes where BuyPlanes.id= '" + idAsc + "' and (BuyPlanes.idPlane=PassengerPlanes.idPlane)", sqlite);
                int needWorkers = Convert.ToInt32(com.ExecuteScalar());
                if (needWorkers==0)
                {
                    com = new SQLiteCommand("select distinct needWorkers from CargoPlanes, BuyPlanes where BuyPlanes.id= '" + idAsc + "' and (BuyPlanes.idPlane=CargoPlanes.idPlane)", sqlite);
                    needWorkers = Convert.ToInt32(com.ExecuteScalar());
                }
                for (int i = 0; i < needWorkers; i++)
                {
                    com = new SQLiteCommand("Insert into UseWorkers(typeVladSam, idSamASC, countWorkers) values ('2', '" + idAsc + "', '0')", sqlite);
                    com.ExecuteNonQuery();
                }
                com = new SQLiteCommand("Update Planes set purchasePrice = PurchasePrice + ChangeCost where id = '" + int.Parse(lb_id.Text) + "' ", sqlite);
                com.ExecuteNonQuery();
                UserPlay.SelfRef.refresh_my_Buypass_planes();
                UserPlay.SelfRef.refresh_my_Buycargo_planes();
                UserPlay.SelfRef.lb_imidg.Text = Convert.ToString(int.Parse(UserPlay.SelfRef.lb_imidg.Text) + 100); //обновление имиджа без запроса
            }
            else
            {
                com = new SQLiteCommand("Insert into BuyPlanes (currentLocation, idPlane, typeVladSam, vzlets) Values ('Пермь', '" + int.Parse(lb_id.Text) + "', '2', '100')",sqlite);
                com.ExecuteNonQuery();
                com = new SQLiteCommand("Select max(id) from BuyPlanes", sqlite);
                int idAsc = Convert.ToInt32(com.ExecuteScalar()); //получили id последнего купленного самолета
                com = new SQLiteCommand("select distinct needWorkers from PassengerPlanes, BuyPlanes where BuyPlanes.id= '" + idAsc + "' and (BuyPlanes.idPlane=PassengerPlanes.idPlane)", sqlite);
                int needWorkers = Convert.ToInt32(com.ExecuteScalar());
                if (needWorkers == 0)
                {
                    com = new SQLiteCommand("select distinct needWorkers from CargoPlanes, BuyPlanes where BuyPlanes.id= '" + idAsc + "' and (BuyPlanes.idPlane=CargoPlanes.idPlane)", sqlite);
                    needWorkers = Convert.ToInt32(com.ExecuteScalar());
                }
                for (int i = 0; i < needWorkers; i++)
                {
                    com = new SQLiteCommand("Insert into UseWorkers(typeVladSam, idSamASC, countWorkers) values ('2', '" + idAsc + "', '0')", sqlite);
                    com.ExecuteNonQuery();
                }
                com = new SQLiteCommand("Update Planes set purchasePrice = PurchasePrice + ChangeCost where id = '" + int.Parse(lb_id.Text) + "' ", sqlite);
                com.ExecuteNonQuery();
                UserPlay.SelfRef.refresh_my_Buypass_planes();
                UserPlay.SelfRef.refresh_my_Buycargo_planes();
                UserPlay.SelfRef.lb_imidg.Text = Convert.ToString(int.Parse(UserPlay.SelfRef.lb_imidg.Text) - 100); //обновление имиджа без запроса 
            }
            sqlite.Close();
            this.Close();
            Shop.SelfRef.Close();

        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
