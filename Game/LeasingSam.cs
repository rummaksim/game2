﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.IO;
using System.Reflection;

namespace Game
{
    public partial class LeasingSam : Form
    {
        string pathToDB = "Data Source=" + string.Concat(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"\Planes.db");
        public static LeasingSam SelfRef
        {
            get;
            set;
        }
        public LeasingSam(Form form)
        {
            InitializeComponent();
            SelfRef = this;
            
        }

        private void LeasingSam_Load(object sender, EventArgs e)
        {
            if (lb_leasingSam.Text=="99")//!!!!!!!!!!
            lb_AllCost.Text = Convert.ToString(int.Parse(lb_leasingSam.Text) * int.Parse(lb_stoimostSam.Text));
        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_leasing_Click(object sender, EventArgs e)
        {
            UserPlay.SelfRef.lb_budget.Text = Convert.ToString(int.Parse(UserPlay.SelfRef.lb_budget.Text) - int.Parse(lb_stoimostSam.Text));//обновление игрового бюджета (не в БД)
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            SQLiteCommand com = new SQLiteCommand();
            if (lb_probegSam.Text == "Без пробега")
            {
                int ticks = int.Parse(lb_leasingSam.Text); //срок лизинга      
                string Endtime = UserPlay.SelfRef.EndArendaOrLeasing(ticks); //время окончания лизинга
                string NextPayment = UserPlay.SelfRef.NextPayArendaOrLeasing(); //время следующей оплаты лизинга
                com = new SQLiteCommand("Insert into LeasingPlanes (currentLocation, dateEndLeasing, ostCost, idPlane, typeVladSam, vzlets) Values ('Пермь', '" + Convert.ToString(Endtime) + "', '" + int.Parse(lb_OstStoimost.Text)+ "', '" + int.Parse(lb_id.Text) + "', '3', '0')", sqlite);
                com.ExecuteNonQuery();
                com = new SQLiteCommand("Select max(id) from LeasingPlanes", sqlite);
                int idAsc = Convert.ToInt32(com.ExecuteScalar()); //получили id последнего взятого в лизинг самолета
                com = new SQLiteCommand("select distinct needWorkers from PassengerPlanes, LeasingPlanes where LeasingPlanes.id= '" + idAsc + "' and (LeasingPlanes.idPlane=PassengerPlanes.idPlane)", sqlite);
                int needWorkers = Convert.ToInt32(com.ExecuteScalar());
                if (needWorkers == 0)
                {
                    com = new SQLiteCommand("select distinct needWorkers from CargoPlanes, LeasingPlanes where LeasingPlanes.id= '" + idAsc + "' and (LeasingPlanes.idPlane=CargoPlanes.idPlane)", sqlite);
                    needWorkers = Convert.ToInt32(com.ExecuteScalar());
                }
                for (int i = 0; i < needWorkers; i++)
                {
                    com = new SQLiteCommand("Insert into UseWorkers(typeVladSam, idSamASC, countWorkers) values ('3', '" + idAsc + "', '0')", sqlite);
                    com.ExecuteNonQuery();
                }
                com = new SQLiteCommand("Update Planes set purchasePrice = PurchasePrice + ChangeCost where id = '" + int.Parse(lb_id.Text) + "' ", sqlite);
                com.ExecuteNonQuery();
                UserPlay.SelfRef.lb_imidg.Text = Convert.ToString(int.Parse(UserPlay.SelfRef.lb_imidg.Text) + 100); //обновление имиджа без запроса
                UserPlay.SelfRef.refresh_my_Leasingcargo_planes();
                UserPlay.SelfRef.refresh_my_Leasingpass_planes();
            }
            else
            {
                int ticks = int.Parse(lb_leasingSam.Text); //срок лизинга
                string Endtime = UserPlay.SelfRef.EndArendaOrLeasing(ticks); //время окончания лизинга
                string NextPayment = UserPlay.SelfRef.NextPayArendaOrLeasing(); //время следующей оплаты лизинга
                com = new SQLiteCommand("Insert into LeasingPlanes (currentLocation, dateEndLeasing, ostCost, idPlane, typeVladSam, vzlets) Values ('Пермь', '" + Convert.ToString(Endtime) + "', '" + int.Parse(lb_OstStoimost.Text) + "', '" + int.Parse(lb_id.Text) + "', '3', '100')", sqlite);
                com.ExecuteNonQuery();
                com = new SQLiteCommand("Select max(id) from LeasingPlanes", sqlite);
                int idAsc = Convert.ToInt32(com.ExecuteScalar()); //получили id последнего взятого в лизинг самолета
                com = new SQLiteCommand("select distinct needWorkers from PassengerPlanes, LeasingPlanes where LeasingPlanes.id= '" + idAsc + "' and (LeasingPlanes.idPlane=PassengerPlanes.idPlane)", sqlite);
                int needWorkers = Convert.ToInt32(com.ExecuteScalar());
                if (needWorkers == 0)
                {
                    com = new SQLiteCommand("select distinct needWorkers from CargoPlanes, LeasingPlanes where LeasingPlanes.id= '" + idAsc + "' and (LeasingPlanes.idPlane=CargoPlanes.idPlane)", sqlite);
                    needWorkers = Convert.ToInt32(com.ExecuteScalar());
                }
                for (int i = 0; i < needWorkers; i++)
                {
                    com = new SQLiteCommand("Insert into UseWorkers(typeVladSam, idSamASC, countWorkers) values ('3', '" + idAsc + "', '0')", sqlite);
                    com.ExecuteNonQuery();
                }
                com = new SQLiteCommand("Update Planes set purchasePrice = PurchasePrice + ChangeCost where id = '" + int.Parse(lb_id.Text) + "' ", sqlite);
                com.ExecuteNonQuery();
                UserPlay.SelfRef.lb_imidg.Text = Convert.ToString(int.Parse(UserPlay.SelfRef.lb_imidg.Text) - 100); //обновление имиджа без запроса
                UserPlay.SelfRef.refresh_my_Leasingcargo_planes();
                UserPlay.SelfRef.refresh_my_Leasingpass_planes();
            }
            sqlite.Close();
            this.Close();
            Shop.SelfRef.Close();
        }

    }
}
