﻿namespace Game
{
    partial class LeasingSam
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lb_id = new System.Windows.Forms.Label();
            this.lb_nameSam = new System.Windows.Forms.Label();
            this.lb_typeSam = new System.Windows.Forms.Label();
            this.lb_probegSam = new System.Windows.Forms.Label();
            this.lb_leasingSam = new System.Windows.Forms.Label();
            this.lb_stoimostSam = new System.Windows.Forms.Label();
            this.lb_OstStoimost = new System.Windows.Forms.Label();
            this.btn_leasing = new System.Windows.Forms.Button();
            this.btn_exit = new System.Windows.Forms.Button();
            this.lb_AllCost = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(23, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Идентификатор";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(23, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Самолет";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(23, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Тип";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(23, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Пробег";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(23, 106);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 16);
            this.label5.TabIndex = 4;
            this.label5.Text = "Срок лизинга(мес)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(23, 130);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(106, 16);
            this.label6.TabIndex = 5;
            this.label6.Text = "Стоимость/мес";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(27, 152);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(157, 16);
            this.label7.TabIndex = 6;
            this.label7.Text = "Остаточная стоимость";
            // 
            // lb_id
            // 
            this.lb_id.AutoSize = true;
            this.lb_id.Location = new System.Drawing.Point(190, 9);
            this.lb_id.Name = "lb_id";
            this.lb_id.Size = new System.Drawing.Size(0, 13);
            this.lb_id.TabIndex = 7;
            // 
            // lb_nameSam
            // 
            this.lb_nameSam.AutoSize = true;
            this.lb_nameSam.Location = new System.Drawing.Point(190, 35);
            this.lb_nameSam.Name = "lb_nameSam";
            this.lb_nameSam.Size = new System.Drawing.Size(0, 13);
            this.lb_nameSam.TabIndex = 8;
            // 
            // lb_typeSam
            // 
            this.lb_typeSam.AutoSize = true;
            this.lb_typeSam.Location = new System.Drawing.Point(190, 59);
            this.lb_typeSam.Name = "lb_typeSam";
            this.lb_typeSam.Size = new System.Drawing.Size(0, 13);
            this.lb_typeSam.TabIndex = 9;
            // 
            // lb_probegSam
            // 
            this.lb_probegSam.AutoSize = true;
            this.lb_probegSam.Location = new System.Drawing.Point(190, 84);
            this.lb_probegSam.Name = "lb_probegSam";
            this.lb_probegSam.Size = new System.Drawing.Size(0, 13);
            this.lb_probegSam.TabIndex = 10;
            // 
            // lb_leasingSam
            // 
            this.lb_leasingSam.AutoSize = true;
            this.lb_leasingSam.Location = new System.Drawing.Point(190, 106);
            this.lb_leasingSam.Name = "lb_leasingSam";
            this.lb_leasingSam.Size = new System.Drawing.Size(0, 13);
            this.lb_leasingSam.TabIndex = 11;
            // 
            // lb_stoimostSam
            // 
            this.lb_stoimostSam.AutoSize = true;
            this.lb_stoimostSam.Location = new System.Drawing.Point(190, 130);
            this.lb_stoimostSam.Name = "lb_stoimostSam";
            this.lb_stoimostSam.Size = new System.Drawing.Size(0, 13);
            this.lb_stoimostSam.TabIndex = 12;
            // 
            // lb_OstStoimost
            // 
            this.lb_OstStoimost.AutoSize = true;
            this.lb_OstStoimost.Location = new System.Drawing.Point(190, 152);
            this.lb_OstStoimost.Name = "lb_OstStoimost";
            this.lb_OstStoimost.Size = new System.Drawing.Size(0, 13);
            this.lb_OstStoimost.TabIndex = 13;
            // 
            // btn_leasing
            // 
            this.btn_leasing.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_leasing.Location = new System.Drawing.Point(45, 213);
            this.btn_leasing.Name = "btn_leasing";
            this.btn_leasing.Size = new System.Drawing.Size(129, 36);
            this.btn_leasing.TabIndex = 14;
            this.btn_leasing.Text = "Подтвердить";
            this.btn_leasing.UseVisualStyleBackColor = true;
            this.btn_leasing.Click += new System.EventHandler(this.btn_leasing_Click);
            // 
            // btn_exit
            // 
            this.btn_exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_exit.Location = new System.Drawing.Point(193, 213);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(119, 36);
            this.btn_exit.TabIndex = 15;
            this.btn_exit.Text = "Отмена";
            this.btn_exit.UseVisualStyleBackColor = true;
            this.btn_exit.Click += new System.EventHandler(this.btn_exit_Click);
            // 
            // lb_AllCost
            // 
            this.lb_AllCost.AutoSize = true;
            this.lb_AllCost.Location = new System.Drawing.Point(190, 179);
            this.lb_AllCost.Name = "lb_AllCost";
            this.lb_AllCost.Size = new System.Drawing.Size(0, 13);
            this.lb_AllCost.TabIndex = 16;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(23, 179);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(121, 16);
            this.label9.TabIndex = 17;
            this.label9.Text = "Общая стоимость";
            // 
            // LeasingSam
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(372, 261);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lb_AllCost);
            this.Controls.Add(this.btn_exit);
            this.Controls.Add(this.btn_leasing);
            this.Controls.Add(this.lb_OstStoimost);
            this.Controls.Add(this.lb_stoimostSam);
            this.Controls.Add(this.lb_leasingSam);
            this.Controls.Add(this.lb_probegSam);
            this.Controls.Add(this.lb_typeSam);
            this.Controls.Add(this.lb_nameSam);
            this.Controls.Add(this.lb_id);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "LeasingSam";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "LeasingSam";
            this.Load += new System.EventHandler(this.LeasingSam_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label lb_id;
        public System.Windows.Forms.Label lb_nameSam;
        public System.Windows.Forms.Label lb_typeSam;
        public System.Windows.Forms.Label lb_probegSam;
        public System.Windows.Forms.Label lb_leasingSam;
        public System.Windows.Forms.Label lb_stoimostSam;
        public System.Windows.Forms.Label lb_OstStoimost;
        private System.Windows.Forms.Button btn_leasing;
        private System.Windows.Forms.Button btn_exit;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label lb_AllCost;
    }
}