﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.IO;
using System.Reflection;

namespace Game
{
    public partial class SellSam : Form
    {
        string pathToDB = "Data Source=" + string.Concat(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"\Planes.db");
        public static SellSam SelfRef
        {
            get;
            set;
        }
        public SellSam(Form form)
        {
            InitializeComponent();
            SelfRef = this;
        }
             
        private void SellSam_Load(object sender, EventArgs e)
        {

        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_sell_Click(object sender, EventArgs e)
        {
            UserPlay.SelfRef.lb_budget.Text = Convert.ToString(int.Parse(UserPlay.SelfRef.lb_budget.Text) + int.Parse(lb_stoimostSam.Text));//обновление игрового бюджета (не в БД)
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            SQLiteCommand com = new SQLiteCommand();

            com = new SQLiteCommand("select idPlane from BuyPlanes where id ='" + int.Parse(lb_id.Text) + "'", sqlite);
            long idPlane = (long)com.ExecuteScalar();
            com = new SQLiteCommand("select vzlets from BuyPlanes where id ='" + int.Parse(lb_id.Text) + "'", sqlite);
            long vzlets = (long)com.ExecuteScalar();
            com = new SQLiteCommand("update Planes set purchasePrice = purchasePrice-changeCost/2 where id='" + idPlane + "'", sqlite);
            com.ExecuteNonQuery();
                com = new SQLiteCommand("delete from BuyPlanes where id='" + int.Parse(lb_id.Text) + "'", sqlite);
                com.ExecuteNonQuery();
                com = new SQLiteCommand("Update UseWorkers set typeVladSam=0, idSamAsc=0 where idSamASC='" + int.Parse(lb_id.Text) + "'", sqlite);
                com.ExecuteNonQuery();
                com = new SQLiteCommand("delete from UseWorkers where typeVladSam=0 and countWorkers=0",sqlite);//удаление пустых записей
                com.ExecuteNonQuery();
            
                if (lb_typeSam.Text == "Пассажирский")
                    UserPlay.SelfRef.refresh_my_Buypass_planes();
                else
                    UserPlay.SelfRef.refresh_my_Buycargo_planes();
                if (vzlets < 100) // обновление имиджа
                    UserPlay.SelfRef.lb_imidg.Text = Convert.ToString(Convert.ToInt32(UserPlay.SelfRef.lb_imidg.Text) - 100);
                else
                    UserPlay.SelfRef.lb_imidg.Text = Convert.ToString(Convert.ToInt32(UserPlay.SelfRef.lb_imidg.Text) + 100);
                    this.Close();
        }
    }
}
