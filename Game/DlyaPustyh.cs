﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.IO;
using System.Reflection;
using System.Threading;

namespace Game
{
    public partial class DlyaPustyh : Form
    {
        string pathToDB = "Data Source=" + string.Concat(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"\Planes.db");
        public static DlyaPustyh SelfRef
        {
            get;
            set;
        }
        public DlyaPustyh(Form form)
        {
            InitializeComponent();
            SelfRef=this;
        }

        private void DlyaPustyh_Load(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            if (loc.Text != "Пермь")
                listBox1.Items.Add("Пермь");
            if (loc.Text != "Москва")
                listBox1.Items.Add("Москва");
            if (loc.Text != "Санкт Петербург")
                listBox1.Items.Add("Санкт Петербург");
            if (loc.Text != "Нью Йорк")
                listBox1.Items.Add("Нью Йорк");
            if (loc.Text != "Сидней")
                listBox1.Items.Add("Сидней");
            if (loc.Text != "Берлин")
                listBox1.Items.Add("Берлин");
            if (loc.Text != "Париж")
                listBox1.Items.Add("Париж");
            if (loc.Text != "Кейптаун")
                listBox1.Items.Add("Кейптаун");

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex > -1)
            {
                SQLiteConnection sqlite;
                sqlite = new SQLiteConnection(pathToDB);
                sqlite.Open();
                SQLiteCommand com = new SQLiteCommand();
                long rasstoyanie;
                com = new SQLiteCommand("select routeDistance from Flights where startFrom = '" + loc.Text + "' and destination = '" + listBox1.Items[listBox1.SelectedIndex] + "'", sqlite);
                rasstoyanie = (long)(com.ExecuteScalar()); //получили дистанцию перелета
                com = new SQLiteCommand("select id from Flights where startFrom = '" + loc.Text + "' and destination = '" + listBox1.Items[listBox1.SelectedIndex] + "'", sqlite);
                long idFlight = (long)(com.ExecuteScalar());
                if ((int.Parse(UserPlay.SelfRef.lb_budget.Text) < 20000) || (rasstoyanie > int.Parse(maxDistance.Text)))
                {
                    UserPlay.SelfRef.log.Items.Add("Рейс не отправлен! Проверьте самолет, экипаж или бюджет");
                    UserPlay.SelfRef.log.TopIndex = UserPlay.SelfRef.log.Items.Count - 1;
                }
                else
                {
                    UserPlay.SelfRef.log.Items.Add("Рейс успешно отправлен!");
                    UserPlay.SelfRef.log.TopIndex = UserPlay.SelfRef.log.Items.Count - 1;
                    //расстояние/скорость = время
                    double minutes = 60 * rasstoyanie / int.Parse(speed.Text);
                    minutes = Math.Floor(minutes); //количество минут полета
                    //добавили инфу в Таблицу расписания
                    if ((label1.Text == "Пассажирский") && (label2.Text == "Купленный"))
                    {
                        com = new SQLiteCommand("insert into TimetablePassFlights (comeTime, typeVladSam, idFlight, idSamAsc) values ('" + UserPlay.SelfRef.stopwatch.AddMinutes(minutes).Ticks + "', '2', '" + idFlight + "', '" + int.Parse(id.Text) + "')", sqlite);
                        com.ExecuteNonQuery();
                        com = new SQLiteCommand("update BuyPlanes set currentLocation = 'В полете' where id='" + int.Parse(id.Text) + "'", sqlite);
                        com.ExecuteNonQuery();
                        com = new SQLiteCommand("update BuyPlanes set vzlets = vzlets+1 where id='" + int.Parse(id.Text) + "'", sqlite);
                        com.ExecuteNonQuery();
                        double dohod = rasstoyanie * 0.02 * int.Parse(UserPlay.SelfRef.lb_benzCost.Text) * (2+ Math.Pow(double.Parse(vzlets.Text), 1.0 / 11));
                        dohod = Math.Floor(dohod);
                        UserPlay.SelfRef.log.Items.Add("Рейс отправлен! Расходы: " + dohod.ToString());
                        UserPlay.SelfRef.log.TopIndex = UserPlay.SelfRef.log.Items.Count - 1;
                        UserPlay.SelfRef.lb_budget.Text = Convert.ToString(int.Parse(UserPlay.SelfRef.lb_budget.Text) - dohod);
                        UserPlay.SelfRef.refreshMyTimeTable();
                        UserPlay.SelfRef.refresh_my_Buypass_planes();
                        UserPlay.SelfRef.refreshNonWorkWorkers();
                        UserPlay.SelfRef.refreshWorkWorkers();
                    }
                    if ((label1.Text == "Пассажирский") && (label2.Text == "Арендованный"))
                    {
                        com = new SQLiteCommand("insert into TimetablePassFlights (comeTime, typeVladSam, idFlight, idSamAsc) values ('" + UserPlay.SelfRef.stopwatch.AddMinutes(minutes).Ticks + "', '1', '" + idFlight + "', '" + int.Parse(id.Text) + "')", sqlite);
                        com.ExecuteNonQuery();
                        com = new SQLiteCommand("update ArendaPlanes set currentLocation = 'В полете' where id='" + int.Parse(id.Text) + "'", sqlite);
                        com.ExecuteNonQuery();
                        com = new SQLiteCommand("update ArendaPlanes set vzlets = vzlets+1 where id='" + int.Parse(id.Text) + "'", sqlite);
                        com.ExecuteNonQuery();
                        double dohod = rasstoyanie * 0.02 * int.Parse(UserPlay.SelfRef.lb_benzCost.Text) * (2+Math.Pow(double.Parse(vzlets.Text), 1.0 / 11));
                        dohod = Math.Floor(dohod);
                        UserPlay.SelfRef.log.Items.Add("Рейс отправлен! Расходы: " + dohod.ToString());
                        UserPlay.SelfRef.log.TopIndex = UserPlay.SelfRef.log.Items.Count - 1;
                        UserPlay.SelfRef.lb_budget.Text = Convert.ToString(int.Parse(UserPlay.SelfRef.lb_budget.Text) - dohod);
                        UserPlay.SelfRef.refreshMyTimeTable();
                        UserPlay.SelfRef.refresh_my_Arendapass_planes();
                        UserPlay.SelfRef.refreshNonWorkWorkers();
                        UserPlay.SelfRef.refreshWorkWorkers();
                    }
                    if ((label1.Text == "Пассажирский") && (label2.Text == "Лизингованный"))
                    {
                        com = new SQLiteCommand("insert into TimetablePassFlights (comeTime, typeVladSam, idFlight, idSamAsc) values ('" + UserPlay.SelfRef.stopwatch.AddMinutes(minutes).Ticks + "', '3', '" + idFlight + "', '" + int.Parse(id.Text) + "')", sqlite);
                        com.ExecuteNonQuery();
                        com = new SQLiteCommand("update LeasingPlanes set currentLocation = 'В полете' where id='" + int.Parse(id.Text) + "'", sqlite);
                        com.ExecuteNonQuery();
                        com = new SQLiteCommand("update LeasingPlanes set vzlets = vzlets+1 where id='" + int.Parse(id.Text) + "'", sqlite);
                        com.ExecuteNonQuery();
                        double dohod = rasstoyanie * 0.02 * int.Parse(UserPlay.SelfRef.lb_benzCost.Text) * (2+Math.Pow(double.Parse(vzlets.Text), 1.0 / 11));
                        dohod = Math.Floor(dohod);
                        UserPlay.SelfRef.log.Items.Add("Рейс отправлен! Расходы: " + dohod.ToString());
                        UserPlay.SelfRef.log.TopIndex = UserPlay.SelfRef.log.Items.Count - 1;
                        UserPlay.SelfRef.lb_budget.Text = Convert.ToString(int.Parse(UserPlay.SelfRef.lb_budget.Text) - dohod);
                        UserPlay.SelfRef.refreshMyTimeTable();
                        UserPlay.SelfRef.refresh_my_Leasingpass_planes();
                        UserPlay.SelfRef.refreshNonWorkWorkers();
                        UserPlay.SelfRef.refreshWorkWorkers();
                    }
                    if ((label1.Text == "Грузовой") && (label2.Text == "Купленный"))
                    {
                        com = new SQLiteCommand("insert into TimetableCargoFlights (comeTime, typeVladSam, idFlight, idSamAsc) values ('" + UserPlay.SelfRef.stopwatch.AddMinutes(minutes).Ticks + "', '2', '" + idFlight + "', '" + int.Parse(id.Text) + "')", sqlite);
                        com.ExecuteNonQuery();
                        com = new SQLiteCommand("update BuyPlanes set currentLocation = 'В полете' where id='" + int.Parse(id.Text) + "'", sqlite);
                        com.ExecuteNonQuery();
                        com = new SQLiteCommand("update BuyPlanes set vzlets = vzlets+1 where id='" + int.Parse(id.Text) + "'", sqlite);
                        com.ExecuteNonQuery();
                        double dohod = rasstoyanie * 0.02 * int.Parse(UserPlay.SelfRef.lb_benzCost.Text) *(2+ Math.Pow(double.Parse(vzlets.Text), 1.0 / 11));
                        dohod = Math.Floor(dohod);
                        UserPlay.SelfRef.log.Items.Add("Рейс отправлен! Расходы: " + dohod.ToString());
                        UserPlay.SelfRef.log.TopIndex = UserPlay.SelfRef.log.Items.Count - 1;
                        UserPlay.SelfRef.lb_budget.Text = Convert.ToString(int.Parse(UserPlay.SelfRef.lb_budget.Text) - dohod);
                        UserPlay.SelfRef.refreshMyTimeTable();
                        UserPlay.SelfRef.refresh_my_Buycargo_planes();
                        UserPlay.SelfRef.refreshNonWorkWorkers();
                        UserPlay.SelfRef.refreshWorkWorkers();
                    }
                    if ((label1.Text == "Грузовой") && (label2.Text == "Арендованный"))
                    {
                        com = new SQLiteCommand("insert into TimetableCargoFlights (comeTime, typeVladSam, idFlight, idSamAsc) values ('" + UserPlay.SelfRef.stopwatch.AddMinutes(minutes).Ticks + "', '1', '" + idFlight + "', '" + int.Parse(id.Text) + "')", sqlite);
                        com.ExecuteNonQuery();
                        com = new SQLiteCommand("update ArendaPlanes set currentLocation = 'В полете' where id='" + int.Parse(id.Text) + "'", sqlite);
                        com.ExecuteNonQuery();
                        com = new SQLiteCommand("update ArendaPlanes set vzlets = vzlets+1 where id='" + int.Parse(id.Text) + "'", sqlite);
                        com.ExecuteNonQuery();
                        double dohod = rasstoyanie * 0.02 * int.Parse(UserPlay.SelfRef.lb_benzCost.Text) *(2+ Math.Pow(double.Parse(vzlets.Text), 1.0 / 11));
                        dohod = Math.Floor(dohod);
                        UserPlay.SelfRef.log.Items.Add("Рейс отправлен! Расходы: " + dohod.ToString());
                        UserPlay.SelfRef.log.TopIndex = UserPlay.SelfRef.log.Items.Count - 1;
                        UserPlay.SelfRef.lb_budget.Text = Convert.ToString(int.Parse(UserPlay.SelfRef.lb_budget.Text) - dohod);
                        UserPlay.SelfRef.refreshMyTimeTable();
                        UserPlay.SelfRef.refresh_my_Arendacargo_planes();
                        UserPlay.SelfRef.refreshNonWorkWorkers();
                        UserPlay.SelfRef.refreshWorkWorkers();
                    }
                    if ((label1.Text == "Грузовой") && (label2.Text == "Лизингованный"))
                    {
                        com = new SQLiteCommand("insert into TimetableCargoFlights (comeTime, typeVladSam, idFlight, idSamAsc) values ('" + UserPlay.SelfRef.stopwatch.AddMinutes(minutes).Ticks + "', '3', '" + idFlight + "', '" + int.Parse(id.Text) + "')", sqlite);
                        com.ExecuteNonQuery();
                        com = new SQLiteCommand("update LeasingPlanes set currentLocation = 'В полете' where id='" + int.Parse(id.Text) + "'", sqlite);
                        com.ExecuteNonQuery();
                        com = new SQLiteCommand("update LeasingPlanes set vzlets = vzlets+1 where id='" + int.Parse(id.Text) + "'", sqlite);
                        com.ExecuteNonQuery();
                        double dohod = rasstoyanie * 0.02 * int.Parse(UserPlay.SelfRef.lb_benzCost.Text) *(2+ Math.Pow(double.Parse(vzlets.Text), 1.0 / 11));
                        dohod = Math.Floor(dohod);
                        UserPlay.SelfRef.log.Items.Add("Рейс отправлен! Расходы: " + dohod.ToString());
                        UserPlay.SelfRef.log.TopIndex = UserPlay.SelfRef.log.Items.Count - 1;
                        UserPlay.SelfRef.lb_budget.Text = Convert.ToString(int.Parse(UserPlay.SelfRef.lb_budget.Text) - dohod);
                        UserPlay.SelfRef.refreshMyTimeTable();
                        UserPlay.SelfRef.refresh_my_Leasingcargo_planes();
                        UserPlay.SelfRef.refreshNonWorkWorkers();
                        UserPlay.SelfRef.refreshWorkWorkers();
                    }
                }
            }
            this.Close();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            SQLiteCommand com = new SQLiteCommand();
            long rasstoyanie;
            com = new SQLiteCommand("select routeDistance from Flights where startFrom = '" + loc.Text + "' and destination = '" + listBox1.Items[listBox1.SelectedIndex] + "'", sqlite);
            rasstoyanie = (long)(com.ExecuteScalar()); //получили дистанцию перелета
            Info.Text = rasstoyanie.ToString();
        }
    }
}
