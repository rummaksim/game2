﻿namespace Game
{
    partial class UserPlay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Polzunok = new System.Windows.Forms.TrackBar();
            this.lb_text1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lb_time = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lb_budget = new System.Windows.Forms.Label();
            this.lb_text2 = new System.Windows.Forms.Label();
            this.lb_text3 = new System.Windows.Forms.Label();
            this.listB_pass_buy_planes = new System.Windows.Forms.ListBox();
            this.lb_text5 = new System.Windows.Forms.Label();
            this.lb_text6 = new System.Windows.Forms.Label();
            this.lb_text4 = new System.Windows.Forms.Label();
            this.listB_pass_arenda_planes = new System.Windows.Forms.ListBox();
            this.listB_pass_leasing_planes = new System.Windows.Forms.ListBox();
            this.btn_exit = new System.Windows.Forms.Button();
            this.btn_shop = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lb_myZab = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lb_botZab = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lb_benzCost = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.log = new System.Windows.Forms.ListBox();
            this.lb_imidg = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.listB_samInfo = new System.Windows.Forms.ListBox();
            this.listB_cargo_arenda_planes = new System.Windows.Forms.ListBox();
            this.listB_cargo_leasing_planes = new System.Windows.Forms.ListBox();
            this.listB_cargo_buy_planes = new System.Windows.Forms.ListBox();
            this.btn_sell = new System.Windows.Forms.Button();
            this.listB_takenPass = new System.Windows.Forms.ListBox();
            this.listB_generPass = new System.Windows.Forms.ListBox();
            this.listB_generCargo = new System.Windows.Forms.ListBox();
            this.listB_takenCargo = new System.Windows.Forms.ListBox();
            this.listB_reisInfo = new System.Windows.Forms.ListBox();
            this.btn_takePass = new System.Windows.Forms.Button();
            this.btn_takeCargo = new System.Windows.Forms.Button();
            this.btn_goPass = new System.Windows.Forms.Button();
            this.btn_goCargo = new System.Windows.Forms.Button();
            this.listB_rynokWorkers = new System.Windows.Forms.ListBox();
            this.listB_workWorkers = new System.Windows.Forms.ListBox();
            this.listB_infoWorkers = new System.Windows.Forms.ListBox();
            this.btn_changeWorker = new System.Windows.Forms.Button();
            this.btn_buyWorker = new System.Windows.Forms.Button();
            this.btn_uval = new System.Windows.Forms.Button();
            this.listB_nonworkWorkers = new System.Windows.Forms.ListBox();
            this.listB_timeTable = new System.Windows.Forms.ListBox();
            this.btn_del = new System.Windows.Forms.Button();
            this.btn_antireklama = new System.Windows.Forms.Button();
            this.btn_reklama = new System.Windows.Forms.Button();
            this.btn_zakaz = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.btn_pustoy = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Polzunok)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // Polzunok
            // 
            this.Polzunok.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Polzunok.Location = new System.Drawing.Point(16, 40);
            this.Polzunok.Maximum = 3;
            this.Polzunok.Name = "Polzunok";
            this.Polzunok.Size = new System.Drawing.Size(153, 45);
            this.Polzunok.TabIndex = 0;
            this.Polzunok.ValueChanged += new System.EventHandler(this.Polzunok_ValueChanged);
            // 
            // lb_text1
            // 
            this.lb_text1.AutoSize = true;
            this.lb_text1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lb_text1.Location = new System.Drawing.Point(55, 12);
            this.lb_text1.Name = "lb_text1";
            this.lb_text1.Size = new System.Drawing.Size(69, 16);
            this.lb_text1.TabIndex = 1;
            this.lb_text1.Text = "Скорость";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel1.Controls.Add(this.Polzunok);
            this.panel1.Controls.Add(this.lb_text1);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(187, 88);
            this.panel1.TabIndex = 2;
            // 
            // lb_time
            // 
            this.lb_time.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.lb_time.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lb_time.Location = new System.Drawing.Point(587, 15);
            this.lb_time.Name = "lb_time";
            this.lb_time.Size = new System.Drawing.Size(180, 85);
            this.lb_time.TabIndex = 3;
            this.lb_time.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel2.Controls.Add(this.lb_budget);
            this.panel2.Controls.Add(this.lb_text2);
            this.panel2.Location = new System.Drawing.Point(838, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(187, 91);
            this.panel2.TabIndex = 2;
            // 
            // lb_budget
            // 
            this.lb_budget.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lb_budget.Location = new System.Drawing.Point(3, 40);
            this.lb_budget.Name = "lb_budget";
            this.lb_budget.Size = new System.Drawing.Size(181, 23);
            this.lb_budget.TabIndex = 1;
            this.lb_budget.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_text2
            // 
            this.lb_text2.AutoSize = true;
            this.lb_text2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lb_text2.Location = new System.Drawing.Point(43, 5);
            this.lb_text2.Name = "lb_text2";
            this.lb_text2.Size = new System.Drawing.Size(91, 25);
            this.lb_text2.TabIndex = 0;
            this.lb_text2.Text = "Бюджет";
            // 
            // lb_text3
            // 
            this.lb_text3.AutoSize = true;
            this.lb_text3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lb_text3.Location = new System.Drawing.Point(113, 103);
            this.lb_text3.Name = "lb_text3";
            this.lb_text3.Size = new System.Drawing.Size(102, 16);
            this.lb_text3.TabIndex = 4;
            this.lb_text3.Text = "Мои самолеты";
            // 
            // listB_pass_buy_planes
            // 
            this.listB_pass_buy_planes.FormattingEnabled = true;
            this.listB_pass_buy_planes.Location = new System.Drawing.Point(16, 136);
            this.listB_pass_buy_planes.Name = "listB_pass_buy_planes";
            this.listB_pass_buy_planes.Size = new System.Drawing.Size(120, 95);
            this.listB_pass_buy_planes.TabIndex = 5;
            this.listB_pass_buy_planes.SelectedIndexChanged += new System.EventHandler(this.listB_pass_buy_planes_SelectedIndexChanged);
            // 
            // lb_text5
            // 
            this.lb_text5.AutoSize = true;
            this.lb_text5.Location = new System.Drawing.Point(130, 234);
            this.lb_text5.Name = "lb_text5";
            this.lb_text5.Size = new System.Drawing.Size(95, 13);
            this.lb_text5.TabIndex = 6;
            this.lb_text5.Text = "Взятые в аренду:";
            // 
            // lb_text6
            // 
            this.lb_text6.AutoSize = true;
            this.lb_text6.Location = new System.Drawing.Point(113, 348);
            this.lb_text6.Name = "lb_text6";
            this.lb_text6.Size = new System.Drawing.Size(95, 13);
            this.lb_text6.TabIndex = 7;
            this.lb_text6.Text = "Взятые в лизинг:";
            // 
            // lb_text4
            // 
            this.lb_text4.AutoSize = true;
            this.lb_text4.Location = new System.Drawing.Point(133, 120);
            this.lb_text4.Name = "lb_text4";
            this.lb_text4.Size = new System.Drawing.Size(66, 13);
            this.lb_text4.TabIndex = 8;
            this.lb_text4.Text = "Купленные:";
            // 
            // listB_pass_arenda_planes
            // 
            this.listB_pass_arenda_planes.FormattingEnabled = true;
            this.listB_pass_arenda_planes.Location = new System.Drawing.Point(16, 250);
            this.listB_pass_arenda_planes.Name = "listB_pass_arenda_planes";
            this.listB_pass_arenda_planes.Size = new System.Drawing.Size(120, 95);
            this.listB_pass_arenda_planes.TabIndex = 9;
            this.listB_pass_arenda_planes.SelectedIndexChanged += new System.EventHandler(this.listB_pass_arenda_planes_SelectedIndexChanged);
            // 
            // listB_pass_leasing_planes
            // 
            this.listB_pass_leasing_planes.FormattingEnabled = true;
            this.listB_pass_leasing_planes.Location = new System.Drawing.Point(16, 364);
            this.listB_pass_leasing_planes.Name = "listB_pass_leasing_planes";
            this.listB_pass_leasing_planes.Size = new System.Drawing.Size(120, 95);
            this.listB_pass_leasing_planes.TabIndex = 10;
            this.listB_pass_leasing_planes.SelectedIndexChanged += new System.EventHandler(this.listB_pass_leasing_planes_SelectedIndexChanged);
            // 
            // btn_exit
            // 
            this.btn_exit.ForeColor = System.Drawing.Color.DarkBlue;
            this.btn_exit.Location = new System.Drawing.Point(1031, 12);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(56, 23);
            this.btn_exit.TabIndex = 11;
            this.btn_exit.Text = "Выход";
            this.btn_exit.UseVisualStyleBackColor = true;
            this.btn_exit.Click += new System.EventHandler(this.btn_exit_Click);
            // 
            // btn_shop
            // 
            this.btn_shop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_shop.Location = new System.Drawing.Point(28, 569);
            this.btn_shop.Name = "btn_shop";
            this.btn_shop.Size = new System.Drawing.Size(90, 23);
            this.btn_shop.TabIndex = 12;
            this.btn_shop.Text = "Магазин";
            this.btn_shop.UseVisualStyleBackColor = true;
            this.btn_shop.Click += new System.EventHandler(this.btn_shop_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel3.Controls.Add(this.lb_myZab);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.lb_botZab);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.lb_benzCost);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.log);
            this.panel3.Controls.Add(this.lb_imidg);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Location = new System.Drawing.Point(16, 598);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1077, 100);
            this.panel3.TabIndex = 13;
            // 
            // lb_myZab
            // 
            this.lb_myZab.AutoSize = true;
            this.lb_myZab.Location = new System.Drawing.Point(118, 72);
            this.lb_myZab.Name = "lb_myZab";
            this.lb_myZab.Size = new System.Drawing.Size(0, 13);
            this.lb_myZab.TabIndex = 37;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 35;
            this.label2.Text = "Моя забастовка";
            // 
            // lb_botZab
            // 
            this.lb_botZab.AutoSize = true;
            this.lb_botZab.Location = new System.Drawing.Point(118, 50);
            this.lb_botZab.Name = "lb_botZab";
            this.lb_botZab.Size = new System.Drawing.Size(0, 13);
            this.lb_botZab.TabIndex = 31;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "Забастовка бота:";
            // 
            // lb_benzCost
            // 
            this.lb_benzCost.AutoSize = true;
            this.lb_benzCost.Location = new System.Drawing.Point(118, 25);
            this.lb_benzCost.Name = "lb_benzCost";
            this.lb_benzCost.Size = new System.Drawing.Size(0, 13);
            this.lb_benzCost.TabIndex = 34;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 33;
            this.label3.Text = "Цена бензина:";
            // 
            // log
            // 
            this.log.FormattingEnabled = true;
            this.log.Location = new System.Drawing.Point(195, 3);
            this.log.Name = "log";
            this.log.Size = new System.Drawing.Size(876, 95);
            this.log.TabIndex = 32;
            // 
            // lb_imidg
            // 
            this.lb_imidg.AutoSize = true;
            this.lb_imidg.Location = new System.Drawing.Point(118, 3);
            this.lb_imidg.Name = "lb_imidg";
            this.lb_imidg.Size = new System.Drawing.Size(0, 13);
            this.lb_imidg.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Имидж:";
            // 
            // listB_samInfo
            // 
            this.listB_samInfo.FormattingEnabled = true;
            this.listB_samInfo.Location = new System.Drawing.Point(16, 468);
            this.listB_samInfo.Name = "listB_samInfo";
            this.listB_samInfo.Size = new System.Drawing.Size(290, 95);
            this.listB_samInfo.TabIndex = 14;
            // 
            // listB_cargo_arenda_planes
            // 
            this.listB_cargo_arenda_planes.FormattingEnabled = true;
            this.listB_cargo_arenda_planes.Location = new System.Drawing.Point(186, 250);
            this.listB_cargo_arenda_planes.Name = "listB_cargo_arenda_planes";
            this.listB_cargo_arenda_planes.Size = new System.Drawing.Size(120, 95);
            this.listB_cargo_arenda_planes.TabIndex = 15;
            this.listB_cargo_arenda_planes.SelectedIndexChanged += new System.EventHandler(this.listB_cargo_arenda_planes_SelectedIndexChanged);
            // 
            // listB_cargo_leasing_planes
            // 
            this.listB_cargo_leasing_planes.FormattingEnabled = true;
            this.listB_cargo_leasing_planes.Location = new System.Drawing.Point(186, 364);
            this.listB_cargo_leasing_planes.Name = "listB_cargo_leasing_planes";
            this.listB_cargo_leasing_planes.Size = new System.Drawing.Size(120, 95);
            this.listB_cargo_leasing_planes.TabIndex = 16;
            this.listB_cargo_leasing_planes.SelectedIndexChanged += new System.EventHandler(this.listB_cargo_leasing_planes_SelectedIndexChanged);
            // 
            // listB_cargo_buy_planes
            // 
            this.listB_cargo_buy_planes.FormattingEnabled = true;
            this.listB_cargo_buy_planes.Location = new System.Drawing.Point(186, 136);
            this.listB_cargo_buy_planes.Name = "listB_cargo_buy_planes";
            this.listB_cargo_buy_planes.Size = new System.Drawing.Size(120, 95);
            this.listB_cargo_buy_planes.TabIndex = 17;
            this.listB_cargo_buy_planes.SelectedIndexChanged += new System.EventHandler(this.listB_cargo_buy_planes_SelectedIndexChanged);
            // 
            // btn_sell
            // 
            this.btn_sell.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_sell.Location = new System.Drawing.Point(133, 569);
            this.btn_sell.Name = "btn_sell";
            this.btn_sell.Size = new System.Drawing.Size(147, 23);
            this.btn_sell.TabIndex = 18;
            this.btn_sell.Text = "Продать самолет";
            this.btn_sell.UseVisualStyleBackColor = true;
            this.btn_sell.Click += new System.EventHandler(this.btn_sell_Click);
            // 
            // listB_takenPass
            // 
            this.listB_takenPass.FormattingEnabled = true;
            this.listB_takenPass.Location = new System.Drawing.Point(567, 228);
            this.listB_takenPass.Name = "listB_takenPass";
            this.listB_takenPass.Size = new System.Drawing.Size(200, 95);
            this.listB_takenPass.TabIndex = 20;
            this.listB_takenPass.SelectedIndexChanged += new System.EventHandler(this.listB_takenPass_SelectedIndexChanged);
            // 
            // listB_generPass
            // 
            this.listB_generPass.FormattingEnabled = true;
            this.listB_generPass.Location = new System.Drawing.Point(567, 465);
            this.listB_generPass.Name = "listB_generPass";
            this.listB_generPass.Size = new System.Drawing.Size(200, 95);
            this.listB_generPass.TabIndex = 21;
            this.listB_generPass.SelectedIndexChanged += new System.EventHandler(this.listB_generPass_SelectedIndexChanged);
            // 
            // listB_generCargo
            // 
            this.listB_generCargo.FormattingEnabled = true;
            this.listB_generCargo.Location = new System.Drawing.Point(886, 465);
            this.listB_generCargo.Name = "listB_generCargo";
            this.listB_generCargo.Size = new System.Drawing.Size(201, 95);
            this.listB_generCargo.TabIndex = 22;
            this.listB_generCargo.SelectedIndexChanged += new System.EventHandler(this.listB_generCargo_SelectedIndexChanged);
            // 
            // listB_takenCargo
            // 
            this.listB_takenCargo.FormattingEnabled = true;
            this.listB_takenCargo.Location = new System.Drawing.Point(876, 228);
            this.listB_takenCargo.Name = "listB_takenCargo";
            this.listB_takenCargo.Size = new System.Drawing.Size(200, 95);
            this.listB_takenCargo.TabIndex = 23;
            this.listB_takenCargo.SelectedIndexChanged += new System.EventHandler(this.listB_takenCargo_SelectedIndexChanged);
            // 
            // listB_reisInfo
            // 
            this.listB_reisInfo.FormattingEnabled = true;
            this.listB_reisInfo.Location = new System.Drawing.Point(697, 354);
            this.listB_reisInfo.Name = "listB_reisInfo";
            this.listB_reisInfo.Size = new System.Drawing.Size(246, 95);
            this.listB_reisInfo.TabIndex = 24;
            // 
            // btn_takePass
            // 
            this.btn_takePass.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_takePass.Location = new System.Drawing.Point(567, 566);
            this.btn_takePass.Name = "btn_takePass";
            this.btn_takePass.Size = new System.Drawing.Size(200, 23);
            this.btn_takePass.TabIndex = 27;
            this.btn_takePass.Text = "Взять пассажирский рейс";
            this.btn_takePass.UseVisualStyleBackColor = true;
            this.btn_takePass.Click += new System.EventHandler(this.btn_takePass_Click);
            // 
            // btn_takeCargo
            // 
            this.btn_takeCargo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_takeCargo.Location = new System.Drawing.Point(886, 566);
            this.btn_takeCargo.Name = "btn_takeCargo";
            this.btn_takeCargo.Size = new System.Drawing.Size(201, 23);
            this.btn_takeCargo.TabIndex = 28;
            this.btn_takeCargo.Text = "Взять грузовой рейс";
            this.btn_takeCargo.UseVisualStyleBackColor = true;
            this.btn_takeCargo.Click += new System.EventHandler(this.btn_takeCargo_Click);
            // 
            // btn_goPass
            // 
            this.btn_goPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_goPass.Location = new System.Drawing.Point(567, 324);
            this.btn_goPass.Name = "btn_goPass";
            this.btn_goPass.Size = new System.Drawing.Size(200, 23);
            this.btn_goPass.TabIndex = 29;
            this.btn_goPass.Text = "Отправить пассажирский рейс";
            this.btn_goPass.UseVisualStyleBackColor = true;
            this.btn_goPass.Click += new System.EventHandler(this.btn_goPass_Click);
            // 
            // btn_goCargo
            // 
            this.btn_goCargo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_goCargo.Location = new System.Drawing.Point(876, 324);
            this.btn_goCargo.Name = "btn_goCargo";
            this.btn_goCargo.Size = new System.Drawing.Size(200, 23);
            this.btn_goCargo.TabIndex = 30;
            this.btn_goCargo.Text = "Отправить грузовой рейс";
            this.btn_goCargo.UseVisualStyleBackColor = true;
            this.btn_goCargo.Click += new System.EventHandler(this.btn_goCargo_Click);
            // 
            // listB_rynokWorkers
            // 
            this.listB_rynokWorkers.FormattingEnabled = true;
            this.listB_rynokWorkers.Location = new System.Drawing.Point(379, 132);
            this.listB_rynokWorkers.Name = "listB_rynokWorkers";
            this.listB_rynokWorkers.Size = new System.Drawing.Size(120, 69);
            this.listB_rynokWorkers.TabIndex = 35;
            this.listB_rynokWorkers.SelectedIndexChanged += new System.EventHandler(this.listB_rynokWorkers_SelectedIndexChanged);
            // 
            // listB_workWorkers
            // 
            this.listB_workWorkers.FormattingEnabled = true;
            this.listB_workWorkers.Location = new System.Drawing.Point(379, 285);
            this.listB_workWorkers.Name = "listB_workWorkers";
            this.listB_workWorkers.Size = new System.Drawing.Size(120, 95);
            this.listB_workWorkers.TabIndex = 36;
            this.listB_workWorkers.SelectedIndexChanged += new System.EventHandler(this.listB_workWorkers_SelectedIndexChanged);
            // 
            // listB_infoWorkers
            // 
            this.listB_infoWorkers.FormattingEnabled = true;
            this.listB_infoWorkers.Location = new System.Drawing.Point(379, 220);
            this.listB_infoWorkers.Name = "listB_infoWorkers";
            this.listB_infoWorkers.Size = new System.Drawing.Size(120, 43);
            this.listB_infoWorkers.TabIndex = 37;
            // 
            // btn_changeWorker
            // 
            this.btn_changeWorker.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_changeWorker.Location = new System.Drawing.Point(379, 565);
            this.btn_changeWorker.Name = "btn_changeWorker";
            this.btn_changeWorker.Size = new System.Drawing.Size(120, 23);
            this.btn_changeWorker.TabIndex = 38;
            this.btn_changeWorker.Text = "Снять/поставить";
            this.btn_changeWorker.UseVisualStyleBackColor = true;
            this.btn_changeWorker.Click += new System.EventHandler(this.btn_changeWorker_Click);
            // 
            // btn_buyWorker
            // 
            this.btn_buyWorker.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_buyWorker.Location = new System.Drawing.Point(379, 536);
            this.btn_buyWorker.Name = "btn_buyWorker";
            this.btn_buyWorker.Size = new System.Drawing.Size(120, 23);
            this.btn_buyWorker.TabIndex = 39;
            this.btn_buyWorker.Text = "Нанять";
            this.btn_buyWorker.UseVisualStyleBackColor = true;
            this.btn_buyWorker.Click += new System.EventHandler(this.btn_buyWorker_Click);
            // 
            // btn_uval
            // 
            this.btn_uval.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_uval.Location = new System.Drawing.Point(379, 507);
            this.btn_uval.Name = "btn_uval";
            this.btn_uval.Size = new System.Drawing.Size(120, 23);
            this.btn_uval.TabIndex = 40;
            this.btn_uval.Text = "Уволить";
            this.btn_uval.UseVisualStyleBackColor = true;
            this.btn_uval.Click += new System.EventHandler(this.btn_uval_Click);
            // 
            // listB_nonworkWorkers
            // 
            this.listB_nonworkWorkers.FormattingEnabled = true;
            this.listB_nonworkWorkers.Location = new System.Drawing.Point(379, 406);
            this.listB_nonworkWorkers.Name = "listB_nonworkWorkers";
            this.listB_nonworkWorkers.Size = new System.Drawing.Size(120, 95);
            this.listB_nonworkWorkers.TabIndex = 41;
            this.listB_nonworkWorkers.SelectedIndexChanged += new System.EventHandler(this.listB_nonworkWorkers_SelectedIndexChanged);
            // 
            // listB_timeTable
            // 
            this.listB_timeTable.FormattingEnabled = true;
            this.listB_timeTable.Location = new System.Drawing.Point(567, 122);
            this.listB_timeTable.Name = "listB_timeTable";
            this.listB_timeTable.Size = new System.Drawing.Size(510, 95);
            this.listB_timeTable.TabIndex = 42;
            // 
            // btn_del
            // 
            this.btn_del.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_del.Location = new System.Drawing.Point(785, 244);
            this.btn_del.Name = "btn_del";
            this.btn_del.Size = new System.Drawing.Size(75, 23);
            this.btn_del.TabIndex = 43;
            this.btn_del.Text = "Удалить рейс";
            this.btn_del.UseVisualStyleBackColor = true;
            this.btn_del.Click += new System.EventHandler(this.btn_del_Click);
            // 
            // btn_antireklama
            // 
            this.btn_antireklama.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_antireklama.ForeColor = System.Drawing.Color.DarkRed;
            this.btn_antireklama.Location = new System.Drawing.Point(263, 45);
            this.btn_antireklama.Name = "btn_antireklama";
            this.btn_antireklama.Size = new System.Drawing.Size(185, 23);
            this.btn_antireklama.TabIndex = 44;
            this.btn_antireklama.Text = "Заказать антирекламу";
            this.btn_antireklama.UseVisualStyleBackColor = true;
            this.btn_antireklama.Click += new System.EventHandler(this.btn_antireklama_Click);
            // 
            // btn_reklama
            // 
            this.btn_reklama.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_reklama.ForeColor = System.Drawing.Color.DarkRed;
            this.btn_reklama.Location = new System.Drawing.Point(263, 17);
            this.btn_reklama.Name = "btn_reklama";
            this.btn_reklama.Size = new System.Drawing.Size(185, 23);
            this.btn_reklama.TabIndex = 45;
            this.btn_reklama.Text = "Заказать рекламу";
            this.btn_reklama.UseVisualStyleBackColor = true;
            this.btn_reklama.Click += new System.EventHandler(this.btn_reklama_Click);
            // 
            // btn_zakaz
            // 
            this.btn_zakaz.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_zakaz.ForeColor = System.Drawing.Color.DarkRed;
            this.btn_zakaz.Location = new System.Drawing.Point(263, 74);
            this.btn_zakaz.Name = "btn_zakaz";
            this.btn_zakaz.Size = new System.Drawing.Size(185, 23);
            this.btn_zakaz.TabIndex = 46;
            this.btn_zakaz.Text = "Заказать забастовку";
            this.btn_zakaz.UseVisualStyleBackColor = true;
            this.btn_zakaz.Click += new System.EventHandler(this.btn_zakaz_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(372, 269);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(133, 13);
            this.label5.TabIndex = 47;
            this.label5.Text = "Работающие сотрудники";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(372, 390);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(146, 13);
            this.label6.TabIndex = 48;
            this.label6.Text = "Неработающие сотрудники";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(385, 106);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(114, 16);
            this.label7.TabIndex = 49;
            this.label7.Text = "Рынок вакансий";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(142, 452);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 13);
            this.label8.TabIndex = 50;
            this.label8.Text = "Info";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(241, 120);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 13);
            this.label9.TabIndex = 51;
            this.label9.Text = "Грузовые";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 120);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 13);
            this.label10.TabIndex = 52;
            this.label10.Text = "Пассажирские";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(413, 204);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(25, 13);
            this.label11.TabIndex = 53;
            this.label11.Text = "Info";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(782, 452);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(91, 13);
            this.label12.TabIndex = 54;
            this.label12.Text = "Предлож. рейсы";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(773, 220);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(103, 13);
            this.label13.TabIndex = 55;
            this.label13.Text = "Мои взятые рейсы";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(765, 106);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(87, 16);
            this.label14.TabIndex = 56;
            this.label14.Text = "Распасание";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(798, 338);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(35, 13);
            this.label15.TabIndex = 57;
            this.label15.Text = "Инфа";
            // 
            // btn_pustoy
            // 
            this.btn_pustoy.Location = new System.Drawing.Point(785, 285);
            this.btn_pustoy.Name = "btn_pustoy";
            this.btn_pustoy.Size = new System.Drawing.Size(75, 23);
            this.btn_pustoy.TabIndex = 59;
            this.btn_pustoy.Text = "Пустой";
            this.btn_pustoy.UseVisualStyleBackColor = true;
            this.btn_pustoy.Click += new System.EventHandler(this.btn_pustoy_Click);
            // 
            // UserPlay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(1099, 708);
            this.Controls.Add(this.btn_pustoy);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btn_zakaz);
            this.Controls.Add(this.btn_reklama);
            this.Controls.Add(this.btn_antireklama);
            this.Controls.Add(this.btn_del);
            this.Controls.Add(this.listB_timeTable);
            this.Controls.Add(this.listB_nonworkWorkers);
            this.Controls.Add(this.btn_uval);
            this.Controls.Add(this.btn_buyWorker);
            this.Controls.Add(this.btn_changeWorker);
            this.Controls.Add(this.listB_rynokWorkers);
            this.Controls.Add(this.listB_workWorkers);
            this.Controls.Add(this.listB_infoWorkers);
            this.Controls.Add(this.btn_goCargo);
            this.Controls.Add(this.btn_goPass);
            this.Controls.Add(this.btn_takeCargo);
            this.Controls.Add(this.btn_takePass);
            this.Controls.Add(this.listB_reisInfo);
            this.Controls.Add(this.listB_takenCargo);
            this.Controls.Add(this.listB_generCargo);
            this.Controls.Add(this.listB_generPass);
            this.Controls.Add(this.listB_takenPass);
            this.Controls.Add(this.btn_sell);
            this.Controls.Add(this.listB_cargo_buy_planes);
            this.Controls.Add(this.listB_cargo_leasing_planes);
            this.Controls.Add(this.listB_cargo_arenda_planes);
            this.Controls.Add(this.listB_samInfo);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.btn_shop);
            this.Controls.Add(this.btn_exit);
            this.Controls.Add(this.listB_pass_leasing_planes);
            this.Controls.Add(this.listB_pass_arenda_planes);
            this.Controls.Add(this.lb_text4);
            this.Controls.Add(this.lb_text6);
            this.Controls.Add(this.lb_text5);
            this.Controls.Add(this.listB_pass_buy_planes);
            this.Controls.Add(this.lb_text3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.lb_time);
            this.Controls.Add(this.panel1);
            this.Name = "UserPlay";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Игра";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UserPlay_FormClosing);
            this.Load += new System.EventHandler(this.UserPlay_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Polzunok)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar Polzunok;
        private System.Windows.Forms.Label lb_text1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lb_time;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lb_text2;
        private System.Windows.Forms.Label lb_text3;
        private System.Windows.Forms.ListBox listB_pass_buy_planes;
        private System.Windows.Forms.Label lb_text5;
        private System.Windows.Forms.Label lb_text6;
        private System.Windows.Forms.Label lb_text4;
        private System.Windows.Forms.ListBox listB_pass_arenda_planes;
        private System.Windows.Forms.ListBox listB_pass_leasing_planes;
        private System.Windows.Forms.Button btn_exit;
        private System.Windows.Forms.Button btn_shop;
        public System.Windows.Forms.Label lb_budget;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label lb_imidg;
        private System.Windows.Forms.ListBox listB_samInfo;
        private System.Windows.Forms.ListBox listB_cargo_arenda_planes;
        private System.Windows.Forms.ListBox listB_cargo_leasing_planes;
        private System.Windows.Forms.ListBox listB_cargo_buy_planes;
        private System.Windows.Forms.Button btn_sell;
        private System.Windows.Forms.ListBox listB_takenPass;
        private System.Windows.Forms.ListBox listB_generPass;
        private System.Windows.Forms.ListBox listB_generCargo;
        private System.Windows.Forms.ListBox listB_takenCargo;
        private System.Windows.Forms.ListBox listB_reisInfo;
        private System.Windows.Forms.Button btn_takePass;
        private System.Windows.Forms.Button btn_takeCargo;
        private System.Windows.Forms.Button btn_goPass;
        private System.Windows.Forms.Button btn_goCargo;
        public System.Windows.Forms.Label lb_benzCost;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lb_botZab;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox listB_rynokWorkers;
        private System.Windows.Forms.ListBox listB_workWorkers;
        private System.Windows.Forms.ListBox listB_infoWorkers;
        private System.Windows.Forms.Button btn_changeWorker;
        private System.Windows.Forms.Button btn_buyWorker;
        private System.Windows.Forms.Button btn_uval;
        private System.Windows.Forms.ListBox listB_nonworkWorkers;
        private System.Windows.Forms.ListBox listB_timeTable;
        private System.Windows.Forms.Button btn_del;
        private System.Windows.Forms.Button btn_antireklama;
        private System.Windows.Forms.Button btn_reklama;
        private System.Windows.Forms.Button btn_zakaz;
        private System.Windows.Forms.Label lb_myZab;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btn_pustoy;
        public System.Windows.Forms.ListBox log;
    }
}