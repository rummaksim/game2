﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.IO;
using System.Reflection;
using System.Threading;

namespace Game
{
    public partial class UserPlay : Form
    {
        System.Windows.Forms.Timer timer;
         public  DateTime stopwatch = DateTime.Now;
        string pathToDB = "Data Source=" + string.Concat(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"\Planes.db");
        DataTable BuyPassPlanes = new DataTable(); //заполнение информацией о пасс. купленных самолетах
        DataTable BuyCargoPlanes = new DataTable(); //заполнение информацией о груз. купленных самолетах
        DataTable ArendaPassPlanes = new DataTable(); //заполнение информацией о пасс. арендованных самолетах
        DataTable ArendaCargoPlanes = new DataTable(); //заполнение информацией о груз. арендованных самолетах
        DataTable LeasingPassPlanes = new DataTable(); //заполнение информю о взятых в лизинг пасс. самолетах
        DataTable LeasingCargoPlanes = new DataTable();//заполнение информю о взятых в лизинг груз. самолетах
        DataTable PassReis = new DataTable(); //заполнение информ. о пассажирских рейсах
        DataTable CargoReis = new DataTable(); //заполнение информ. о грузовых рейсах
        DataTable PassTakenReis = new DataTable(); //информ. а взятых пасс. рейсах
        DataTable CargoTakenReis = new DataTable(); // информ. о взятых груз. рейсах
        DataTable RynokWorkers = new DataTable(); //информация о рынке труда
        DataTable WorkWorkers = new DataTable(); //информация о моих работающих сотрудниках
        DataTable NonWorkWorkers = new DataTable(); //информация о моих неработающих сотрудниках
        DataTable Timetable = new DataTable();//расписание
        string EndNextPassArenda="";//ближайший конец аренды пасс. самолета
        string EndNextCargoArenda=""; //ближайший конец аренды груз. самолета
        string EndNextPassLeasing=""; //ближайший конец лизинга пасс. самолета
        string EndNextCargoLeasing=""; // ближайший конец лизинга груз. самолета
        string EndNextReis = ""; //ближайший конец рейса
        int plus_time=1; //сколько минут добавляет ticktimer
        

        public static UserPlay SelfRef
        {
            get;
            set;
        }
        public UserPlay() 
        {
            InitializeComponent();
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            new SellSam(this);
            new BuySam(this);
            new ArendaSam(this);
            new LeasingSam(this);
            new Shop(this);
            new LeasingBuy(this);
            new ThenEndArenda(this);
            new DlyaPustyh(this);
            SQLiteCommand com = new SQLiteCommand("SELECT gameTime FROM Info", sqlite);
            long time = (long)com.ExecuteScalar();
            com = new SQLiteCommand("select benzCost From Info", sqlite);
            lb_benzCost.Text = Convert.ToString(com.ExecuteScalar()); //считывание цены бензина
            com = new SQLiteCommand("select myZabastovka From Info", sqlite);
            lb_myZab.Text = Convert.ToString(com.ExecuteScalar()); //считывание цены бензина
            com = new SQLiteCommand("select botZabastovka from Info", sqlite);
            lb_botZab.Text = Convert.ToString(com.ExecuteScalar()); //считывание информации о забастовке бота
            if (time>0)
            stopwatch = new DateTime(time); //считывание времении из бд при загрузке игры            
            com = new SQLiteCommand("SELECT myBudget FROM Info", sqlite);  //считывание бюджета из бд при загрузке игры
            long budget = (long)com.ExecuteScalar();
            lb_budget.Text = budget.ToString();
            com = new SQLiteCommand("SELECT myImidg FROM Info", sqlite); //считывание имиджа из БД при загрузке игры
            long imidg = (long)com.ExecuteScalar();
            lb_imidg.Text = imidg.ToString();
            sqlite.Close();
            sekundomer(1000);
            SelfRef = this;
            refreshMyTimeTable(); //обновление расписания
            refresh_my_Buypass_planes(); //обновление купленных пасс. самолетов
            refresh_my_Buycargo_planes(); //обновление купленных груз. самолетов
            refresh_my_Arendacargo_planes(); //обновление арендованных груз. самолетов
            refresh_my_Arendapass_planes(); // обновление арендованных пасс. самолетов
            refresh_my_Leasingcargo_planes();//обновление лизинг самолетов
            refresh_my_Leasingpass_planes();//обновление лизинг самолетов
            PassReisRefresh(); //обновление сгенерированных пассажирских рейсов
            CargoReisRefresh(); //обновление сгенерированных грузовых рейсов
            PassReisTakenRefresh(); //обновление информации о взятых пасс. рейсах
            CargoReisTakenRefresh(); // обновление информации о взятых грузовых рейсах
            refreshRynokWorkers(); // обновлении информации о рынке сотрудников
            refreshWorkWorkers();// обновление моих работающих сотрудников
            refreshNonWorkWorkers();//обновление моих неработающих сотрудников
            btn_sell.Enabled = false;
            btn_takeCargo.Enabled = false;
            btn_takePass.Enabled = false;
            btn_goPass.Enabled = false;
            btn_goCargo.Enabled = false;
        }
       
        private void UserPlay_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
        }
        public string EndArendaOrLeasing(int dlitelnost) // дата окончания аренды/лизинга
        { 
            DateTime EndDate = stopwatch.AddMonths(dlitelnost);
            string time = EndDate.Ticks.ToString();
            return time;
        }

        public string NextPayArendaOrLeasing() // дата следующей оплаты аренды/лизинга
        {
            DateTime EndDate = stopwatch.AddMonths(1);
            string time = EndDate.Ticks.ToString();
            return time;
        }
        private void Polzunok_ValueChanged(object sender, EventArgs e) //изменение скорости течения времени
        {
            timer.Stop();
            switch (Polzunok.Value+1)
            {
                case 1:
                    sekundomer(1000);
                    break;
                case 2:
                    sekundomer(500);
                    break;
                case 3:
                    sekundomer(100);
                    break;
                case 4:
                    sekundomer(1);
                    break;
            }

        }
        public void sekundomer(int milisecond) //изменение скорости течения времени
        {
            timer = new System.Windows.Forms.Timer();
            timer.Tick += new EventHandler(tickTimer);
            timer.Interval = milisecond;
            timer.Start();
        }

        public void change_plusTime() //изменения плюсования времени при некоторых событиях
        {
            plus_time = 1;
        }
        public void tickTimer(object sender, EventArgs e) //для игрового времени 
        {
            if (int.Parse(lb_imidg.Text)<0)
                lb_imidg.Text="0";
            string EndSam=stopwatch.Ticks.ToString(); //считываем время
            if (EndSam==EndNextReis) //обновление рейсов и статусов
            {
                EndNextReis = "";
                SQLiteConnection sqlite;
                sqlite = new SQLiteConnection(pathToDB);
                sqlite.Open();
                SQLiteCommand com = new SQLiteCommand();
                com = new SQLiteCommand("delete from TimetablePassFlights where comeTime = '" + Convert.ToInt64(EndSam) + "'", sqlite);
                com.ExecuteNonQuery();
                com = new SQLiteCommand("delete from TimetableCargoFlights where comeTime = '" + Convert.ToInt64(EndSam) + "'", sqlite);
                com.ExecuteNonQuery();
                if (Timetable.Rows[0][2].ToString() == "2") //обновление текущей локации самолетов
                {
                    com = new SQLiteCommand("update BuyPlanes set currentLocation='" + Timetable.Rows[0][7] + "'where id = '" + Timetable.Rows[0][4] + "'", sqlite);
                    com.ExecuteNonQuery();
                }
                if (Timetable.Rows[0][2].ToString() == "3")
                {
                    com = new SQLiteCommand("update LeasingPlanes set currentLocation='" + Timetable.Rows[0][7] + "'where id = '" + Timetable.Rows[0][4] + "'", sqlite);
                    com.ExecuteNonQuery();
                }
                if (Timetable.Rows[0][2].ToString() == "1")
                {
                    com = new SQLiteCommand("update ArendaPlanes set currentLocation='" + Timetable.Rows[0][7] + "'where id = '" + Timetable.Rows[0][4] + "'", sqlite);
                    com.ExecuteNonQuery();
                }
                refreshRynokWorkers();
                refreshWorkWorkers();
                refreshNonWorkWorkers();
                refresh_my_Buypass_planes(); //обновление купленных пасс. самолетов
                refresh_my_Buycargo_planes(); //обновление купленных груз. самолетов
                refresh_my_Arendacargo_planes(); //обновление арендованных груз. самолетов
                refresh_my_Arendapass_planes(); // обновление арендованных пасс. самолетов
                refresh_my_Leasingcargo_planes();//обновление лизинг самолетов
                refresh_my_Leasingpass_planes();//обновление лизинг самолетов
                refreshMyTimeTable(); // обновление расписания
            }
            if (EndSam==EndNextPassArenda)
            {
                EndNextPassArenda = "";
                plus_time = 0;
                BuySam.SelfRef.Close(); //закрываем все формы, так как на них не стоит автообновление при выполнении действия
                ArendaSam.SelfRef.Close();
                LeasingSam.SelfRef.Close();
                SellSam.SelfRef.Close();
                Shop.SelfRef.Close();                
                string message = "Аренда вашего пассажирского самолета "+ArendaPassPlanes.Rows[0][1]+" закончена";
                ThenEndArenda.SelfRef.lb_info.Text = message;
                ThenEndArenda.SelfRef.ShowDialog();
                SQLiteConnection sqlite;
                sqlite = new SQLiteConnection(pathToDB);
                sqlite.Open();
                SQLiteCommand com = new SQLiteCommand();
                com = new SQLiteCommand("select idPlane from ArendaPlanes where id='" + Convert.ToInt32(ArendaPassPlanes.Rows[0][0]) + "'", sqlite); //получили id удаляемого самолета
                long idPlane = (long)com.ExecuteScalar();
                int vzlets = Convert.ToInt32(ArendaPassPlanes.Rows[0][5]); //получили число взлетов
                com = new SQLiteCommand("update Planes set purchasePrice = purchasePrice-changeCost/2 where id='" + idPlane + "'", sqlite); //обновили цену на самолет
                com.ExecuteNonQuery();
                com = new SQLiteCommand("delete from ArendaPlanes where id='" + Convert.ToInt32(ArendaPassPlanes.Rows[0][0]) + "'", sqlite); //удалили самолет из арендованных
                com.ExecuteNonQuery();
                com = new SQLiteCommand("Update UseWorkers set typeVladSam=0, idSamAsc=0 where idSamASC='" + Convert.ToInt32(ArendaPassPlanes.Rows[0][0]) + "'", sqlite); //обновили сотрудников
                com.ExecuteNonQuery();
                com = new SQLiteCommand("delete from UseWorkers where typeVladSam=0 and countWorkers=0", sqlite);//удаление пустых записей о сотрудниках
                com.ExecuteNonQuery();
                refreshWorkWorkers();
                refreshNonWorkWorkers();
                if (vzlets < 100) // обновление имиджа
                    UserPlay.SelfRef.lb_imidg.Text = Convert.ToString(Convert.ToInt32(UserPlay.SelfRef.lb_imidg.Text) - 100);
                else
                    UserPlay.SelfRef.lb_imidg.Text = Convert.ToString(Convert.ToInt32(UserPlay.SelfRef.lb_imidg.Text) + 100);
                UserPlay.SelfRef.refresh_my_Arendapass_planes(); //обновление панели самолетов    

                
            }

            if (EndSam == EndNextCargoArenda)
            {
                EndNextCargoArenda = "";
                plus_time = 0;
                BuySam.SelfRef.Close(); //закрываем все формы, так как на них не стоит автообновление при выполнении действия
                ArendaSam.SelfRef.Close();
                LeasingSam.SelfRef.Close();
                SellSam.SelfRef.Close();
                Shop.SelfRef.Close();
                string message = "Аренда вашего грузового самолета " + ArendaCargoPlanes.Rows[0][1] + " закончена";
                ThenEndArenda.SelfRef.lb_info.Text = message;
                ThenEndArenda.SelfRef.ShowDialog();
                SQLiteConnection sqlite;
                sqlite = new SQLiteConnection(pathToDB);
                sqlite.Open();
                SQLiteCommand com = new SQLiteCommand();
                com = new SQLiteCommand("select idPlane from ArendaPlanes where id='" + Convert.ToInt32(ArendaCargoPlanes.Rows[0][0]) + "'", sqlite); //получили id удаляемого самолета
                long idPlane = (long)com.ExecuteScalar();
                int vzlets = Convert.ToInt32(ArendaCargoPlanes.Rows[0][5]); //получили число взлетов
                com = new SQLiteCommand("update Planes set purchasePrice = purchasePrice-changeCost/2 where id='" + idPlane + "'", sqlite); //обновили цену на самолет
                com.ExecuteNonQuery();
                com = new SQLiteCommand("delete from ArendaPlanes where id='" + Convert.ToInt32(ArendaCargoPlanes.Rows[0][0]) + "'", sqlite); //удалили самолет из арендованных
                com.ExecuteNonQuery();
                com = new SQLiteCommand("Update UseWorkers set typeVladSam=0, idSamAsc=0 where idSamASC='" + Convert.ToInt32(ArendaCargoPlanes.Rows[0][0]) + "'", sqlite); //обновили сотрудников
                com.ExecuteNonQuery();
                com = new SQLiteCommand("delete from UseWorkers where typeVladSam=0 and countWorkers=0", sqlite);//удаление пустых записей о сотрудниках
                com.ExecuteNonQuery();
                refreshWorkWorkers();
                refreshNonWorkWorkers();
                if (vzlets < 100) // обновление имиджа
                    UserPlay.SelfRef.lb_imidg.Text = Convert.ToString(Convert.ToInt32(UserPlay.SelfRef.lb_imidg.Text) - 100);
                else
                    UserPlay.SelfRef.lb_imidg.Text = Convert.ToString(Convert.ToInt32(UserPlay.SelfRef.lb_imidg.Text) + 100);
                UserPlay.SelfRef.refresh_my_Arendacargo_planes(); //обновление панели самолетов       
            }
                if (EndSam == EndNextPassLeasing) //конец пасс. лизинга
                {
                    EndNextPassLeasing = "";
                    plus_time = 0;
                    BuySam.SelfRef.Close(); //закрываем все формы, так как на них не стоит автообновление при выполнении действия
                    ArendaSam.SelfRef.Close();
                    LeasingSam.SelfRef.Close();
                    SellSam.SelfRef.Close();
                    Shop.SelfRef.Close();
                    string message = "Срок лизинга вашего самолета " + LeasingPassPlanes.Rows[0][1] + " подошел к концу";
                    LeasingBuy.SelfRef.lb_info.Text = message;
                    LeasingBuy.SelfRef.lb_Cost.Text = Convert.ToString(LeasingPassPlanes.Rows[0][10]); //остаточная стоимость
                    LeasingBuy.SelfRef.lb_type.Text = "Пассажирский";
                    LeasingBuy.SelfRef.ShowDialog();                      
                }
                if (EndSam == EndNextCargoLeasing)
                {
                    EndNextCargoLeasing = "";
                    plus_time = 0;
                    BuySam.SelfRef.Close(); //закрываем все формы, так как на них не стоит автообновление при выполнении действия
                    ArendaSam.SelfRef.Close();
                    LeasingSam.SelfRef.Close();
                    SellSam.SelfRef.Close();
                    Shop.SelfRef.Close();
                    string message = "Срок лизинга вашего самолета " + LeasingCargoPlanes.Rows[0][1] + " подошел к концу";
                    LeasingBuy.SelfRef.lb_info.Text = message;
                    LeasingBuy.SelfRef.lb_Cost.Text = Convert.ToString(LeasingCargoPlanes.Rows[0][10]); //остаточная стоимость
                    LeasingBuy.SelfRef.lb_type.Text = "Грузовой";
                    LeasingBuy.SelfRef.ShowDialog();  
                }
                lb_time.Text = String.Format("{0:dd.MM.yyyyHH:mm}", stopwatch);           
                stopwatch = stopwatch.AddMinutes(plus_time);  
                if ((stopwatch.Hour==0) && (stopwatch.Minute==0))
                {
                    PassReisGeneration(); //генерация пасс. рейсов
                    CargoReisGeneration(); //генерация грузовых рейсов
                    PassReisTakenRefreshPolnoch(); //обновление взятых пассажирских рейсов в полночь
                    CargoReisTakenRefreshPolnoch();
                    refreshWorkWorkers();// обновление моих работающих сотрудников
                    refreshNonWorkWorkers();//обновление моих неработающих сотрудников
                    refresh_my_Buypass_planes(); //обновление купленных пасс. самолетов
                    refresh_my_Buycargo_planes(); //обновление купленных груз. самолетов
                    refresh_my_Arendacargo_planes(); //обновление арендованных груз. самолетов
                    refresh_my_Arendapass_planes(); // обновление арендованных пасс. самолетов
                    refresh_my_Leasingcargo_planes();//обновление лизинг самолетов
                    refresh_my_Leasingpass_planes();//обновление лизинг самолетов
 
                    ///!!ПРИ ПОСТАНОВКЕ РЕЙСА В РАСПИСАНИЕ УДАЛЯЕМ ЕГО ИЗ БД ВЗЯТЫХ СРАЗУ ЖЕ ЕСЛИ НЕ РЕГУЛЯРНЫЙ, ИНАЧЕ МЕНЯЕМ ДАТУ!!! 
                    //все действия при обновлении суток
                    lb_imidg.Text = Convert.ToString(int.Parse(lb_imidg.Text) - 100); //ежедневное обновление имиджа
                    log.Items.Add("Имидж обновлен");
                    log.TopIndex = log.Items.Count - 1;
                    SQLiteConnection sqlite;
                    sqlite = new SQLiteConnection(pathToDB);
                    sqlite.Open();
                    SQLiteCommand com = new SQLiteCommand();
                    com = new SQLiteCommand("update Planes set purchasePrice=PurchasePrice-changeCost/10",sqlite);
                    com.ExecuteNonQuery();
                    log.Items.Add("Цены на самолеты изменены!"); //ежедневное обновление цен на самолеты
                    log.TopIndex = log.Items.Count - 1;
                    lb_myZab.Text = "Нет";//ежедневная отмена забастовки
                    lb_botZab.Text = "Нет"; //ежедневная отмена забастовки
                    btn_goPass.Visible = true;
                    btn_goCargo.Visible = true;
                    btn_pustoy.Visible = true;
                    com = new SQLiteCommand("select sum(payment) from UseWorkers",sqlite);
                    int sum;
                    if (com.ExecuteScalar().ToString() != "")
                    {
                         sum = Convert.ToInt32((com.ExecuteScalar()));
                    }
                    else sum = 0;
                    lb_budget.Text = Convert.ToString(int.Parse(lb_budget.Text) - sum);
                    log.Items.Add("Зарплаты выплачены! Размер: " + sum);
                    log.TopIndex = log.Items.Count - 1;
                    Random rnd3 = new Random();
                    int rnd_benz = rnd3.Next(1, 101);
                    if (rnd_benz <50)
                    {
                        lb_benzCost.Text = Convert.ToString(int.Parse(lb_benzCost.Text) - 1);
                        log.Items.Add("Цена на бензин упала!");
                        log.TopIndex = log.Items.Count - 1;
                    }
                    else
                    {
                        lb_benzCost.Text = Convert.ToString(int.Parse(lb_benzCost.Text) + 1);
                        log.Items.Add("Цена на бензин выросла!");
                        log.TopIndex = log.Items.Count - 1;
                    }
                    Random rnd1 = new Random();
                    int deistv = rnd1.Next(1, 101);//действия бота
                    com = new SQLiteCommand("select avg(payment) from UseWorkers", sqlite);
                    int avg;
                    if (com.ExecuteScalar().ToString() != "")
                    {
                        avg = Convert.ToInt32((com.ExecuteScalar()));
                    }
                    else avg = 999999;
                    if ((avg < 4000) && (deistv > 30))
                        deistv -= 15; //увеличение шанса забастовки.
                    if (deistv<30)
                    {
                        lb_myZab.Text = "Да";
                        log.Items.Add("У вас началась забастовка! Либо проделки конкурента, либо поднимайте зарплату!");
                        lb_imidg.Text = Convert.ToString(int.Parse(lb_imidg.Text) - 200);
                        log.TopIndex = log.Items.Count - 1;
                        btn_goCargo.Visible = false;
                        btn_goPass.Visible = false;
                        btn_pustoy.Visible = false;
                    }
                    if ((deistv>30)&&(deistv<70))
                    {
                        lb_imidg.Text = Convert.ToString(int.Parse(lb_imidg.Text) - 200);
                        log.Items.Add("Вам заказали антирекламу!");
                        log.TopIndex = log.Items.Count - 1;
                    }
                    if (deistv > 70)
                        log.Items.Add("Бот заказал себе рекламу!");
                    log.TopIndex = log.Items.Count - 1;
                    Random rnd2 = new Random();
                    int idPokupka = rnd2.Next(1, 21);

                    
                    com = new SQLiteCommand("update Planes set purchasePrice=purchasePrice+changeCost where id ='" + idPokupka + "'",sqlite);
                    com.ExecuteNonQuery();
                    com = new SQLiteCommand("select name from Planes where id='" + idPokupka + "'",sqlite);
                    string s = Convert.ToString(com.ExecuteScalar()); //название купленного самолета
                    log.Items.Add("Бот купил самолет " + s); //ежедневная покупка самоелта ботом  
                    log.TopIndex = log.Items.Count - 1;
                    if (int.Parse(lb_imidg.Text) < 0)
                        lb_imidg.Text = "0";
                }    
            if ((stopwatch.Hour%2==0)&&(stopwatch.Hour!=0)&&(stopwatch.Minute==0)) //воровство рейсов конкурентом
            {
                if (lb_botZab.Text=="Нет")
                {
                    if ((stopwatch.Hour%2==0)&&(stopwatch.Hour%4!=0))
                    {
                        if (listB_generPass.Items.Count>0)
                        {
                            SQLiteConnection sqlite;
                            sqlite = new SQLiteConnection(pathToDB);
                            sqlite.Open();
                            SQLiteCommand com = new SQLiteCommand();
                            com = new SQLiteCommand("delete from TodayPassFlights where idPassFlight = '" + Convert.ToInt32(PassReis.Rows[0][5]) + "'", sqlite);
                            com.ExecuteNonQuery();
                            PassReisRefresh();
                            log.Items.Add("Конкурент забрал пассажирский рейс!");
                            log.TopIndex = log.Items.Count - 1;
                        }
                    }
                    else
                        if (listB_generCargo.Items.Count > 0)
                        {
                            SQLiteConnection sqlite;
                            sqlite = new SQLiteConnection(pathToDB);
                            sqlite.Open();
                            SQLiteCommand com = new SQLiteCommand();
                            com = new SQLiteCommand("delete from TodayCargoFlights where idCargoFlight = '" + Convert.ToInt32(CargoReis.Rows[0][5]) + "'", sqlite);
                            com.ExecuteNonQuery();
                            CargoReisRefresh();
                            log.Items.Add("Конкурент забрал грузовой рейс!");
                            log.TopIndex = log.Items.Count - 1;
                        }
                }
            }
        }

     
        public void PassReisGeneration() //генерация пассажирских рейсов при наступлении нового дня
        {
            PassReis.Clear();
            listB_generPass.Items.Clear();
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            Random rnd = new Random();
            int idFlight = rnd.Next(1, 8); //rnd от 1 до 7 
            SQLiteCommand command = new SQLiteCommand("delete from TodayPassFlights",sqlite);
            command.ExecuteNonQuery(); //удаление всего из сохраненных взятых
            SQLiteDataAdapter com = new SQLiteDataAdapter("select * from Flights, PassengerFlights where id=idFlight and (idFlight-'" + idFlight + "')%7=0", sqlite);
            com.Fill(PassReis); //id, startFrom, destination, routeDistance, forfeit, idFlight, regularity, popularHours, minCapacity, ticketPrice
            for (int i=0; i<PassReis.Rows.Count; i++)
            {
                DataRow dr_PassReis = PassReis.Rows[i];
                listB_generPass.Items.Add(dr_PassReis["startFrom"].ToString()+"-"+dr_PassReis["destination"].ToString());
                command = new SQLiteCommand("insert into TodayPassFlights(idPassFlight) values ('" + Convert.ToInt32(PassReis.Rows[i][0]) + "')", sqlite); //добавляем рейсы в сохраненки
                command.ExecuteNonQuery();
            }                     
        }
        public void PassReisRefresh() //обновление сгенерированных пассажирских рейсов
        {
            PassReis.Clear();
            listB_generPass.Items.Clear();
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            SQLiteDataAdapter com = new SQLiteDataAdapter("select Flights.id, startFrom, destination, routeDistance, forfeit, PassengerFlights.idFlight, regularity, popularHours, minCapacity, ticketPrice from Flights, PassengerFlights, TodayPassFlights where Flights.id = TodayPassFlights.idPassFlight and PassengerFlights.idFlight = TodayPassFlights.idPassFlight", sqlite);
            com.Fill(PassReis);
            for (int i = 0; i < PassReis.Rows.Count; i++)
            {
                DataRow dr_PassReis = PassReis.Rows[i];
                listB_generPass.Items.Add(dr_PassReis["startFrom"].ToString() + "-" + dr_PassReis["destination"].ToString());
            }
        }

        private void listB_generPass_SelectedIndexChanged(object sender, EventArgs e) //информация о пасс. рейсе
        {
            if (listB_generPass.SelectedIndex > -1)
            {
                btn_takePass.Enabled = true;
                btn_takeCargo.Enabled = false;
                listB_takenPass.ClearSelected();
                listB_takenCargo.ClearSelected();
                listB_generCargo.ClearSelected();
                listB_reisInfo.Items.Clear();
                listB_reisInfo.Items.Add("Расстояние: " + PassReis.Rows[listB_generPass.SelectedIndex][3]);
                listB_reisInfo.Items.Add("Регулярность: " + PassReis.Rows[listB_generPass.SelectedIndex][6]);
                listB_reisInfo.Items.Add("Популярные часы: " + PassReis.Rows[listB_generPass.SelectedIndex][7]);
                listB_reisInfo.Items.Add("Минимальная необх. пассажировм-ть: " + PassReis.Rows[listB_generPass.SelectedIndex][8]);
                listB_reisInfo.Items.Add("Цена за билет: " + PassReis.Rows[listB_generPass.SelectedIndex][9]);
                listB_reisInfo.Items.Add("Неустойка: " + PassReis.Rows[listB_generPass.SelectedIndex][4]);          
            }
        }

        public void CargoReisGeneration()//генерация грузовых рейсов при наступлении нового дня
        {
            CargoReis.Clear();
            listB_generCargo.Items.Clear();
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            Random rnd = new Random();
            int idFlight = rnd.Next(1, 8); //rnd от 1 до 7 
            SQLiteCommand command = new SQLiteCommand("delete from TodayCargoFlights", sqlite);
            command.ExecuteNonQuery(); //удаление всего из сохраненных взятых
            SQLiteDataAdapter com = new SQLiteDataAdapter("select * from Flights, CargoFlights where id=idFlight and (idFlight-'" + idFlight + "')%7=0", sqlite);
            com.Fill(CargoReis); //id, startFrom, destination, routeDistance, forfeit, idFlight,  minLoadingCapacity, flightPayment
            for (int i = 0; i < CargoReis.Rows.Count; i++)
            {
                DataRow dr_CargoReis = CargoReis.Rows[i];
                listB_generCargo.Items.Add(dr_CargoReis["startFrom"].ToString() + "-" + dr_CargoReis["destination"].ToString());
                command = new SQLiteCommand("insert into TodayCargoFlights(idCargoFlight) values ('" + Convert.ToInt32(CargoReis.Rows[i][0]) + "')", sqlite); //добавляем рейсы в сохраненки
                command.ExecuteNonQuery();
            }                     
        }

        public void CargoReisRefresh() //обновление сгенерированных грузовых рейсов
        {
            CargoReis.Clear();
            listB_generCargo.Items.Clear();
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            SQLiteDataAdapter com = new SQLiteDataAdapter("select Flights.id, startFrom, destination, routeDistance, forfeit, CargoFlights.idFlight, minLoadingCapacity, flightPayment from Flights, CargoFlights, TodayCargoFlights where Flights.id = TodayCargoFlights.idCargoFlight and CargoFlights.idFlight = TodayCargoFlights.idCargoFlight", sqlite);
            com.Fill(CargoReis);
            for (int i = 0; i < CargoReis.Rows.Count; i++)
            {
                DataRow dr_CargoReis = CargoReis.Rows[i];
                listB_generCargo.Items.Add(dr_CargoReis["startFrom"].ToString() + "-" + dr_CargoReis["destination"].ToString());
            }
        }
        private void listB_generCargo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listB_generCargo.SelectedIndex > -1)
            {
                btn_takeCargo.Enabled = true;
                btn_takePass.Enabled = false;
                listB_takenPass.ClearSelected();
                listB_takenCargo.ClearSelected();
                listB_generPass.ClearSelected();
                listB_reisInfo.Items.Clear();
                listB_reisInfo.Items.Add("Расстояние: " + CargoReis.Rows[listB_generCargo.SelectedIndex][3]);
                listB_reisInfo.Items.Add("Нужная грузоподъемность: " + CargoReis.Rows[listB_generCargo.SelectedIndex][6]);
                listB_reisInfo.Items.Add("Оплата за рейс: " + CargoReis.Rows[listB_generCargo.SelectedIndex][7]);
                listB_reisInfo.Items.Add("Неустойка: " + CargoReis.Rows[listB_generCargo.SelectedIndex][4]);
            }
        }

        public void LeasingPassOtkaz()//удаление лизингованного самолета
        {
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            SQLiteCommand com = new SQLiteCommand();
            com = new SQLiteCommand("select idPlane from LeasingPlanes where id='" + Convert.ToInt32(LeasingPassPlanes.Rows[0][0]) + "'", sqlite); //получили id удаляемого самолета
            long idPlane = (long)com.ExecuteScalar(); //для обновления цены
            int vzlets = Convert.ToInt32(LeasingPassPlanes.Rows[0][5]); //получили число взлетов
            com = new SQLiteCommand("update Planes set purchasePrice = purchasePrice-changeCost/2 where id='" + idPlane + "'", sqlite); //обновили цену на самолет
            com.ExecuteNonQuery();
            com = new SQLiteCommand("delete from LeasingPlanes where id='" + Convert.ToInt32(LeasingPassPlanes.Rows[0][0]) + "'", sqlite); //удалили самолет из лизингованных
            com.ExecuteNonQuery();
            com = new SQLiteCommand("Update UseWorkers set typeVladSam=0, idSamAsc=0 where idSamASC='" + Convert.ToInt32(LeasingPassPlanes.Rows[0][0]) + "'", sqlite); //обновили сотрудников
            com.ExecuteNonQuery();
            com = new SQLiteCommand("delete from UseWorkers where typeVladSam=0 and countWorkers=0", sqlite);//удаление пустых записей о сотрудниках
            com.ExecuteNonQuery();
            refreshWorkWorkers();
            refreshNonWorkWorkers();
            if (vzlets < 100) // обновление имиджа
                UserPlay.SelfRef.lb_imidg.Text = Convert.ToString(Convert.ToInt32(UserPlay.SelfRef.lb_imidg.Text) - 100);
            else
                UserPlay.SelfRef.lb_imidg.Text = Convert.ToString(Convert.ToInt32(UserPlay.SelfRef.lb_imidg.Text) + 100);
            UserPlay.SelfRef.refresh_my_Leasingpass_planes(); //обновление панели самолетов
            UserPlay.SelfRef.refresh_my_Leasingpass_planes();
            change_plusTime(); //вызываем изменение времени
        }
        
        public void LeasingPassAccept()//перенос пассажирских из лизинга в купленные
        {
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            SQLiteCommand com = new SQLiteCommand();
            com = new SQLiteCommand("select idPlane from LeasingPlanes where id='" + Convert.ToInt32(LeasingPassPlanes.Rows[0][0]) + "'", sqlite); //получили id удаляемого самолета
            long idPlane = (long)com.ExecuteScalar();
            int vzlets = Convert.ToInt32(LeasingPassPlanes.Rows[0][5]); //получили число взлетов
            string currLoc = Convert.ToString(LeasingPassPlanes.Rows[0][2]); //получили текущую локацию
            int idL = Convert.ToInt32(LeasingPassPlanes.Rows[0][0]); //idASC в UseWorkers
            com = new SQLiteCommand("delete from LeasingPlanes where id='" + idL + "'", sqlite); //удалили самолет из лизингованных
            com.ExecuteNonQuery();
            com = new SQLiteCommand("insert into BuyPlanes(currentLocation, idPlane, typeVladSam, vzlets) values ('" + currLoc + "', '" + idPlane + "', '2', '" + vzlets + "')", sqlite); 
            com.ExecuteNonQuery();//вставили в купленные самолеты
            com = new SQLiteCommand("select max(id) from BuyPlanes",sqlite);
            long idB=(long)(com.ExecuteScalar()); //получили idASC того же самолета в купленных и обновляем сотрудников
            com = new SQLiteCommand("update UseWorkers set typeVladSam='2', idSamAsc = '" + idB + "' where idSamAsc='" + idL + "' and typeVladSam='3'", sqlite);
            com.ExecuteNonQuery();//обновили сотрудников
            UserPlay.SelfRef.refresh_my_Leasingpass_planes();
            UserPlay.SelfRef.refresh_my_Buypass_planes();
            refreshWorkWorkers();
            refreshNonWorkWorkers();
            change_plusTime();
        }

        public void LeasingCargoOtkaz()//удаление лизингованного самолета
        {
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            SQLiteCommand com = new SQLiteCommand();
            com = new SQLiteCommand("select idPlane from LeasingPlanes where id='" + Convert.ToInt32(LeasingCargoPlanes.Rows[0][0]) + "'", sqlite); //получили id удаляемого самолета
            long idPlane = (long)com.ExecuteScalar();
            int vzlets = Convert.ToInt32(LeasingCargoPlanes.Rows[0][5]); //число взлетов
            com = new SQLiteCommand("update Planes set purchasePrice = purchasePrice-changeCost/2 where id='" + idPlane + "'", sqlite); //обновили цену на самолет
            com.ExecuteNonQuery();
            com = new SQLiteCommand("delete from LeasingPlanes where id='" + Convert.ToInt32(LeasingCargoPlanes.Rows[0][0]) + "'", sqlite); //удалили самолет из лизингованных
            com.ExecuteNonQuery();
            com = new SQLiteCommand("Update UseWorkers set typeVladSam=0, idSamAsc=0 where idSamASC='" + Convert.ToInt32(LeasingCargoPlanes.Rows[0][0]) + "'", sqlite); //обновили сотрудников
            com.ExecuteNonQuery();
            com = new SQLiteCommand("delete from UseWorkers where typeVladSam=0 and countWorkers=0", sqlite);//удаление пустых записей о сотрудниках
            com.ExecuteNonQuery();
            refreshWorkWorkers();
            refreshNonWorkWorkers();
            if (vzlets < 100) // обновление имиджа
                UserPlay.SelfRef.lb_imidg.Text = Convert.ToString(Convert.ToInt32(UserPlay.SelfRef.lb_imidg.Text) - 100);
            else
                UserPlay.SelfRef.lb_imidg.Text = Convert.ToString(Convert.ToInt32(UserPlay.SelfRef.lb_imidg.Text) + 100);
            UserPlay.SelfRef.refresh_my_Leasingcargo_planes(); //обновление панели самолетов   
            UserPlay.SelfRef.refresh_my_Leasingcargo_planes();
            change_plusTime();
        }

        public void LeasingCargoAccept() //перенос грухового лизингованного в купленные
        {
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            SQLiteCommand com = new SQLiteCommand();
            com = new SQLiteCommand("select idPlane from LeasingPlanes where id='" + Convert.ToInt32(LeasingCargoPlanes.Rows[0][0]) + "'", sqlite); //получили id удаляемого самолета
            long idPlane = (long)com.ExecuteScalar();
            int vzlets = Convert.ToInt32(LeasingCargoPlanes.Rows[0][5]); //получили число взлетов
            string currLoc = Convert.ToString(LeasingCargoPlanes.Rows[0][2]); //получили текущую локацию
            int idL = Convert.ToInt32(LeasingCargoPlanes.Rows[0][0]); //idASC в UseWorkers
            com = new SQLiteCommand("delete from LeasingPlanes where id='" + idL + "'", sqlite); //удалили самолет из лизингованных
            com.ExecuteNonQuery();
            com = new SQLiteCommand("insert into BuyPlanes(currentLocation, idPlane, typeVladSam, vzlets) values ('" + currLoc + "', '" + idPlane + "', '2', '" + vzlets + "')", sqlite);
            com.ExecuteNonQuery();//вставили в купленные самолеты
            com = new SQLiteCommand("select max(id) from BuyPlanes", sqlite);
            long idB = (long)(com.ExecuteScalar()); //получили idASC того же самолета в купленных и обновляем сотрудников
            com = new SQLiteCommand("update UseWorkers set typeVladSam='2', idSamAsc = '" + idB + "' where idSamAsc='" + idL + "' and typeVladSam='3'", sqlite);
            com.ExecuteNonQuery();//обновили сотрудников
            UserPlay.SelfRef.refresh_my_Leasingcargo_planes();
            UserPlay.SelfRef.refresh_my_Buycargo_planes();
            refreshWorkWorkers();
            refreshNonWorkWorkers();
            change_plusTime();
        }

        private void btn_exit_Click(object sender, EventArgs e)  //обновление данных при выходе из игры
        {
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            string time = stopwatch.Ticks.ToString();
            SQLiteCommand com = new SQLiteCommand("UPDATE Info SET gameTime='" + time + "'", sqlite);//время
            com.ExecuteNonQuery();
            com = new SQLiteCommand("UPDATE Info SET benzCost='" + int.Parse(lb_benzCost.Text) + "'", sqlite); //цена бензина
            com.ExecuteNonQuery();
            com = new SQLiteCommand("UPDATE Info SET botZabastovka='"+lb_botZab.Text+"'",sqlite); //забастовка бота
            com.ExecuteNonQuery();
            com = new SQLiteCommand("UPDATE Info SET myBudget='" + int.Parse(lb_budget.Text) + "'", sqlite);//бюджет
            com.ExecuteNonQuery();
            com = new SQLiteCommand("UPDATE Info set myImidg='" + int.Parse(lb_imidg.Text) + "'", sqlite);//имидж
            com.ExecuteNonQuery();
            com = new SQLiteCommand("UPDATE Info set myZabastovka='" + lb_myZab.Text + "'", sqlite);//забастовка моя
            com.ExecuteNonQuery();
            sqlite.Close();
            Application.Exit();
        }

        private void btn_shop_Click(object sender, EventArgs e)
        {
            Shop FShop = new Shop(this);
            FShop.ShowDialog();
        }

        private void UserPlay_FormClosing(object sender, FormClosingEventArgs e) //обновление данных при выходе из игры
        {
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            string time = stopwatch.Ticks.ToString();
            SQLiteCommand com = new SQLiteCommand("UPDATE Info SET gameTime='" + time + "'", sqlite);//время
            com.ExecuteNonQuery();
            com = new SQLiteCommand("UPDATE Info SET benzCost='" + int.Parse(lb_benzCost.Text) + "'", sqlite); //цена бензина
            com.ExecuteNonQuery();
            com = new SQLiteCommand("UPDATE Info SET botZabastovka='" + lb_botZab.Text + "'", sqlite); //забастовка бота
            com.ExecuteNonQuery();
            com = new SQLiteCommand("UPDATE Info SET myBudget='" + int.Parse(lb_budget.Text) + "'", sqlite);//бюджет
            com.ExecuteNonQuery();
            com = new SQLiteCommand("UPDATE Info set myImidg='" + int.Parse(lb_imidg.Text) + "'", sqlite);//имидж
            com.ExecuteNonQuery();
            com = new SQLiteCommand("UPDATE Info set myZabastovka='" + lb_myZab.Text + "'", sqlite);//забастовка моя
            com.ExecuteNonQuery();
            sqlite.Close();
            Application.Exit();
        }
        public void refresh_my_Buypass_planes() //обновление моих купленных пасс. самолетов
        {
            listB_pass_buy_planes.Items.Clear();
            BuyPassPlanes.Clear();
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            SQLiteDataAdapter com = new SQLiteDataAdapter("SELECT BuyPlanes.id, name, currentLocation, maxDistance, speed, vzlets, capacity, needWorkers, sum(UseWorkers.countWorkers), purchasePrice FROM UseWorkers, Planes, PassengerPlanes, BuyPlanes WHERE Planes.id=PassengerPlanes.idPlane and PassengerPlanes.idPlane = BuyPlanes.idPlane and UseWorkers.typeVladSam = '2' and UseWorkers.idSamASC=BuyPlanes.id Group By BuyPlanes.id", sqlite);
            com.Fill(BuyPassPlanes); //считали купленные пассажирские самолеты из БД и занесли в таблицу passPlanes
            for (int i = 0; i < BuyPassPlanes.Rows.Count; i++)
            {
                DataRow dr_BuyPassPlanes = BuyPassPlanes.Rows[i];
                listB_pass_buy_planes.Items.Add(dr_BuyPassPlanes["name"].ToString());
            }
        }

        public void refresh_my_Buycargo_planes() //обновление моих купленных груз. самолетов
        {
            listB_cargo_buy_planes.Items.Clear();
            BuyCargoPlanes.Clear();
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            SQLiteDataAdapter com = new SQLiteDataAdapter("SELECT BuyPlanes.id, name, currentLocation, maxDistance, speed, vzlets, capacity, needWorkers, sum(UseWorkers.countWorkers), purchasePrice FROM UseWorkers, Planes, CargoPlanes, BuyPlanes WHERE Planes.id=CargoPlanes.idPlane and CargoPlanes.idPlane = BuyPlanes.idPlane and UseWorkers.typeVladSam = '2' and UseWorkers.idSamASC=BuyPlanes.id Group By BuyPlanes.id", sqlite);
            com.Fill(BuyCargoPlanes); //считали купленные пассажирские самолеты из БД и занесли в таблицу passPlanes
            for (int i = 0; i < BuyCargoPlanes.Rows.Count; i++)
            {
                DataRow dr_BuyCargoPlanes = BuyCargoPlanes.Rows[i];
                listB_cargo_buy_planes.Items.Add(dr_BuyCargoPlanes["name"].ToString());
            }
        }

        private void listB_pass_buy_planes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listB_pass_buy_planes.SelectedIndex > -1)
            {
                listB_takenCargo.ClearSelected();
                btn_sell.Enabled = true;
                listB_cargo_arenda_planes.ClearSelected();
                listB_cargo_buy_planes.ClearSelected();
                listB_cargo_leasing_planes.ClearSelected();
                listB_pass_arenda_planes.ClearSelected();
                listB_pass_leasing_planes.ClearSelected();
                listB_samInfo.Items.Clear();
                listB_samInfo.Items.Add("Локация: " + BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][2]);
                listB_samInfo.Items.Add("Макс. дистанция: " + BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][3]);
                listB_samInfo.Items.Add("Скорость: " + BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][4]);
                listB_samInfo.Items.Add("Полетов: " + BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][5]);
                listB_samInfo.Items.Add("Пассажировм-ть: " + BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][6]);
                listB_samInfo.Items.Add("Нужно экипажа: " + BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][7]);
                listB_samInfo.Items.Add("Закреплено экипажа: " + BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][8]);

            }
        }

        private void listB_cargo_buy_planes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listB_cargo_buy_planes.SelectedIndex>-1)
            {
                btn_sell.Enabled = true;
                listB_takenPass.ClearSelected();
            listB_cargo_arenda_planes.ClearSelected();
            listB_pass_buy_planes.ClearSelected();
            listB_cargo_leasing_planes.ClearSelected();
            listB_pass_arenda_planes.ClearSelected();
            listB_pass_leasing_planes.ClearSelected();
            listB_samInfo.Items.Clear();
            listB_samInfo.Items.Add("Локация: " + BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][2]);
            listB_samInfo.Items.Add("Макс. дистанция: " + BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][3]);
            listB_samInfo.Items.Add("Скорость: " + BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][4]);
            listB_samInfo.Items.Add("Полетов: " + BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][5]);
            listB_samInfo.Items.Add("Грузоподъемность: " + BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][6]);
            listB_samInfo.Items.Add("Нужно экипажа: " + BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][7]);
            listB_samInfo.Items.Add("Закреплено экипажа: " + BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][8]);
            }
        }

        public void refresh_my_Arendapass_planes() //обновление арендованных пасс. самолетов
        {

            listB_pass_arenda_planes.Items.Clear();
            ArendaPassPlanes.Clear();
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            SQLiteDataAdapter com = new SQLiteDataAdapter("SELECT ArendaPlanes.id, name, currentLocation, maxDistance, speed, vzlets, capacity, needWorkers, sum(UseWorkers.countWorkers), dateEndArenda FROM UseWorkers, Planes, PassengerPlanes, ArendaPlanes WHERE Planes.id=PassengerPlanes.idPlane and PassengerPlanes.idPlane = ArendaPlanes.idPlane and UseWorkers.typeVladSam = '1' and UseWorkers.idSamASC=ArendaPlanes.id Group By ArendaPlanes.id order by dateEndArenda", sqlite);
            com.Fill(ArendaPassPlanes); //считали арендованные пассажирские самолеты из БД и занесли в таблицу ArendaPlanes
            for (int i = 0; i < ArendaPassPlanes.Rows.Count; i++)
            {
                DataRow dr_ArendaPassPlanes = ArendaPassPlanes.Rows[i];
                listB_pass_arenda_planes.Items.Add(dr_ArendaPassPlanes["name"].ToString());
            }
            if (ArendaPassPlanes.Rows.Count>0)
                EndNextPassArenda=Convert.ToString(ArendaPassPlanes.Rows[0][9]);
        }
        public void refresh_my_Arendacargo_planes() //обновление арендованных груз. самолетов
        {
            listB_cargo_arenda_planes.Items.Clear();
            ArendaCargoPlanes.Clear();
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            SQLiteDataAdapter com = new SQLiteDataAdapter("SELECT ArendaPlanes.id, name, currentLocation, maxDistance, speed, vzlets, capacity, needWorkers, sum(UseWorkers.countWorkers), dateEndArenda FROM UseWorkers, Planes, CargoPlanes, ArendaPlanes WHERE Planes.id=CargoPlanes.idPlane and CargoPlanes.idPlane = ArendaPlanes.idPlane and UseWorkers.typeVladSam = '1' and UseWorkers.idSamASC=ArendaPlanes.id Group By ArendaPlanes.id order by dateEndArenda", sqlite);
            com.Fill(ArendaCargoPlanes); //считали арендованные грузовые самолеты из БД и занесли в таблицу ArendaPlanes
            for (int i = 0; i < ArendaCargoPlanes.Rows.Count; i++)
            {
                DataRow dr_ArendaCargoPlanes = ArendaCargoPlanes.Rows[i];
                listB_cargo_arenda_planes.Items.Add(dr_ArendaCargoPlanes["name"].ToString());
            }
            if (ArendaCargoPlanes.Rows.Count>0)
            EndNextCargoArenda = Convert.ToString(ArendaCargoPlanes.Rows[0][9]);
        }

        private void listB_pass_arenda_planes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listB_pass_arenda_planes.SelectedIndex > -1)
            {
                listB_takenCargo.ClearSelected();
                btn_sell.Enabled = false;
                listB_cargo_arenda_planes.ClearSelected();
                listB_pass_buy_planes.ClearSelected();
                listB_cargo_leasing_planes.ClearSelected();
                listB_cargo_buy_planes.ClearSelected();
                listB_pass_leasing_planes.ClearSelected();
                listB_samInfo.Items.Clear();
                listB_samInfo.Items.Add("Локация: " + ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][2]);
                listB_samInfo.Items.Add("Макс. дистанция: " + ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][3]);
                listB_samInfo.Items.Add("Скорость: " + ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][4]);
                listB_samInfo.Items.Add("Взлетов: " + ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][5]);
                listB_samInfo.Items.Add("Пассажировм-ть: " + ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][6]);
                listB_samInfo.Items.Add("Нужно экипажа: " + ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][7]);
                listB_samInfo.Items.Add("Закреплено экипажа: " + ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][8]);
                long SEndArenda = Convert.ToInt64(ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][9]);
                DateTime EndArenda = new DateTime(SEndArenda);
                string FEndArenda = String.Format("{0:dd.MM.yyyy HH:mm}", EndArenda);
                listB_samInfo.Items.Add("Конец аренды: " + FEndArenda);
            }
    
        }

        private void listB_cargo_arenda_planes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listB_cargo_arenda_planes.SelectedIndex > -1)
            {
                listB_takenPass.ClearSelected();
                btn_sell.Enabled = false;
                listB_pass_arenda_planes.ClearSelected();
                listB_pass_buy_planes.ClearSelected();
                listB_cargo_leasing_planes.ClearSelected();
                listB_cargo_buy_planes.ClearSelected();
                listB_pass_leasing_planes.ClearSelected();
                listB_samInfo.Items.Clear();
                listB_samInfo.Items.Add("Локация: " + ArendaCargoPlanes.Rows[listB_cargo_arenda_planes.SelectedIndex][2]);
                listB_samInfo.Items.Add("Макс. дистанция: " + ArendaCargoPlanes.Rows[listB_cargo_arenda_planes.SelectedIndex][3]);
                listB_samInfo.Items.Add("Скорость: " + ArendaCargoPlanes.Rows[listB_cargo_arenda_planes.SelectedIndex][4]);
                listB_samInfo.Items.Add("Взлетов: " + ArendaCargoPlanes.Rows[listB_cargo_arenda_planes.SelectedIndex][5]);
                listB_samInfo.Items.Add("Грузоподъемность: " + ArendaCargoPlanes.Rows[listB_cargo_arenda_planes.SelectedIndex][6]);
                listB_samInfo.Items.Add("Нужно экипажа: " + ArendaCargoPlanes.Rows[listB_cargo_arenda_planes.SelectedIndex][7]);
                listB_samInfo.Items.Add("Закреплено экипажа: " + ArendaCargoPlanes.Rows[listB_cargo_arenda_planes.SelectedIndex][8]);
                long SEndArenda = Convert.ToInt64(ArendaCargoPlanes.Rows[listB_cargo_arenda_planes.SelectedIndex][9]);
                DateTime EndArenda = new DateTime(SEndArenda);
                string FEndArenda = String.Format("{0:dd.MM.yyyy HH:mm}", EndArenda);
                listB_samInfo.Items.Add("Конец аренды: " + FEndArenda);
            }
        }

        public void refresh_my_Leasingpass_planes() //обновление взятых в лизинг пасс. самолетов
        {
            listB_pass_leasing_planes.Items.Clear();
            LeasingPassPlanes.Clear();
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            SQLiteDataAdapter com = new SQLiteDataAdapter("SELECT LeasingPlanes.id, name, currentLocation, maxDistance, speed, vzlets, capacity, needWorkers, sum(UseWorkers.countWorkers), dateEndLeasing, ostCost FROM UseWorkers, Planes, PassengerPlanes, LeasingPlanes WHERE Planes.id=PassengerPlanes.idPlane and PassengerPlanes.idPlane = LeasingPlanes.idPlane and UseWorkers.typeVladSam = '3' and UseWorkers.idSamASC=LeasingPlanes.id Group By LeasingPlanes.id order by dateEndLeasing", sqlite);
            com.Fill(LeasingPassPlanes); //считали 
            for (int i = 0; i < LeasingPassPlanes.Rows.Count; i++)
            {
                DataRow dr_LeasingPassPlanes = LeasingPassPlanes.Rows[i];
                listB_pass_leasing_planes.Items.Add(dr_LeasingPassPlanes["name"].ToString());
            }
            if (LeasingPassPlanes.Rows.Count>0)
            EndNextPassLeasing = Convert.ToString(LeasingPassPlanes.Rows[0][9]);
        }
        public void refresh_my_Leasingcargo_planes() //обновление арендованных груз. самолетов
        {
            listB_cargo_leasing_planes.Items.Clear();
            LeasingCargoPlanes.Clear();
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            SQLiteDataAdapter com = new SQLiteDataAdapter("SELECT LeasingPlanes.id, name, currentLocation, maxDistance, speed, vzlets, capacity, needWorkers, sum(UseWorkers.countWorkers), dateEndLeasing, ostCost FROM UseWorkers, Planes, CargoPlanes, LeasingPlanes WHERE Planes.id=CargoPlanes.idPlane and CargoPlanes.idPlane = LeasingPlanes.idPlane and UseWorkers.typeVladSam = '3' and UseWorkers.idSamASC=LeasingPlanes.id Group By LeasingPlanes.id order by dateEndLeasing", sqlite);
            com.Fill(LeasingCargoPlanes); //считали 
            for (int i = 0; i < LeasingCargoPlanes.Rows.Count; i++)
            {
                DataRow dr_LeasingCargoPlanes = LeasingCargoPlanes.Rows[i];
                listB_cargo_leasing_planes.Items.Add(dr_LeasingCargoPlanes["name"].ToString());
            }
            if (LeasingCargoPlanes.Rows.Count>0)
            EndNextCargoLeasing = Convert.ToString(LeasingCargoPlanes.Rows[0][9]);
        }

        private void listB_pass_leasing_planes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listB_pass_leasing_planes.SelectedIndex > -1)
            {
                listB_takenCargo.ClearSelected();
                btn_sell.Enabled = false;
                listB_cargo_arenda_planes.ClearSelected();
                listB_pass_buy_planes.ClearSelected();
                listB_cargo_leasing_planes.ClearSelected();
                listB_cargo_buy_planes.ClearSelected();
                listB_pass_arenda_planes.ClearSelected();
                listB_samInfo.Items.Clear();
                listB_samInfo.Items.Add("Локация: " + LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][2]);
                listB_samInfo.Items.Add("Макс. дистанция: " + LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][3]);
                listB_samInfo.Items.Add("Скорость: " + LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][4]);
                listB_samInfo.Items.Add("Взлетов: " + LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][5]);
                listB_samInfo.Items.Add("Пассажировмест-ть: " + LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][6]);
                listB_samInfo.Items.Add("Требуется экипажа: " + LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][7]);
                listB_samInfo.Items.Add("Закреплено экипажа: " + LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][8]);
                long SEndArenda = Convert.ToInt64(LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][9]);
                DateTime EndArenda = new DateTime(SEndArenda);
                string FEndArenda = String.Format("{0:dd.MM.yyyy HH:mm}", EndArenda);
                listB_samInfo.Items.Add("Конец аренды: " + FEndArenda);
                listB_samInfo.Items.Add("Остаточ. стоимость: " + LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][10]);
            }
        }

        private void listB_cargo_leasing_planes_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (listB_cargo_leasing_planes.SelectedIndex > -1)
            {
                listB_takenPass.ClearSelected();
                btn_sell.Enabled = false;
                listB_pass_arenda_planes.ClearSelected();
                listB_pass_buy_planes.ClearSelected();
                listB_cargo_arenda_planes.ClearSelected();
                listB_cargo_buy_planes.ClearSelected();
                listB_pass_leasing_planes.ClearSelected();
                listB_samInfo.Items.Clear();
                listB_samInfo.Items.Add("Локация: " + LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][2]);
                listB_samInfo.Items.Add("Макс. дистанция: " + LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][3]);
                listB_samInfo.Items.Add("Скорость: " + LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][4]);
                listB_samInfo.Items.Add("Взлетов: " + LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][5]);
                listB_samInfo.Items.Add("Грузоподъемность: " + LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][6]);
                listB_samInfo.Items.Add("Требуется экипажа: " + LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][7]);
                listB_samInfo.Items.Add("Закреплено экипажа: " + LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][8]);
                long SEndArenda = Convert.ToInt64(LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][9]);
                DateTime EndArenda = new DateTime(SEndArenda);
                string FEndArenda = String.Format("{0:dd.MM.yyyy HH:mm}", EndArenda);
                listB_samInfo.Items.Add("Конец аренды: " + FEndArenda);
                listB_samInfo.Items.Add("Остаточ. стоимость: " + LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][10]);

            }
        }

        private void btn_sell_Click(object sender, EventArgs e)
        {
            btn_sell.Enabled = false;
            if (listB_pass_buy_planes.SelectedIndex>-1)
            {
                if (BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][2].ToString() != "В полете")
                {
                    //5-взлетов 9-//цена покупки //0-id //1-название
                    SellSam.SelfRef.lb_id.Text = Convert.ToString(BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][0]);
                    SellSam.SelfRef.lb_nameSam.Text = Convert.ToString(BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][1]);
                    SellSam.SelfRef.lb_typeSam.Text = "Пассажирский";
                    SellSam.SelfRef.lb_stoimostSam.Text = Convert.ToString(Convert.ToInt32(BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][9]) / 2 + (200 - Convert.ToInt32(BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][5])) * Convert.ToInt32(BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][9]) / 1000);
                    SellSam.SelfRef.ShowDialog();
                    refreshWorkWorkers();
                    refreshNonWorkWorkers();
                }
                else { log.Items.Add("Отказано! Самолет находится в полете!"); log.TopIndex = log.Items.Count - 1; listB_pass_buy_planes.ClearSelected(); } 
            }
            if (listB_cargo_buy_planes.SelectedIndex > -1)
            {
                if (BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][2].ToString() != "В полете")
                {
                    //5-взлетов 9-//цена покупки //0-id //1-название
                    SellSam.SelfRef.lb_id.Text = Convert.ToString(BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][0]);
                    SellSam.SelfRef.lb_nameSam.Text = Convert.ToString(BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][1]);
                    SellSam.SelfRef.lb_typeSam.Text = "Грузовой";
                    SellSam.SelfRef.lb_stoimostSam.Text = Convert.ToString(Convert.ToInt32(BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][9]) / 2 + (200 - Convert.ToInt32(BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][5])) * Convert.ToInt32(BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][9]) / 1000);
                    SellSam.SelfRef.ShowDialog();
                    refreshWorkWorkers();
                    refreshNonWorkWorkers();

                }
                else { log.Items.Add("Отказано! Самолет находится в полете!"); log.TopIndex = log.Items.Count - 1; listB_cargo_buy_planes.ClearSelected(); }
            }

        }


        private void btn_takePass_Click(object sender, EventArgs e)//взятие пасс. рейса
        {
            btn_takePass.Enabled = false;
            if (listB_generPass.SelectedIndex>-1)
            {
                SQLiteConnection sqlite;
                sqlite = new SQLiteConnection(pathToDB);
                sqlite.Open();
                SQLiteCommand com = new SQLiteCommand("delete from TodayPassFlights where idPassFlight = '" + PassReis.Rows[listB_generPass.SelectedIndex][0] + "'", sqlite);
                com.ExecuteNonQuery(); //удалили запись из бд сегодняшних рейсов
                com = new SQLiteCommand("insert into TakenPassFlights (date, idPassFlight) values ('" + stopwatch.Date + "', '" + PassReis.Rows[listB_generPass.SelectedIndex][0] + "')", sqlite);
                com.ExecuteNonQuery();
                PassReisRefresh();
                PassReisTakenRefresh();
            }
        }
        public void PassReisTakenRefresh()//обновление инф. о взятых пасс. рейсах  (для полуночи отдельный метод, вызывается с этим)
        {
            PassTakenReis.Clear();
            listB_takenPass.Items.Clear();
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            SQLiteDataAdapter com = new SQLiteDataAdapter("select Flights.id, startFrom, destination, routeDistance, forfeit, PassengerFlights.idFlight, regularity, popularHours, minCapacity, ticketPrice,date from Flights, PassengerFlights, TakenPassFlights where Flights.id = TakenPassFlights.idPassFlight and PassengerFlights.idFlight = TakenPassFlights.idPassFlight", sqlite);
            com.Fill(PassTakenReis);
            for (int i = 0; i < PassTakenReis.Rows.Count; i++)
            {
                DataRow dr_PassTakenReis = PassTakenReis.Rows[i];
                listB_takenPass.Items.Add(dr_PassTakenReis["startFrom"].ToString() + "-" + dr_PassTakenReis["destination"].ToString());
            }
        }
        public void PassReisTakenRefreshPolnoch()//обновление инф. о взятых пасс. рейсах В ПОЛНОЧЬ
        {
            listB_takenPass.Items.Clear();
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            for (int i = 0; i < PassTakenReis.Rows.Count; i++)
            {//обновляем даты (нужно для сохранения регулярных рейсов
                if (Convert.ToString(PassTakenReis.Rows[i][10]) == stopwatch.AddDays(-1).Date.ToString())
                {
                    SQLiteCommand command = new SQLiteCommand("update TakenPassFlights set date ='" + stopwatch.AddDays(Convert.ToInt32(PassTakenReis.Rows[i][6]) - 1).Date + "'where idPassFlight='" + Convert.ToInt32(PassTakenReis.Rows[i][0]) + "'", sqlite);
                    command.ExecuteNonQuery();
                }
            }
            SQLiteCommand com = new SQLiteCommand("delete from TakenPassFlights where date='" + stopwatch.AddDays(-1).Date + "'", sqlite);
                com.ExecuteNonQuery();
                for (int i = 0; i < PassTakenReis.Rows.Count; i++)//если в конце дня в таблице есть запись о несовершенном в данную дату рейсе, то...
                    if (Convert.ToString(PassTakenReis.Rows[i][10]) == stopwatch.AddDays(-1).Date.ToString())
                        lb_budget.Text = Convert.ToString(int.Parse(lb_budget.Text) - Convert.ToInt32(PassTakenReis.Rows[i][4]));  //выплата неустоек
            PassReisTakenRefresh();
        }
        private void listB_takenPass_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listB_takenPass.SelectedIndex > -1)
            {
                listB_cargo_buy_planes.ClearSelected();
                listB_cargo_arenda_planes.ClearSelected();
                listB_cargo_leasing_planes.ClearSelected();
                listB_generPass.ClearSelected();
                listB_takenCargo.ClearSelected();
                listB_generCargo.ClearSelected();
                listB_reisInfo.Items.Clear();
                listB_reisInfo.Items.Add("Расстояние: " + PassTakenReis.Rows[listB_takenPass.SelectedIndex][3]);
                listB_reisInfo.Items.Add("Ругелярность: " + PassTakenReis.Rows[listB_takenPass.SelectedIndex][6]);
                listB_reisInfo.Items.Add("Популярные часы: " + PassTakenReis.Rows[listB_takenPass.SelectedIndex][7]);
                listB_reisInfo.Items.Add("Мин. необх. пассажировм-ть: " + PassTakenReis.Rows[listB_takenPass.SelectedIndex][8]);
                listB_reisInfo.Items.Add("Цена билета: " + PassTakenReis.Rows[listB_takenPass.SelectedIndex][9]);
                listB_reisInfo.Items.Add("Дата: " + Convert.ToString(PassTakenReis.Rows[listB_takenPass.SelectedIndex][10]).Substring(0, 10));
                listB_reisInfo.Items.Add("Неустойка: " + PassTakenReis.Rows[listB_takenPass.SelectedIndex][4]);
                if (Convert.ToString(PassTakenReis.Rows[listB_takenPass.SelectedIndex][10]) == stopwatch.Date.ToString())
                    btn_goPass.Enabled = true;
                else
                    btn_goPass.Enabled = false;
            }
            
        }

        private void btn_takeCargo_Click(object sender, EventArgs e)//взятие грузового рейса
        {
            btn_takeCargo.Enabled = false;
            if (listB_generCargo.SelectedIndex > -1)
            {
                SQLiteConnection sqlite;
                sqlite = new SQLiteConnection(pathToDB);
                sqlite.Open();
                SQLiteCommand com = new SQLiteCommand("delete from TodayCargoFlights where idCargoFlight = '" + CargoReis.Rows[listB_generCargo.SelectedIndex][0] + "'", sqlite);
                com.ExecuteNonQuery(); //удалили запись из бд сегодняшних рейсов
                com = new SQLiteCommand("insert into TakenCargoFlights (idCargoFlight) values ('" + CargoReis.Rows[listB_generCargo.SelectedIndex][0] + "')", sqlite);
                com.ExecuteNonQuery();
                CargoReisRefresh();
                CargoReisTakenRefresh();
            }
        }

        public void CargoReisTakenRefresh()//обновление инф. о взятых груз. рейсах  (для полуночи отдельный метод, вызывается с этим)
        {
            CargoTakenReis.Clear();
            listB_takenCargo.Items.Clear();
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            SQLiteDataAdapter com = new SQLiteDataAdapter("select Flights.id, startFrom, destination, routeDistance, forfeit, CargoFlights.idFlight, minLoadingCapacity, flightPayment from Flights, CargoFlights, TakenCargoFlights where Flights.id = TakenCargoFlights.idCargoFlight and CargoFlights.idFlight = TakenCargoFlights.idCargoFlight", sqlite);
            com.Fill(CargoTakenReis);
            for (int i = 0; i < CargoTakenReis.Rows.Count; i++)
            {
                DataRow dr_CargoTakenReis = CargoTakenReis.Rows[i];
                listB_takenCargo.Items.Add(dr_CargoTakenReis["startFrom"].ToString() + "-" + dr_CargoTakenReis["destination"].ToString());
            }
        }

        public void CargoReisTakenRefreshPolnoch()//обновление инф. о взятых груз. рейсах В ПОЛНОЧЬ
        {
            listB_takenCargo.Items.Clear();
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            SQLiteCommand com = new SQLiteCommand("delete from TakenCargoFlights", sqlite);
            com.ExecuteNonQuery();
            for (int i = 0; i < CargoTakenReis.Rows.Count; i++)//если в конце дня в таблице есть запись о несовершенном в данную дату рейсе, то...
                    lb_budget.Text = Convert.ToString(int.Parse(lb_budget.Text) - Convert.ToInt32(CargoTakenReis.Rows[i][4]));  //выплата неустоек
            CargoReisTakenRefresh();
        }

        private void listB_takenCargo_SelectedIndexChanged(object sender, EventArgs e)
        {
            btn_goCargo.Enabled = false;
            if (listB_takenCargo.SelectedIndex > -1)
            {
                listB_pass_buy_planes.ClearSelected();
                listB_pass_arenda_planes.ClearSelected();
                listB_pass_leasing_planes.ClearSelected();
                listB_generPass.ClearSelected();
                listB_takenPass.ClearSelected();
                listB_generCargo.ClearSelected();
                listB_reisInfo.Items.Clear();
                listB_reisInfo.Items.Add("Расстояние: " + CargoTakenReis.Rows[listB_takenCargo.SelectedIndex][3]);
                listB_reisInfo.Items.Add("Необходимая грузоподъемность: " + CargoTakenReis.Rows[listB_takenCargo.SelectedIndex][6]);
                listB_reisInfo.Items.Add("Оплата за рейс: " + CargoTakenReis.Rows[listB_takenCargo.SelectedIndex][7]);
                listB_reisInfo.Items.Add("Дата: " + Convert.ToString(stopwatch.Date).Substring(0, 10));
                listB_reisInfo.Items.Add("Неустойка: " + CargoTakenReis.Rows[listB_takenCargo.SelectedIndex][4]);
                    btn_goCargo.Enabled = true;           
            }
        }

        

        private void btn_goPass_Click(object sender, EventArgs e) //отправка  рейса БЕЗУСЛОВНАЯ //ДОДЕЛЫВАНИЕ Е
        {
            listB_samInfo.Items.Clear();
            listB_reisInfo.Items.Clear();
            btn_goPass.Enabled = false;
            if (listB_takenPass.SelectedIndex > -1)
            {                
                //PassTakenReis Flights.id, startFrom, destination, routeDistance, forfeit, PassengerFlights.idFlight, regularity, popularHours, minCapacity, ticketPrice,date
               //BuyPassPlanes BuyPlanes.id, name, currentLocation, maxDistance, speed, vzlets, capacity, needWorkers, sum(UseWorkers.countWorkers), purchasePrice
                if (listB_pass_buy_planes.SelectedIndex > -1) //если куплен
                {
                    if ((Convert.ToString(BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][2]) != Convert.ToString(PassTakenReis.Rows[listB_takenPass.SelectedIndex][1])) ||
                        (Convert.ToInt32(BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][3]) < Convert.ToInt32(PassTakenReis.Rows[listB_takenPass.SelectedIndex][3])) ||
                         (Convert.ToInt32(BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][6]) < Convert.ToInt32(PassTakenReis.Rows[listB_takenPass.SelectedIndex][8])) ||
                         (int.Parse(lb_budget.Text) < 0) ||
                         (Convert.ToInt32(BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][7]) > Convert.ToInt32(BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][8])))
                    { log.Items.Add("Рейс не отправлен. Проверьте самолет или бюджет"); log.TopIndex = log.Items.Count - 1; }
                    else
                    {
                        log.Items.Add("Рейс успешно отправлен!");
                        log.TopIndex = log.Items.Count - 1;
                        SQLiteConnection sqlite;
                        sqlite = new SQLiteConnection(pathToDB);
                        sqlite.Open();
                        SQLiteCommand com = new SQLiteCommand();
                        if (PassTakenReis.Rows[listB_takenPass.SelectedIndex][6].ToString() == "0")//если рейс разовый
                        {
                            com = new SQLiteCommand("delete from TakenPassFlights where idPassFlight = '" + PassTakenReis.Rows[listB_takenPass.SelectedIndex][0] + "'", sqlite);
                            com.ExecuteNonQuery();
                        }
                        else //если регулярный, просто обновляем дату
                        {
                            com = new SQLiteCommand("update TakenPassFlights set date = '" + stopwatch.AddDays(Convert.ToDouble(PassTakenReis.Rows[listB_takenPass.SelectedIndex][6])).Date + "'where idPassFlight = '" + PassTakenReis.Rows[listB_takenPass.SelectedIndex][0] + "'", sqlite);
                            com.ExecuteNonQuery();
                        }
                        //расстояние/скорость = время
                        double minutes = 60 * Convert.ToDouble(PassTakenReis.Rows[listB_takenPass.SelectedIndex][3]) / Convert.ToDouble(BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][4]);
                        minutes = Math.Floor(minutes); //количество минут полета
                        //добавили инфу в Таблицу расписания
                        com = new SQLiteCommand("insert into TimetablePassFlights (comeTime, typeVladSam, idFlight, idSamAsc) values ('" + stopwatch.AddMinutes(minutes).Ticks + "', '2', '" + Convert.ToInt32(PassTakenReis.Rows[listB_takenPass.SelectedIndex][0]) + "', '" + Convert.ToInt32(BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][0]) + "')", sqlite);
                        com.ExecuteNonQuery();
                        com = new SQLiteCommand("update BuyPlanes set currentLocation = 'В полете' where id='" + BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][0] + "'", sqlite);
                        com.ExecuteNonQuery();
                        com = new SQLiteCommand("update BuyPlanes set vzlets = vzlets+1 where id='" + BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][0] + "'", sqlite);
                        com.ExecuteNonQuery();
                        if ((Convert.ToString(PassTakenReis.Rows[listB_takenPass.SelectedIndex][7]) == "До 12") && (Convert.ToInt32(stopwatch.Hour) < 12) ||//часы популярности
                           (Convert.ToString(PassTakenReis.Rows[listB_takenPass.SelectedIndex][7]) == "После 12") && (Convert.ToInt32(stopwatch.Hour) > 12))
                        {
                            log.Items.Add("Рейс отправлен в час пик!");
                            log.TopIndex = log.Items.Count - 1;
                            double passengers = ((Convert.ToInt32(PassTakenReis.Rows[listB_takenPass.SelectedIndex][8]) * (2 + Math.Pow(int.Parse(lb_imidg.Text), 1.0 / 11))));
                            passengers = Math.Floor(passengers);
                            if (passengers > Convert.ToInt32(BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][6]))
                                passengers = Convert.ToInt32(BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][6]);
                            log.Items.Add("Пассажиров: " + passengers.ToString());
                            log.TopIndex = log.Items.Count - 1;
                            double dohod = Convert.ToDouble(passengers * Convert.ToInt32(PassTakenReis.Rows[listB_takenPass.SelectedIndex][9]) - 0.02 * int.Parse(lb_benzCost.Text) * Convert.ToInt32(PassTakenReis.Rows[listB_takenPass.SelectedIndex][3]) * Math.Pow(Convert.ToDouble(BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][5]) + 1, 1.0 / 11));
                            dohod = Math.Floor(dohod);
                            log.Items.Add("Выплата: " + dohod.ToString());
                            log.TopIndex = log.Items.Count - 1;
                            lb_budget.Text = Convert.ToString(int.Parse(lb_budget.Text) + dohod);
                        }
                        else
                        {
                            log.Items.Add("Рейс отправлен в невыгодное время!");
                            log.TopIndex = log.Items.Count - 1;
                            double passengers = ((Convert.ToInt32(PassTakenReis.Rows[listB_takenPass.SelectedIndex][8]) * (1 + Math.Pow(int.Parse(lb_imidg.Text), 1.0 / 11))));
                            passengers = Math.Floor(passengers);
                            if (passengers > Convert.ToInt32(BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][6]))
                                passengers = Convert.ToInt32(BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][6]);
                            log.Items.Add("Пассажиров: " + passengers.ToString());
                            log.TopIndex = log.Items.Count - 1;
                            double dohod = Convert.ToDouble(passengers * Convert.ToInt32(PassTakenReis.Rows[listB_takenPass.SelectedIndex][9]) - 0.02 * int.Parse(lb_benzCost.Text) * Convert.ToInt32(PassTakenReis.Rows[listB_takenPass.SelectedIndex][3]) * Math.Pow(Convert.ToDouble(BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][5]) + 1, 1.0 / 11));
                            dohod = Math.Floor(dohod);
                            log.Items.Add("Выплата: " + dohod.ToString());
                            log.TopIndex = log.Items.Count - 1;
                            lb_budget.Text = Convert.ToString(int.Parse(lb_budget.Text) + dohod);
                        }
                        refreshMyTimeTable();
                        PassReisTakenRefresh();
                        refresh_my_Buypass_planes();
                        refreshNonWorkWorkers();
                        refreshWorkWorkers();
                    }
                }
                if (listB_pass_arenda_planes.SelectedIndex > -1) //если арендован
                {
                    DateTime endr = new DateTime (Convert.ToInt64(ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][9]));
                    long endar = Convert.ToInt64(endr.Ticks); //время окончания аренды 
                    double minutes = 60 * Convert.ToDouble(PassTakenReis.Rows[listB_takenPass.SelectedIndex][3]) / Convert.ToDouble(ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][4]);
                        minutes = Math.Floor(minutes); //количество минут полета
                        //добавили инфу в Таблицу расписания
                        if ((Convert.ToString(ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][2]) != Convert.ToString(PassTakenReis.Rows[listB_takenPass.SelectedIndex][1])) ||
                            (Convert.ToInt32(ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][3]) < Convert.ToInt32(PassTakenReis.Rows[listB_takenPass.SelectedIndex][3])) ||
                             (Convert.ToInt32(ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][6]) < Convert.ToInt32(PassTakenReis.Rows[listB_takenPass.SelectedIndex][8])) ||
                             (int.Parse(lb_budget.Text) < 0) ||
                             (Convert.ToInt32(ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][7]) > Convert.ToInt32(ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][8])) ||
                             (Convert.ToInt64(stopwatch.AddMinutes(minutes).Ticks) >= endar)) //конец ареды наступает раньше прилета

                        { log.Items.Add("Рейс не отправлен. Проверьте самолет или бюджет"); log.TopIndex = log.Items.Count - 1; }
                        else
                        {
                            log.Items.Add("Рейс успешно отправлен!");
                            log.TopIndex = log.Items.Count - 1;
                            SQLiteConnection sqlite;
                            sqlite = new SQLiteConnection(pathToDB);
                            sqlite.Open();
                            SQLiteCommand com = new SQLiteCommand();
                            if (PassTakenReis.Rows[listB_takenPass.SelectedIndex][6].ToString() == "0")//если рейс разовый
                            {
                                com = new SQLiteCommand("delete from TakenPassFlights where idPassFlight = '" + PassTakenReis.Rows[listB_takenPass.SelectedIndex][0] + "'", sqlite);
                                com.ExecuteNonQuery();
                            }
                            else //если регулярный, просто обновляем дату
                            {
                                com = new SQLiteCommand("update TakenPassFlights set date = '" + stopwatch.AddDays(Convert.ToDouble(PassTakenReis.Rows[listB_takenPass.SelectedIndex][6])).Date + "'where idPassFlight = '" + PassTakenReis.Rows[listB_takenPass.SelectedIndex][0] + "'", sqlite);
                                com.ExecuteNonQuery();
                            }
                            //расстояние/скорость = время

                            com = new SQLiteCommand("insert into TimetablePassFlights (comeTime, typeVladSam, idFlight, idSamAsc) values ('" + stopwatch.AddMinutes(minutes).Ticks + "', '1', '" + Convert.ToInt32(PassTakenReis.Rows[listB_takenPass.SelectedIndex][0]) + "', '" + Convert.ToInt32(ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][0]) + "')", sqlite);
                            com.ExecuteNonQuery();
                            com = new SQLiteCommand("update ArendaPlanes set currentLocation = 'В полете' where id='" + ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][0] + "'", sqlite);
                            com.ExecuteNonQuery();
                            com = new SQLiteCommand("update ArendaPlanes set vzlets = vzlets+1 where id='" + ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][0] + "'", sqlite);
                            com.ExecuteNonQuery();
                            if ((Convert.ToString(PassTakenReis.Rows[listB_takenPass.SelectedIndex][7]) == "До 12") && (Convert.ToInt32(stopwatch.Hour) < 12) ||//часы популярности
                               (Convert.ToString(PassTakenReis.Rows[listB_takenPass.SelectedIndex][7]) == "После 12") && (Convert.ToInt32(stopwatch.Hour) > 12))
                            {
                                log.Items.Add("Рейс отправлен в час пик!");
                                log.TopIndex = log.Items.Count - 1;
                                double passengers = ((Convert.ToInt32(PassTakenReis.Rows[listB_takenPass.SelectedIndex][8]) * (2 + Math.Pow(int.Parse(lb_imidg.Text), 1.0 / 11))));
                                passengers = Math.Floor(passengers);
                                if (passengers > Convert.ToInt32(ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][6]))
                                    passengers = Convert.ToInt32(ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][6]);
                                log.Items.Add("Пассажиров: " + passengers.ToString());
                                log.TopIndex = log.Items.Count - 1;
                                double dohod = Convert.ToDouble(passengers * Convert.ToInt32(PassTakenReis.Rows[listB_takenPass.SelectedIndex][9]) - 0.02 * int.Parse(lb_benzCost.Text) * Convert.ToInt32(PassTakenReis.Rows[listB_takenPass.SelectedIndex][3]) * Math.Pow(Convert.ToDouble(ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][5]) + 1, 1.0 / 11));
                                dohod = Math.Floor(dohod);
                                log.Items.Add("Выплата: " + dohod.ToString());
                                log.TopIndex = log.Items.Count - 1;
                                lb_budget.Text = Convert.ToString(int.Parse(lb_budget.Text) + dohod);
                            }
                            else
                            {
                                log.Items.Add("Рейс отправлен в невыгодное время!");
                                log.TopIndex = log.Items.Count - 1;
                                double passengers = ((Convert.ToInt32(PassTakenReis.Rows[listB_takenPass.SelectedIndex][8]) * (1 + Math.Pow(int.Parse(lb_imidg.Text), 1.0 / 11))));
                                passengers = Math.Floor(passengers);
                                if (passengers > Convert.ToInt32(ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][6]))
                                    passengers = Convert.ToInt32(ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][6]);
                                log.Items.Add("Пассажиров: " + passengers.ToString());
                                log.TopIndex = log.Items.Count - 1;
                                double dohod = Convert.ToDouble(passengers * Convert.ToInt32(PassTakenReis.Rows[listB_takenPass.SelectedIndex][9]) - 0.02 * int.Parse(lb_benzCost.Text) * Convert.ToInt32(PassTakenReis.Rows[listB_takenPass.SelectedIndex][3]) * Math.Pow(Convert.ToDouble(ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][5]) + 1, 1.0 / 11));
                                dohod = Math.Floor(dohod);
                                log.Items.Add("Выплата: " + dohod.ToString());
                                log.TopIndex = log.Items.Count - 1;
                                lb_budget.Text = Convert.ToString(int.Parse(lb_budget.Text) + dohod);
                            }
                            refreshMyTimeTable();
                            PassReisTakenRefresh();
                            refresh_my_Arendapass_planes();
                            refreshNonWorkWorkers();
                            refreshWorkWorkers();
                        }
                }
                if (listB_pass_leasing_planes.SelectedIndex > -1) //если лизинг
                {
                    DateTime endr = new DateTime(Convert.ToInt64(LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][9]));
                    long endar = Convert.ToInt64(endr.Ticks); //время окончания аренды 
                    double minutes = 60 * Convert.ToDouble(PassTakenReis.Rows[listB_takenPass.SelectedIndex][3]) / Convert.ToDouble(LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][4]);
                    minutes = Math.Floor(minutes); //количество минут полета
                    //добавили инфу в Таблицу расписания
                    if ((Convert.ToString(LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][2]) != Convert.ToString(PassTakenReis.Rows[listB_takenPass.SelectedIndex][1])) ||
                        (Convert.ToInt32(LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][3]) < Convert.ToInt32(PassTakenReis.Rows[listB_takenPass.SelectedIndex][3])) ||
                         (Convert.ToInt32(LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][6]) < Convert.ToInt32(PassTakenReis.Rows[listB_takenPass.SelectedIndex][8])) ||
                         (int.Parse(lb_budget.Text) < 0) ||
                         (Convert.ToInt32(LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][7]) > Convert.ToInt32(LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][8])) ||
                         (Convert.ToInt64(stopwatch.AddMinutes(minutes).Ticks) >= endar)) //конец ареды наступает раньше прилета

                    { log.Items.Add("Рейс не отправлен. Проверьте самолет или бюджет"); log.TopIndex = log.Items.Count - 1; }
                    else
                    {
                        log.Items.Add("Рейс успешно отправлен!");
                        log.TopIndex = log.Items.Count - 1;
                        SQLiteConnection sqlite;
                        sqlite = new SQLiteConnection(pathToDB);
                        sqlite.Open();
                        SQLiteCommand com = new SQLiteCommand();
                        if (PassTakenReis.Rows[listB_takenPass.SelectedIndex][6].ToString() == "0")//если рейс разовый
                        {
                            com = new SQLiteCommand("delete from TakenPassFlights where idPassFlight = '" + PassTakenReis.Rows[listB_takenPass.SelectedIndex][0] + "'", sqlite);
                            com.ExecuteNonQuery();
                        }
                        else //если регулярный, просто обновляем дату
                        {
                            com = new SQLiteCommand("update TakenPassFlights set date = '" + stopwatch.AddDays(Convert.ToDouble(PassTakenReis.Rows[listB_takenPass.SelectedIndex][6])).Date + "'where idPassFlight = '" + PassTakenReis.Rows[listB_takenPass.SelectedIndex][0] + "'", sqlite);
                            com.ExecuteNonQuery();
                        }
                        //расстояние/скорость = время

                        com = new SQLiteCommand("insert into TimetablePassFlights (comeTime, typeVladSam, idFlight, idSamAsc) values ('" + stopwatch.AddMinutes(minutes).Ticks + "', '3', '" + Convert.ToInt32(PassTakenReis.Rows[listB_takenPass.SelectedIndex][0]) + "', '" + Convert.ToInt32(LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][0]) + "')", sqlite);
                        com.ExecuteNonQuery();
                        com = new SQLiteCommand("update LeasingPlanes set currentLocation = 'В полете' where id='" + LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][0] + "'", sqlite);
                        com.ExecuteNonQuery();
                        com = new SQLiteCommand("update LeasingPlanes set vzlets = vzlets+1 where id='" + LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][0] + "'", sqlite);
                        com.ExecuteNonQuery();
                        if ((Convert.ToString(PassTakenReis.Rows[listB_takenPass.SelectedIndex][7]) == "До 12") && (Convert.ToInt32(stopwatch.Hour) < 12) ||//часы популярности
                           (Convert.ToString(PassTakenReis.Rows[listB_takenPass.SelectedIndex][7]) == "После 12") && (Convert.ToInt32(stopwatch.Hour) > 12))
                        {
                            log.Items.Add("Рейс отправлен в час пик!");
                            log.TopIndex = log.Items.Count - 1;
                            double passengers = ((Convert.ToInt32(PassTakenReis.Rows[listB_takenPass.SelectedIndex][8]) * (2 + Math.Pow(int.Parse(lb_imidg.Text), 1.0 / 11))));
                            passengers = Math.Floor(passengers);
                            if (passengers > Convert.ToInt32(LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][6]))
                                passengers = Convert.ToInt32(LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][6]);
                            log.Items.Add("Пассажиров: " + passengers.ToString());
                            log.TopIndex = log.Items.Count - 1;
                            double dohod = Convert.ToDouble(passengers * Convert.ToInt32(PassTakenReis.Rows[listB_takenPass.SelectedIndex][9]) - 0.02 * int.Parse(lb_benzCost.Text) * Convert.ToInt32(PassTakenReis.Rows[listB_takenPass.SelectedIndex][3]) * Math.Pow(Convert.ToDouble(LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][5]) + 1, 1.0 / 11));
                            dohod = Math.Floor(dohod);
                            log.Items.Add("Выплата: " + dohod.ToString());
                            log.TopIndex = log.Items.Count - 1;
                            lb_budget.Text = Convert.ToString(int.Parse(lb_budget.Text) + dohod);
                        }
                        else
                        {
                            log.Items.Add("Рейс отправлен в невыгодное время!");
                            log.TopIndex = log.Items.Count - 1;
                            double passengers = ((Convert.ToInt32(PassTakenReis.Rows[listB_takenPass.SelectedIndex][8]) * (1 + Math.Pow(int.Parse(lb_imidg.Text), 1.0 / 11))));
                            passengers = Math.Floor(passengers);
                            if (passengers > Convert.ToInt32(LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][6]))
                                passengers = Convert.ToInt32(LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][6]);
                            log.Items.Add("Пассажиров: " + passengers.ToString());
                            log.TopIndex = log.Items.Count - 1;
                            double dohod = Convert.ToDouble(passengers * Convert.ToInt32(PassTakenReis.Rows[listB_takenPass.SelectedIndex][9]) - 0.02 * int.Parse(lb_benzCost.Text) * Convert.ToInt32(PassTakenReis.Rows[listB_takenPass.SelectedIndex][3]) * Math.Pow(Convert.ToDouble(LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][5]) + 1, 1.0 / 11));
                            dohod = Math.Floor(dohod);
                            log.Items.Add("Выплата: " + dohod.ToString());
                            log.TopIndex = log.Items.Count - 1;
                            lb_budget.Text = Convert.ToString(int.Parse(lb_budget.Text) + dohod);
                        }
                        refreshMyTimeTable();
                        PassReisTakenRefresh();
                        refresh_my_Leasingpass_planes();
                        refreshNonWorkWorkers();
                        refreshWorkWorkers();//ПРОДОЛЖИТЬ ДЕЛАТЬ  ТУТ
                    }
                }
                
            }
        }

        public void refreshMyTimeTable()//обновление расписания
        {
            listB_samInfo.Items.Clear();
            listB_reisInfo.Items.Clear();
            listB_timeTable.Items.Clear();
            Timetable.Clear();
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();  
            SQLiteDataAdapter com = new SQLiteDataAdapter("select * from TimetablePassFlights, Flights where TimetablePassFlights.idFlight=Flights.id union all select * from TimetableCargoFlights, Flights where TimetableCargoFlights.idFlight=Flights.id order by ComeTime", sqlite);
            com.Fill(Timetable);
            for (int i = 0; i < Timetable.Rows.Count; i++)
            {
                DataRow dr_timetable = Timetable.Rows[i];
                DateTime EndTimeReis = new DateTime(Convert.ToInt64(dr_timetable["comeTime"]));
                listB_timeTable.Items.Add(dr_timetable["startFrom"].ToString() +"-"+ dr_timetable["destination"].ToString()+"        -        "+EndTimeReis.ToString());
            }
            if (Timetable.Rows.Count >0)
            {
                DateTime endr = new DateTime(Convert.ToInt64(Timetable.Rows[0][1]));
                EndNextReis = endr.Ticks.ToString(); //конец ближайшего рейса
            }
        }

        public void refreshRynokWorkers()//обновление рынка сотрудников
        {
            listB_infoWorkers.Items.Clear();
            RynokWorkers.Clear();
            listB_rynokWorkers.Items.Clear();
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            SQLiteDataAdapter com = new SQLiteDataAdapter("select id, zp, countWorkers from Workers where countWorkers>0", sqlite);
            com.Fill(RynokWorkers);
            for (int i = 0; i < RynokWorkers.Rows.Count; i++)
            {
                DataRow dr_RynokWorkers = RynokWorkers.Rows[i];
                listB_rynokWorkers.Items.Add("Тип " + (i + 1).ToString());
            }
        }
        private void listB_rynokWorkers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listB_rynokWorkers.SelectedIndex > -1)
            {
                listB_workWorkers.ClearSelected();
                listB_nonworkWorkers.ClearSelected();
                listB_infoWorkers.Items.Clear();
                listB_infoWorkers.Items.Add("Зарплата: " + RynokWorkers.Rows[listB_rynokWorkers.SelectedIndex][1]);
                listB_infoWorkers.Items.Add("Осталось: " + RynokWorkers.Rows[listB_rynokWorkers.SelectedIndex][2]);
            }
        }
        public void refreshWorkWorkers()// обновление моих работающих сотрудников
        {
            listB_infoWorkers.Items.Clear();
            WorkWorkers.Clear();
            listB_workWorkers.Items.Clear();
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            SQLiteDataAdapter com = new SQLiteDataAdapter("select UseWorkers.id, zabastovka, payment, idWorker, UseWorkers.typeVladSam, idSamASC, countWorkers,currentLocation from UseWorkers,BuyPlanes where UseWorkers.typeVladSam='2' and UseWorkers.idSamASC=BuyPlanes.id and (idWorker='1' or idWorker='2' or idWorker='3') Union all select UseWorkers.id, zabastovka, payment, idWorker, UseWorkers.typeVladSam, idSamASC, countWorkers,currentLocation from UseWorkers,ArendaPlanes where UseWorkers.typeVladSam='1' and UseWorkers.idSamASC=ArendaPlanes.id and (idWorker='1' or idWorker='2' or idWorker='3') Union all select UseWorkers.id, zabastovka, payment, idWorker, UseWorkers.typeVladSam, idSamASC, countWorkers,currentLocation from UseWorkers,LeasingPlanes where UseWorkers.typeVladSam='3' and UseWorkers.idSamASC=LeasingPlanes.id and (idWorker='1' or idWorker='2' or idWorker='3')", sqlite);
            com.Fill(WorkWorkers);
            for (int i = 0; i < WorkWorkers.Rows.Count; i++)
            {
                DataRow dr_MyWorkWorkers = WorkWorkers.Rows[i];
                listB_workWorkers.Items.Add("Сотрудник " + dr_MyWorkWorkers["id"].ToString());
            }

        }    

        public void refreshNonWorkWorkers()//обновление моих неработающих сотрудников
        {
            listB_infoWorkers.Items.Clear();
            NonWorkWorkers.Clear();
            listB_nonworkWorkers.Items.Clear();
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            SQLiteDataAdapter com = new SQLiteDataAdapter("select UseWorkers.id, zabastovka, payment, idWorker, typeVladSam, idSamASC, countWorkers from UseWorkers where typeVladSam=0 and ((idWorker='1') or (idWorker='2') or (idWorker='3'))", sqlite);
            com.Fill(NonWorkWorkers);
            for (int i = 0; i < NonWorkWorkers.Rows.Count; i++)
            {
                DataRow dr_MyNonWorkWorkers = NonWorkWorkers.Rows[i];
                listB_nonworkWorkers.Items.Add("Сотрудник " + dr_MyNonWorkWorkers["id"].ToString());
            }

        }
        private void btn_buyWorker_Click(object sender, EventArgs e) //наем сотрудника
        {
            if (listB_rynokWorkers.SelectedIndex > -1)
            {
                SQLiteConnection sqlite;
                sqlite = new SQLiteConnection(pathToDB);
                sqlite.Open();
                SQLiteCommand com = new SQLiteCommand("insert into UseWorkers (zabastovka, payment, idWorker, typeVladSam, idSamASC, countWorkers) values('0', '" + RynokWorkers.Rows[listB_rynokWorkers.SelectedIndex][1] + "','" + RynokWorkers.Rows[listB_rynokWorkers.SelectedIndex][0] + "', '0','0','1')", sqlite);
                com.ExecuteNonQuery();
                com = new SQLiteCommand("update Workers set countWorkers=countWorkers-1 where id ='" + RynokWorkers.Rows[listB_rynokWorkers.SelectedIndex][0] + "'", sqlite);
                com.ExecuteNonQuery();
                if (RynokWorkers.Rows[listB_rynokWorkers.SelectedIndex][0].ToString() == "1")
                    lb_imidg.Text = Convert.ToString(int.Parse(lb_imidg.Text) + 100);
                else
                    lb_imidg.Text = Convert.ToString(int.Parse(lb_imidg.Text) - 100);
                refreshRynokWorkers();
                refreshWorkWorkers();
                refreshNonWorkWorkers();
                log.Items.Add("Сотрудник нанят!");
                log.TopIndex = log.Items.Count - 1;
                
            }
        }
        private void listB_workWorkers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listB_workWorkers.SelectedIndex > -1)
            {
                listB_rynokWorkers.ClearSelected();
                listB_nonworkWorkers.ClearSelected();
                listB_infoWorkers.Items.Clear();
                //listB_infoWorkers.Items.Add("Забастовка: " + WorkWorkers.Rows[listB_workWorkers.SelectedIndex][1]);
                listB_infoWorkers.Items.Add("Зарплата: " + WorkWorkers.Rows[listB_workWorkers.SelectedIndex][2]);
                listB_infoWorkers.Items.Add("Локация: " + WorkWorkers.Rows[listB_workWorkers.SelectedIndex][7]);
            }
           
        }
        private void listB_nonworkWorkers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listB_nonworkWorkers.SelectedIndex > -1)
            {
                listB_rynokWorkers.ClearSelected();
                listB_workWorkers.ClearSelected();
                listB_infoWorkers.Items.Clear();
                //listB_infoWorkers.Items.Add("Забастовка: " + NonWorkWorkers.Rows[listB_nonworkWorkers.SelectedIndex][1]);
                listB_infoWorkers.Items.Add("Зарплата: " + NonWorkWorkers.Rows[listB_nonworkWorkers.SelectedIndex][2]);
                listB_infoWorkers.Items.Add("Локация: Пермь");
            }
        }

        private void btn_uval_Click(object sender, EventArgs e)
        {
            listB_samInfo.Items.Clear();
            if (listB_workWorkers.SelectedIndex > -1)
            {

                if (WorkWorkers.Rows[listB_workWorkers.SelectedIndex][7].ToString() == "Пермь")
                {
                    SQLiteConnection sqlite;
                    sqlite = new SQLiteConnection(pathToDB);
                    sqlite.Open();

                    SQLiteCommand com = new SQLiteCommand("delete from UseWorkers where id = '" + WorkWorkers.Rows[listB_workWorkers.SelectedIndex][0] + "'", sqlite);
                    com.ExecuteNonQuery();
                    com = new SQLiteCommand("update Workers set countWorkers=countWorkers+1 where id ='" + WorkWorkers.Rows[listB_workWorkers.SelectedIndex][3] + "'", sqlite);
                    com.ExecuteNonQuery();
                    if (WorkWorkers.Rows[listB_workWorkers.SelectedIndex][3].ToString() == "1")
                        lb_imidg.Text = Convert.ToString(int.Parse(lb_imidg.Text) - 100);
                    else
                        lb_imidg.Text = Convert.ToString(int.Parse(lb_imidg.Text) + 100);
                    refreshRynokWorkers();
                    refreshWorkWorkers();
                    refreshNonWorkWorkers();
                    refresh_my_Buypass_planes(); //обновление купленных пасс. самолетов
                    refresh_my_Buycargo_planes(); //обновление купленных груз. самолетов
                    refresh_my_Arendacargo_planes(); //обновление арендованных груз. самолетов
                    refresh_my_Arendapass_planes(); // обновление арендованных пасс. самолетов
                    refresh_my_Leasingcargo_planes();//обновление лизинг самолетов
                    refresh_my_Leasingpass_planes();//обновление лизинг самолетов
                    log.Items.Add("Сотрудник уволен");
                    log.TopIndex = log.Items.Count - 1;


                }
                else
                { log.Items.Add("Отказано! Сотрудник должен находится в Перми!"); log.TopIndex = log.Items.Count - 1; }
                
            }
            else if (listB_nonworkWorkers.SelectedIndex > -1)
            {
                SQLiteConnection sqlite;
                sqlite = new SQLiteConnection(pathToDB);
                sqlite.Open();
                SQLiteCommand com = new SQLiteCommand("delete from UseWorkers where id = '" + NonWorkWorkers.Rows[listB_nonworkWorkers.SelectedIndex][0] + "'", sqlite);
                com.ExecuteNonQuery();
                com = new SQLiteCommand("update Workers set countWorkers=countWorkers+1 where id ='" + NonWorkWorkers.Rows[listB_nonworkWorkers.SelectedIndex][3] + "'", sqlite);
                if (NonWorkWorkers.Rows[listB_nonworkWorkers.SelectedIndex][3].ToString() == "1")
                    lb_imidg.Text = Convert.ToString(int.Parse(lb_imidg.Text) - 100);
                else
                    lb_imidg.Text = Convert.ToString(int.Parse(lb_imidg.Text) + 100);
                com.ExecuteNonQuery();
                refreshRynokWorkers();
                refreshWorkWorkers();
                refreshNonWorkWorkers();
                refresh_my_Buypass_planes(); //обновление купленных пасс. самолетов
                refresh_my_Buycargo_planes(); //обновление купленных груз. самолетов
                refresh_my_Arendacargo_planes(); //обновление арендованных груз. самолетов
                refresh_my_Arendapass_planes(); // обновление арендованных пасс. самолетов
                refresh_my_Leasingcargo_planes();//обновление лизинг самолетов
                refresh_my_Leasingpass_planes();//обновление лизинг самолетов
                log.Items.Add("Сотрудник уволен");
                log.TopIndex = log.Items.Count - 1;
            }
        }

        private void btn_changeWorker_Click(object sender, EventArgs e) //работа с сотрудниками
        {
            listB_samInfo.Items.Clear();
            if (listB_workWorkers.SelectedIndex > -1) //если работает
            {

                if (WorkWorkers.Rows[listB_workWorkers.SelectedIndex][7].ToString() == "Пермь")
                {
                    SQLiteConnection sqlite;
                    sqlite = new SQLiteConnection(pathToDB);
                    sqlite.Open();
                    SQLiteCommand com = new SQLiteCommand("update UseWorkers set typeVladSam=0, idSamASC=0 where id='" + WorkWorkers.Rows[listB_workWorkers.SelectedIndex][0] + "'", sqlite);
                    com.ExecuteNonQuery();
                    refreshRynokWorkers();
                    refreshWorkWorkers();
                    refreshNonWorkWorkers();
                    refresh_my_Buypass_planes(); //обновление купленных пасс. самолетов
                    refresh_my_Buycargo_planes(); //обновление купленных груз. самолетов
                    refresh_my_Arendacargo_planes(); //обновление арендованных груз. самолетов
                    refresh_my_Arendapass_planes(); // обновление арендованных пасс. самолетов
                    refresh_my_Leasingcargo_planes();//обновление лизинг самолетов
                    refresh_my_Leasingpass_planes();//обновление лизинг самолетов
                    log.Items.Add("Сотрудник снят с должности");
                    log.TopIndex = log.Items.Count - 1;
                }
                else
                { log.Items.Add("Отказано! Сотрудник должен находится в Перми!"); log.TopIndex = log.Items.Count - 1; }
            }
            if (listB_nonworkWorkers.SelectedIndex>-1) //если не работает
            {
                if (listB_pass_buy_planes.SelectedIndex>-1)//пасс самолеты //7-need 8-use
                {
                    if (Convert.ToInt32(BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][7]) != Convert.ToInt32(BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][8]))
                    {
                        SQLiteConnection sqlite;
                        sqlite = new SQLiteConnection(pathToDB);
                        sqlite.Open();
                        SQLiteCommand com = new SQLiteCommand("update UseWorkers set typeVladSam='2', idSamASC='" + BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][0] + "'where id = '" + NonWorkWorkers.Rows[listB_nonworkWorkers.SelectedIndex][0] + "'", sqlite);
                        com.ExecuteNonQuery();
                        refreshRynokWorkers();
                        refreshWorkWorkers();
                        refreshNonWorkWorkers();
                        refresh_my_Buypass_planes(); //обновление купленных пасс. самолетов
                        refresh_my_Buycargo_planes(); //обновление купленных груз. самолетов
                        refresh_my_Arendacargo_planes(); //обновление арендованных груз. самолетов
                        refresh_my_Arendapass_planes(); // обновление арендованных пасс. самолетов
                        refresh_my_Leasingcargo_planes();//обновление лизинг самолетов
                        refresh_my_Leasingpass_planes();//обновление лизинг самолетов
                        log.Items.Add("Должность обновлена");
                        log.TopIndex = log.Items.Count - 1;
                    }
                    else
                    {
                        { log.Items.Add("На данный самолет сотрудники не нужны"); log.TopIndex = log.Items.Count - 1; }
                    }
                }
                if (listB_pass_arenda_planes.SelectedIndex > -1)//пасс самолеты //7-need 8-use
                {
                    if (Convert.ToInt32(ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][7]) != Convert.ToInt32(ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][8]))
                    {
                        SQLiteConnection sqlite;
                        sqlite = new SQLiteConnection(pathToDB);
                        sqlite.Open();
                        SQLiteCommand com = new SQLiteCommand("update UseWorkers set typeVladSam='1', idSamASC='" + ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][0] + "'where id = '" + NonWorkWorkers.Rows[listB_nonworkWorkers.SelectedIndex][0] + "'", sqlite);
                        com.ExecuteNonQuery();
                        refreshRynokWorkers();
                        refreshWorkWorkers();
                        refreshNonWorkWorkers();
                        refresh_my_Buypass_planes(); //обновление купленных пасс. самолетов
                        refresh_my_Buycargo_planes(); //обновление купленных груз. самолетов
                        refresh_my_Arendacargo_planes(); //обновление арендованных груз. самолетов
                        refresh_my_Arendapass_planes(); // обновление арендованных пасс. самолетов
                        refresh_my_Leasingcargo_planes();//обновление лизинг самолетов
                        refresh_my_Leasingpass_planes();//обновление лизинг самолетов
                        log.Items.Add("Должность обновлена");
                        log.TopIndex = log.Items.Count - 1;
                    }
                    else
                    {
                        log.Items.Add("На данный самолет сотрудники не нужны");
                        log.TopIndex = log.Items.Count - 1;
                    }
                }
                if (listB_pass_leasing_planes.SelectedIndex > -1)//пасс самолеты //7-need 8-use
                {
                    if (Convert.ToInt32(LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][7]) != Convert.ToInt32(LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][8]))
                    {
                        SQLiteConnection sqlite;
                        sqlite = new SQLiteConnection(pathToDB);
                        sqlite.Open();
                        SQLiteCommand com = new SQLiteCommand("update UseWorkers set typeVladSam='3', idSamASC='" + LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][0] + "'where id = '" + NonWorkWorkers.Rows[listB_nonworkWorkers.SelectedIndex][0] + "'", sqlite);
                        com.ExecuteNonQuery();
                        refreshRynokWorkers();
                        refreshWorkWorkers();
                        refreshNonWorkWorkers();
                        refresh_my_Buypass_planes(); //обновление купленных пасс. самолетов
                        refresh_my_Buycargo_planes(); //обновление купленных груз. самолетов
                        refresh_my_Arendacargo_planes(); //обновление арендованных груз. самолетов
                        refresh_my_Arendapass_planes(); // обновление арендованных пасс. самолетов
                        refresh_my_Leasingcargo_planes();//обновление лизинг самолетов
                        refresh_my_Leasingpass_planes();//обновление лизинг самолетов
                        log.Items.Add("Должность обновлена");
                        log.TopIndex = log.Items.Count - 1;
                    }
                    else
                    {
                        log.Items.Add("На данный самолет сотрудники не нужны");
                        log.TopIndex = log.Items.Count - 1;
                    }
                }
                if (listB_cargo_buy_planes.SelectedIndex > -1)//пасс самолеты //7-need 8-use
                {
                    if (Convert.ToInt32(BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][7]) != Convert.ToInt32(BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][8]))
                    {
                        SQLiteConnection sqlite;
                        sqlite = new SQLiteConnection(pathToDB);
                        sqlite.Open();
                        SQLiteCommand com = new SQLiteCommand("update UseWorkers set typeVladSam='2', idSamASC='" + BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][0] + "'where id = '" + NonWorkWorkers.Rows[listB_nonworkWorkers.SelectedIndex][0] + "'", sqlite);
                        com.ExecuteNonQuery();
                        refreshRynokWorkers();
                        refreshWorkWorkers();
                        refreshNonWorkWorkers();
                        refresh_my_Buypass_planes(); //обновление купленных пасс. самолетов
                        refresh_my_Buycargo_planes(); //обновление купленных груз. самолетов
                        refresh_my_Arendacargo_planes(); //обновление арендованных груз. самолетов
                        refresh_my_Arendapass_planes(); // обновление арендованных пасс. самолетов
                        refresh_my_Leasingcargo_planes();//обновление лизинг самолетов
                        refresh_my_Leasingpass_planes();//обновление лизинг самолетов
                        log.Items.Add("Должность обновлена");
                        log.TopIndex = log.Items.Count - 1;
                    }
                    else
                    {
                        log.Items.Add("На данный самолет сотрудники не нужны");
                        log.TopIndex = log.Items.Count - 1;
                    }
                }
                if (listB_cargo_arenda_planes.SelectedIndex > -1)//пасс самолеты //7-need 8-use
                {
                    if (Convert.ToInt32(ArendaCargoPlanes.Rows[listB_cargo_arenda_planes.SelectedIndex][7]) != Convert.ToInt32(ArendaCargoPlanes.Rows[listB_cargo_arenda_planes.SelectedIndex][8]))
                    {
                        SQLiteConnection sqlite;
                        sqlite = new SQLiteConnection(pathToDB);
                        sqlite.Open();
                        SQLiteCommand com = new SQLiteCommand("update UseWorkers set typeVladSam='1', idSamASC='" + ArendaCargoPlanes.Rows[listB_cargo_arenda_planes.SelectedIndex][0] + "'where id = '" + NonWorkWorkers.Rows[listB_nonworkWorkers.SelectedIndex][0] + "'", sqlite);
                        com.ExecuteNonQuery();
                        refreshRynokWorkers();
                        refreshWorkWorkers();
                        refreshNonWorkWorkers();
                        refresh_my_Buypass_planes(); //обновление купленных пасс. самолетов
                        refresh_my_Buycargo_planes(); //обновление купленных груз. самолетов
                        refresh_my_Arendacargo_planes(); //обновление арендованных груз. самолетов
                        refresh_my_Arendapass_planes(); // обновление арендованных пасс. самолетов
                        refresh_my_Leasingcargo_planes();//обновление лизинг самолетов
                        refresh_my_Leasingpass_planes();//обновление лизинг самолетов
                        log.Items.Add("Должность обновлена");
                        log.TopIndex = log.Items.Count - 1;
                    }
                    else
                    {
                        log.Items.Add("На данный самолет сотрудники не нужны");
                        log.TopIndex = log.Items.Count - 1;
                    }
                }
                if (listB_cargo_leasing_planes.SelectedIndex > -1)//пасс самолеты //7-need 8-use
                {
                    if (Convert.ToInt32(LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][7]) != Convert.ToInt32(LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][8]))
                    {
                        SQLiteConnection sqlite;
                        sqlite = new SQLiteConnection(pathToDB);
                        sqlite.Open();
                        SQLiteCommand com = new SQLiteCommand("update UseWorkers set typeVladSam='3', idSamASC='" + LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][0] + "'where id = '" + NonWorkWorkers.Rows[listB_nonworkWorkers.SelectedIndex][0] + "'", sqlite);
                        com.ExecuteNonQuery();
                        refreshRynokWorkers();
                        refreshWorkWorkers();
                        refreshNonWorkWorkers();
                        refresh_my_Buypass_planes(); //обновление купленных пасс. самолетов
                        refresh_my_Buycargo_planes(); //обновление купленных груз. самолетов
                        refresh_my_Arendacargo_planes(); //обновление арендованных груз. самолетов
                        refresh_my_Arendapass_planes(); // обновление арендованных пасс. самолетов
                        refresh_my_Leasingcargo_planes();//обновление лизинг самолетов
                        refresh_my_Leasingpass_planes();//обновление лизинг самолетов
                        log.Items.Add("Должность обновлена");
                        log.TopIndex = log.Items.Count - 1;
                    }
                    else
                    {
                        log.Items.Add("На данный самолет сотрудники не нужны");
                        log.TopIndex = log.Items.Count - 1;
                    }
                }
            }
        }

        private void btn_goCargo_Click(object sender, EventArgs e)
        {
            listB_samInfo.Items.Clear();
            listB_reisInfo.Items.Clear();
            btn_goCargo.Enabled = false;
            if (listB_takenCargo.SelectedIndex>-1)
            {
                if (listB_cargo_buy_planes.SelectedIndex > -1) //если куплен грузовой
                {
                    if ((Convert.ToString(BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][2]) != Convert.ToString(CargoTakenReis.Rows[listB_takenCargo.SelectedIndex][1])) ||
                        (Convert.ToInt32(BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][3]) < Convert.ToInt32(CargoTakenReis.Rows[listB_takenCargo.SelectedIndex][3])) ||
                         (Convert.ToInt32(BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][6]) < Convert.ToInt32(CargoTakenReis.Rows[listB_takenCargo.SelectedIndex][6])) ||
                         (int.Parse(lb_budget.Text) < 0) ||
                         (Convert.ToInt32(BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][7]) > Convert.ToInt32(BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][8])))
                    { log.Items.Add("Рейс не отправлен. Проверьте самолет или бюджет"); log.TopIndex = log.Items.Count - 1; }

                    else
                    {
                        log.Items.Add("Рейс успешно отправлен!");
                        log.TopIndex = log.Items.Count - 1;
                        SQLiteConnection sqlite;
                        sqlite = new SQLiteConnection(pathToDB);
                        sqlite.Open();
                        SQLiteCommand com = new SQLiteCommand();

                        com = new SQLiteCommand("delete from TakenCargoFlights where idCargoFlight = '" + CargoTakenReis.Rows[listB_takenCargo.SelectedIndex][0] + "'", sqlite);
                        com.ExecuteNonQuery();


                        //расстояние/скорость = время
                        double minutes = 60 * Convert.ToDouble(CargoTakenReis.Rows[listB_takenCargo.SelectedIndex][3]) / Convert.ToDouble(BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][4]);
                        minutes = Math.Floor(minutes); //количество минут полета
                        //добавили инфу в Таблицу расписания
                        com = new SQLiteCommand("insert into TimetableCargoFlights (comeTime, typeVladSam, idFlight, idSamAsc) values ('" + stopwatch.AddMinutes(minutes).Ticks + "', '2', '" + Convert.ToInt32(CargoTakenReis.Rows[listB_takenCargo.SelectedIndex][0]) + "', '" + Convert.ToInt32(BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][0]) + "')", sqlite);
                        com.ExecuteNonQuery();
                        com = new SQLiteCommand("update BuyPlanes set currentLocation = 'В полете' where id='" + BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][0] + "'", sqlite);
                        com.ExecuteNonQuery();
                        com = new SQLiteCommand("update BuyPlanes set vzlets = vzlets+1 where id='" + BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][0] + "'", sqlite);
                        com.ExecuteNonQuery();
                        int dohod = Convert.ToInt32(CargoTakenReis.Rows[listB_takenCargo.SelectedIndex][7]);
                        log.Items.Add("Выплата: " + dohod.ToString());
                        log.TopIndex = log.Items.Count - 1;
                        lb_budget.Text = Convert.ToString(int.Parse(lb_budget.Text) + dohod);
                        refreshMyTimeTable();
                        CargoReisTakenRefresh();
                        refresh_my_Buycargo_planes();
                        refreshNonWorkWorkers();
                        refreshWorkWorkers();
                    }
                }

                if (listB_cargo_arenda_planes.SelectedIndex > -1) //если арендован грузовой
                {
                    DateTime endr = new DateTime(Convert.ToInt64(ArendaCargoPlanes.Rows[listB_cargo_arenda_planes.SelectedIndex][9]));
                    long endar = Convert.ToInt64(endr.Ticks); //время окончания аренды 
                    double minutes = 60 * Convert.ToDouble(CargoTakenReis.Rows[listB_takenCargo.SelectedIndex][3]) / Convert.ToDouble(ArendaCargoPlanes.Rows[listB_cargo_arenda_planes.SelectedIndex][4]);
                    minutes = Math.Floor(minutes); //количество минут полета
                    //добавили инфу в Таблицу расписания
                    if ((Convert.ToString(ArendaCargoPlanes.Rows[listB_cargo_arenda_planes.SelectedIndex][2]) != Convert.ToString(CargoTakenReis.Rows[listB_takenCargo.SelectedIndex][1])) ||
                        (Convert.ToInt32(ArendaCargoPlanes.Rows[listB_cargo_arenda_planes.SelectedIndex][3]) < Convert.ToInt32(CargoTakenReis.Rows[listB_takenCargo.SelectedIndex][3])) ||
                         (Convert.ToInt32(ArendaCargoPlanes.Rows[listB_cargo_arenda_planes.SelectedIndex][6]) < Convert.ToInt32(CargoTakenReis.Rows[listB_takenCargo.SelectedIndex][6])) ||
                         (int.Parse(lb_budget.Text) < 0) ||
                         (Convert.ToInt32(ArendaCargoPlanes.Rows[listB_cargo_arenda_planes.SelectedIndex][7]) > Convert.ToInt32(ArendaCargoPlanes.Rows[listB_cargo_arenda_planes.SelectedIndex][8])) ||
                         (Convert.ToInt64(stopwatch.AddMinutes(minutes).Ticks) >= endar)) //конец ареды наступает раньше прилета

                    { log.Items.Add("Рейс не отправлен. Проверьте самолет или бюджет"); log.TopIndex = log.Items.Count - 1; }
                    else
                    {
                        log.Items.Add("Рейс успешно отправлен!");
                        log.TopIndex = log.Items.Count - 1;
                        SQLiteConnection sqlite;
                        sqlite = new SQLiteConnection(pathToDB);
                        sqlite.Open();
                        SQLiteCommand com = new SQLiteCommand();

                        com = new SQLiteCommand("delete from TakenCargoFlights where idCargoFlight = '" + CargoTakenReis.Rows[listB_takenCargo.SelectedIndex][0] + "'", sqlite);
                        com.ExecuteNonQuery();
                        com = new SQLiteCommand("insert into TimetableCargoFlights (comeTime, typeVladSam, idFlight, idSamAsc) values ('" + stopwatch.AddMinutes(minutes).Ticks + "', '1', '" + Convert.ToInt32(CargoTakenReis.Rows[listB_takenCargo.SelectedIndex][0]) + "', '" + Convert.ToInt32(ArendaCargoPlanes.Rows[listB_cargo_arenda_planes.SelectedIndex][0]) + "')", sqlite);
                        com.ExecuteNonQuery();
                        com = new SQLiteCommand("update ArendaPlanes set currentLocation = 'В полете' where id='" + ArendaCargoPlanes.Rows[listB_cargo_arenda_planes.SelectedIndex][0] + "'", sqlite);
                        com.ExecuteNonQuery();
                        com = new SQLiteCommand("update ArendaPlanes set vzlets = vzlets+1 where id='" + ArendaCargoPlanes.Rows[listB_cargo_arenda_planes.SelectedIndex][0] + "'", sqlite);
                        com.ExecuteNonQuery();
                        int dohod = Convert.ToInt32(CargoTakenReis.Rows[listB_takenCargo.SelectedIndex][7]);
                        log.Items.Add("Выплата: " + dohod.ToString());
                        log.TopIndex = log.Items.Count - 1;
                        lb_budget.Text = Convert.ToString(int.Parse(lb_budget.Text) + dohod);
                        refreshMyTimeTable();
                        CargoReisTakenRefresh();
                        refresh_my_Arendacargo_planes();
                        refreshNonWorkWorkers();
                        refreshWorkWorkers();
                    }

                }
                if (listB_cargo_leasing_planes.SelectedIndex > -1) //если зализингован грузовой
                {
                    DateTime endr = new DateTime(Convert.ToInt64(LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][9]));
                    long endar = Convert.ToInt64(endr.Ticks); //время окончания аренды 
                    double minutes = 60 * Convert.ToDouble(CargoTakenReis.Rows[listB_takenCargo.SelectedIndex][3]) / Convert.ToDouble(LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][4]);
                    minutes = Math.Floor(minutes); //количество минут полета
                    //добавили инфу в Таблицу расписания
                    if ((Convert.ToString(LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][2]) != Convert.ToString(CargoTakenReis.Rows[listB_takenCargo.SelectedIndex][1])) ||
                        (Convert.ToInt32(LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][3]) < Convert.ToInt32(CargoTakenReis.Rows[listB_takenCargo.SelectedIndex][3])) ||
                         (Convert.ToInt32(LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][6]) < Convert.ToInt32(CargoTakenReis.Rows[listB_takenCargo.SelectedIndex][6])) ||
                         (int.Parse(lb_budget.Text) < 0) ||
                         (Convert.ToInt32(LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][7]) > Convert.ToInt32(LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][8])) ||
                         (Convert.ToInt64(stopwatch.AddMinutes(minutes).Ticks) >= endar)) //конец ареды наступает раньше прилета

                    { log.Items.Add("Рейс не отправлен. Проверьте самолет или бюджет"); log.TopIndex = log.Items.Count - 1; }

                    else
                    {
                        log.Items.Add("Рейс успешно отправлен!");
                        log.TopIndex = log.Items.Count - 1;
                        SQLiteConnection sqlite;
                        sqlite = new SQLiteConnection(pathToDB);
                        sqlite.Open();
                        SQLiteCommand com = new SQLiteCommand();

                        com = new SQLiteCommand("delete from TakenCargoFlights where idCargoFlight = '" + CargoTakenReis.Rows[listB_takenCargo.SelectedIndex][0] + "'", sqlite);
                        com.ExecuteNonQuery();
                        com = new SQLiteCommand("insert into TimetableCargoFlights (comeTime, typeVladSam, idFlight, idSamAsc) values ('" + stopwatch.AddMinutes(minutes).Ticks + "', '3', '" + Convert.ToInt32(CargoTakenReis.Rows[listB_takenCargo.SelectedIndex][0]) + "', '" + Convert.ToInt32(LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][0]) + "')", sqlite);
                        com.ExecuteNonQuery();
                        com = new SQLiteCommand("update LeasingPlanes set currentLocation = 'В полете' where id='" + LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][0] + "'", sqlite);
                        com.ExecuteNonQuery();
                        com = new SQLiteCommand("update LeasingPlanes set vzlets = vzlets+1 where id='" + LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][0] + "'", sqlite);
                        com.ExecuteNonQuery();
                        int dohod = Convert.ToInt32(CargoTakenReis.Rows[listB_takenCargo.SelectedIndex][7]);
                        log.Items.Add("Выплата: " + dohod.ToString());
                        log.TopIndex = log.Items.Count - 1;
                        lb_budget.Text = Convert.ToString(int.Parse(lb_budget.Text) + dohod);
                        refreshMyTimeTable();
                        CargoReisTakenRefresh();
                        refresh_my_Leasingcargo_planes();
                        refreshNonWorkWorkers();
                        refreshWorkWorkers();
                    }
                }
            }
        }

        private void btn_del_Click(object sender, EventArgs e)
        {
            listB_reisInfo.Items.Clear();
            if (listB_takenPass.SelectedIndex>-1)
            {
                SQLiteConnection sqlite;
                sqlite = new SQLiteConnection(pathToDB);
                sqlite.Open();
                SQLiteCommand com = new SQLiteCommand();
                com = new SQLiteCommand("delete from TakenPassFlights where idPassFlight = '" + PassTakenReis.Rows[listB_takenPass.SelectedIndex][0] + "'", sqlite);
                com.ExecuteNonQuery();
                PassReisTakenRefresh();
            }
            if (listB_takenCargo.SelectedIndex > -1)
            {
                SQLiteConnection sqlite;
                sqlite = new SQLiteConnection(pathToDB);
                sqlite.Open();
                SQLiteCommand com = new SQLiteCommand();
                com = new SQLiteCommand("delete from TakenCargoFlights where idCargoFlight = '" + CargoTakenReis.Rows[listB_takenCargo.SelectedIndex][0] + "'", sqlite);
                com.ExecuteNonQuery();
                CargoReisTakenRefresh();
            }
        }

        private void btn_reklama_Click(object sender, EventArgs e)
        {
            lb_budget.Text = Convert.ToString(int.Parse(lb_budget.Text) - 15000);
            lb_imidg.Text = Convert.ToString(int.Parse(lb_imidg.Text) + 1000); //рекламное обновление имиджа
            log.Items.Add("Имидж улучшен! Из бюджета вычтено 15000");
            log.TopIndex = log.Items.Count - 1;
        }

        private void btn_antireklama_Click(object sender, EventArgs e)
        {
            log.Items.Add("Антиреклама конкурента заказана! Из бюджета вычтено 15000");
            log.TopIndex = log.Items.Count - 1;
            lb_budget.Text = Convert.ToString(int.Parse(lb_budget.Text) - 15000);
        }

        private void btn_zakaz_Click(object sender, EventArgs e)
        {
            log.Items.Add("Забастовка у конкурента заказана! Из бюджета вычтено 15000");
            log.TopIndex = log.Items.Count - 1;
            lb_budget.Text = Convert.ToString(int.Parse(lb_budget.Text) - 15000);
            lb_botZab.Text = "Да";
        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void btn_pustoy_Click(object sender, EventArgs e)
        {
            if ((listB_pass_buy_planes.SelectedIndex > -1) && (BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][2].ToString() != "В полете")&& ((BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][7].ToString() == BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][8].ToString())))
            {
                DlyaPustyh.SelfRef.loc.Text = BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][2].ToString();
                DlyaPustyh.SelfRef.maxDistance.Text = BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][3].ToString();
                DlyaPustyh.SelfRef.id.Text = BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][0].ToString();
                DlyaPustyh.SelfRef.vzlets.Text = BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][5].ToString();
                DlyaPustyh.SelfRef.speed.Text = BuyPassPlanes.Rows[listB_pass_buy_planes.SelectedIndex][4].ToString();
                DlyaPustyh.SelfRef.label1.Text = "Пассажирский";
                DlyaPustyh.SelfRef.label2.Text = "Купленный";
                DlyaPustyh.SelfRef.ShowDialog();
            }
            else if (listB_pass_buy_planes.SelectedIndex > -1)
            {
                log.Items.Add("Эту операцию совершить нельзя. Возможная причина - самолет не выбран или самолет в полете или не хватает экипажа");
                log.TopIndex = log.Items.Count - 1;
            }
            if ((listB_pass_arenda_planes.SelectedIndex > -1) && (ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][2].ToString() != "В полете") && ((ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][7].ToString() == ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][8].ToString())))
            {
                DlyaPustyh.SelfRef.loc.Text = ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][2].ToString();
                DlyaPustyh.SelfRef.maxDistance.Text = ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][3].ToString();
                DlyaPustyh.SelfRef.id.Text = ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][0].ToString();
                DlyaPustyh.SelfRef.vzlets.Text = ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][5].ToString();
                DlyaPustyh.SelfRef.speed.Text = ArendaPassPlanes.Rows[listB_pass_arenda_planes.SelectedIndex][4].ToString();
                DlyaPustyh.SelfRef.label1.Text = "Пассажирский";
                DlyaPustyh.SelfRef.label2.Text = "Арендованный";
                DlyaPustyh.SelfRef.ShowDialog();
            }
            else if (listB_pass_arenda_planes.SelectedIndex > -1)
            {
                log.Items.Add("Эту операцию совершить нельзя. Возможная причина - самолет не выбран или самолет в полете или не хватает экипажа");
                log.TopIndex = log.Items.Count - 1;
            }
            if ((listB_pass_leasing_planes.SelectedIndex > -1) && (LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][2].ToString() != "В полете") && ((LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][7].ToString() == LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][8].ToString())))
            {
                DlyaPustyh.SelfRef.loc.Text = LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][2].ToString();
                DlyaPustyh.SelfRef.maxDistance.Text = LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][3].ToString();
                DlyaPustyh.SelfRef.id.Text = LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][0].ToString();
                DlyaPustyh.SelfRef.vzlets.Text = LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][5].ToString();
                DlyaPustyh.SelfRef.speed.Text = LeasingPassPlanes.Rows[listB_pass_leasing_planes.SelectedIndex][4].ToString();
                DlyaPustyh.SelfRef.label1.Text = "Пассажирский";
                DlyaPustyh.SelfRef.label2.Text = "Лизингованный";
                DlyaPustyh.SelfRef.ShowDialog();
            }
            else if (listB_pass_leasing_planes.SelectedIndex > -1)
            {
                log.Items.Add("Эту операцию совершить нельзя. Возможная причина - самолет не выбран или самолет в полете или не хватает экипажа");
                log.TopIndex = log.Items.Count - 1;
            }
            if ((listB_cargo_buy_planes.SelectedIndex > -1) && (BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][2].ToString() != "В полете") && ((BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][7].ToString() == BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][8].ToString())))
            {
                DlyaPustyh.SelfRef.loc.Text = BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][2].ToString();
                DlyaPustyh.SelfRef.maxDistance.Text = BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][3].ToString();
                DlyaPustyh.SelfRef.id.Text = BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][0].ToString();
                DlyaPustyh.SelfRef.vzlets.Text = BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][5].ToString();
                DlyaPustyh.SelfRef.speed.Text = BuyCargoPlanes.Rows[listB_cargo_buy_planes.SelectedIndex][4].ToString();
                DlyaPustyh.SelfRef.label1.Text = "Грузовой";
                DlyaPustyh.SelfRef.label2.Text = "Купленный";
                DlyaPustyh.SelfRef.ShowDialog();
            }
            else if (listB_cargo_buy_planes.SelectedIndex > -1)
            {
                log.Items.Add("Эту операцию совершить нельзя. Возможная причина - самолет не выбран или самолет в полете или не хватает экипажа");
                log.TopIndex = log.Items.Count - 1;
            }
            if ((listB_cargo_arenda_planes.SelectedIndex > -1) && (ArendaCargoPlanes.Rows[listB_cargo_arenda_planes.SelectedIndex][2].ToString() != "В полете") && ((ArendaCargoPlanes.Rows[listB_cargo_arenda_planes.SelectedIndex][7].ToString() == ArendaCargoPlanes.Rows[listB_cargo_arenda_planes.SelectedIndex][8].ToString())))
            {
                DlyaPustyh.SelfRef.loc.Text = ArendaCargoPlanes.Rows[listB_cargo_arenda_planes.SelectedIndex][2].ToString();
                DlyaPustyh.SelfRef.maxDistance.Text = ArendaCargoPlanes.Rows[listB_cargo_arenda_planes.SelectedIndex][3].ToString();
                DlyaPustyh.SelfRef.id.Text = ArendaCargoPlanes.Rows[listB_cargo_arenda_planes.SelectedIndex][0].ToString();
                DlyaPustyh.SelfRef.vzlets.Text = ArendaCargoPlanes.Rows[listB_cargo_arenda_planes.SelectedIndex][5].ToString();
                DlyaPustyh.SelfRef.speed.Text = ArendaCargoPlanes.Rows[listB_cargo_arenda_planes.SelectedIndex][4].ToString();
                DlyaPustyh.SelfRef.label1.Text = "Грузовой";
                DlyaPustyh.SelfRef.label2.Text = "Арендованный";
                DlyaPustyh.SelfRef.ShowDialog();
            }
            else if (listB_cargo_arenda_planes.SelectedIndex > -1)
            {
                log.Items.Add("Эту операцию совершить нельзя. Возможная причина - самолет не выбран или самолет в полете или не хватает экипажа");
                log.TopIndex = log.Items.Count - 1;
            }
            if ((listB_cargo_leasing_planes.SelectedIndex > -1) && (LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][2].ToString() != "В полете") && ((LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][7].ToString() == LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][8].ToString())))
            {
                DlyaPustyh.SelfRef.loc.Text = LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][2].ToString();
                DlyaPustyh.SelfRef.maxDistance.Text = LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][3].ToString();
                DlyaPustyh.SelfRef.id.Text = LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][0].ToString();
                DlyaPustyh.SelfRef.vzlets.Text = LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][5].ToString();
                DlyaPustyh.SelfRef.speed.Text = LeasingCargoPlanes.Rows[listB_cargo_leasing_planes.SelectedIndex][4].ToString();
                DlyaPustyh.SelfRef.label1.Text = "Грузовой";
                DlyaPustyh.SelfRef.label2.Text = "Лизингованный";
                DlyaPustyh.SelfRef.ShowDialog();
            }
            else if (listB_cargo_leasing_planes.SelectedIndex > -1)
            {
                log.Items.Add("Эту операцию совершить нельзя. Возможная причина - самолет не выбран или самолет в полете или не хватает экипажа");
                log.TopIndex = log.Items.Count - 1;
            }
        }
        


   

    }
}
