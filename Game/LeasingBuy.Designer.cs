﻿namespace Game
{
    partial class LeasingBuy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_otkaz = new System.Windows.Forms.Button();
            this.btn_vykup = new System.Windows.Forms.Button();
            this.lb_info = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lb_Cost = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lb_type = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_otkaz
            // 
            this.btn_otkaz.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_otkaz.Location = new System.Drawing.Point(216, 130);
            this.btn_otkaz.Name = "btn_otkaz";
            this.btn_otkaz.Size = new System.Drawing.Size(171, 35);
            this.btn_otkaz.TabIndex = 0;
            this.btn_otkaz.Text = "Не выкупать";
            this.btn_otkaz.UseVisualStyleBackColor = true;
            this.btn_otkaz.Click += new System.EventHandler(this.btn_exit_Click);
            // 
            // btn_vykup
            // 
            this.btn_vykup.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_vykup.Location = new System.Drawing.Point(31, 130);
            this.btn_vykup.Name = "btn_vykup";
            this.btn_vykup.Size = new System.Drawing.Size(179, 35);
            this.btn_vykup.TabIndex = 1;
            this.btn_vykup.Text = "Выкупить";
            this.btn_vykup.UseVisualStyleBackColor = true;
            this.btn_vykup.Click += new System.EventHandler(this.btn_vykup_Click);
            // 
            // lb_info
            // 
            this.lb_info.AutoSize = true;
            this.lb_info.Location = new System.Drawing.Point(28, 16);
            this.lb_info.Name = "lb_info";
            this.lb_info.Size = new System.Drawing.Size(0, 13);
            this.lb_info.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(28, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Цена:";
            // 
            // lb_Cost
            // 
            this.lb_Cost.AutoSize = true;
            this.lb_Cost.Location = new System.Drawing.Point(92, 92);
            this.lb_Cost.Name = "lb_Cost";
            this.lb_Cost.Size = new System.Drawing.Size(0, 13);
            this.lb_Cost.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(28, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 16);
            this.label1.TabIndex = 5;
            this.label1.Text = "Тип";
            // 
            // lb_type
            // 
            this.lb_type.AutoSize = true;
            this.lb_type.Location = new System.Drawing.Point(92, 68);
            this.lb_type.Name = "lb_type";
            this.lb_type.Size = new System.Drawing.Size(0, 13);
            this.lb_type.TabIndex = 6;
            // 
            // LeasingBuy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(427, 187);
            this.ControlBox = false;
            this.Controls.Add(this.lb_type);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lb_Cost);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lb_info);
            this.Controls.Add(this.btn_vykup);
            this.Controls.Add(this.btn_otkaz);
            this.Name = "LeasingBuy";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "LeasingBuy";
            this.Load += new System.EventHandler(this.LeasingBuy_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_otkaz;
        private System.Windows.Forms.Button btn_vykup;
        public System.Windows.Forms.Label lb_info;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label lb_Cost;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label lb_type;
    }
}