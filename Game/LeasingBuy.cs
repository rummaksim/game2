﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Game
{
    public partial class LeasingBuy : Form
    {
        public static LeasingBuy SelfRef
        {
            get;
            set;
        }
        public LeasingBuy(Form form)
        {
            InitializeComponent();
            SelfRef = this;
        }

        private void LeasingBuy_Load(object sender, EventArgs e)
        {
            
        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            if (lb_type.Text == "Пассажирский")
                UserPlay.SelfRef.LeasingPassOtkaz();
            else
                UserPlay.SelfRef.LeasingCargoOtkaz();
            this.Close();
        }

        private void btn_vykup_Click(object sender, EventArgs e)
        {
            UserPlay.SelfRef.lb_budget.Text = Convert.ToString(int.Parse(UserPlay.SelfRef.lb_budget.Text) - int.Parse(lb_Cost.Text));
            if (lb_type.Text == "Пассажирский")
                UserPlay.SelfRef.LeasingPassAccept();
            else
                UserPlay.SelfRef.LeasingCargoAccept();
            this.Close();
        }
    }
}
