﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.IO;
using System.Reflection;
namespace Game
{
    public partial class Shop : Form
    {
        DataTable passPlanes = new DataTable();
        DataTable cargoPlanes = new DataTable();
        string pathToDB = "Data Source=" + string.Concat(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"\Planes.db");
        public static Shop SelfRef 
        { 
            get; 
            set; 
        }
        public Shop(Form form)
        {
            SelfRef = this;
            new BuySam(this);
            new ArendaSam(this);
            new LeasingSam(this);
            InitializeComponent();        
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            SQLiteDataAdapter com = new SQLiteDataAdapter("SELECT Planes.id, name, maxDistance, speed, serviceCost, probeg, capacity, needWorkers, purchasePrice FROM Planes, PassengerPlanes WHERE Planes.id=PassengerPlanes.idPlane", sqlite);
            com.Fill(passPlanes); //считали пассажирские самолеты из БД и занесли в таблицу passPlanes
            for (int i=0; i<passPlanes.Rows.Count; i++)
            {
                DataRow dr_passPlanes = passPlanes.Rows[i];
                listB_pass.Items.Add(dr_passPlanes["name"].ToString());                
            }
            com = new SQLiteDataAdapter("SELECT Planes.id, name, maxDistance, speed, serviceCost, probeg, capacity, needWorkers, purchasePrice FROM Planes, CargoPlanes WHERE Planes.id=CargoPlanes.idPlane", sqlite);
            com.Fill(cargoPlanes); //считали грузовые самолеты из БД и занесли в таблицу cargoPlanes
            for (int i = 0; i < cargoPlanes.Rows.Count; i++)
            {
                DataRow dr_cargoPlanes = cargoPlanes.Rows[i];
                listB_cargo.Items.Add(dr_cargoPlanes["name"].ToString());
            }
            sqlite.Close();
        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void listB_pass_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listB_pass.SelectedIndex>-1)
            {
                listB_cargo.ClearSelected();
                listB_info.Items.Clear(); 
                listB_info.Items.Add("Макс. дальность: " + passPlanes.Rows[listB_pass.SelectedIndex][2]);
                listB_info.Items.Add("Макс. скорость: " + passPlanes.Rows[listB_pass.SelectedIndex][3]);
                listB_info.Items.Add("Обслуживание/км: " + passPlanes.Rows[listB_pass.SelectedIndex][4]);
                listB_info.Items.Add("Пробег: " + passPlanes.Rows[listB_pass.SelectedIndex][5]);
                listB_info.Items.Add("Пассажировмест-ть: " + passPlanes.Rows[listB_pass.SelectedIndex][6]);
                listB_info.Items.Add("Нужно экипажа: " + passPlanes.Rows[listB_pass.SelectedIndex][7]);
                listB_info.Items.Add("Стоимость: " + passPlanes.Rows[listB_pass.SelectedIndex][8]);
            }
        }

        private void listB_cargo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listB_cargo.SelectedIndex > -1)
            {
                listB_pass.ClearSelected();
                listB_info.Items.Clear();
                listB_info.Items.Add("Макс. дальность: " + cargoPlanes.Rows[listB_cargo.SelectedIndex][2]);
                listB_info.Items.Add("Макс. скорость: " + cargoPlanes.Rows[listB_cargo.SelectedIndex][3]);
                listB_info.Items.Add("Обслуживание/км: " + cargoPlanes.Rows[listB_cargo.SelectedIndex][4]);
                listB_info.Items.Add("Пробег: " + cargoPlanes.Rows[listB_cargo.SelectedIndex][5]);
                listB_info.Items.Add("Грузоподъемность: " + cargoPlanes.Rows[listB_cargo.SelectedIndex][6]);
                listB_info.Items.Add("Нужно экипажа: " + cargoPlanes.Rows[listB_cargo.SelectedIndex][7]);
                listB_info.Items.Add("Стоимость: " + cargoPlanes.Rows[listB_cargo.SelectedIndex][8]);
            }
            
        }

        private void btn_buy_Click(object sender, EventArgs e)
        {
           
            if (listB_pass.SelectedIndex>-1)
            {
                if (int.Parse(UserPlay.SelfRef.lb_budget.Text) < Convert.ToInt32(passPlanes.Rows[listB_pass.SelectedIndex][8]))
                {
                    const string message = "Вам не хватает денег для совершения операции!";
                    const string caption = "Операция покупки отменена";
                    var result = MessageBox.Show(message, caption,
                                                 MessageBoxButtons.OK,
                                                 MessageBoxIcon.Error);
                }
                else
                {                   
                    BuySam.SelfRef.lb_id.Text = Convert.ToString(passPlanes.Rows[listB_pass.SelectedIndex][0]);
                    BuySam.SelfRef.lb_nameSam.Text = Convert.ToString(passPlanes.Rows[listB_pass.SelectedIndex][1]);
                    BuySam.SelfRef.lb_stoimostSam.Text = Convert.ToString(passPlanes.Rows[listB_pass.SelectedIndex][8]);
                    BuySam.SelfRef.lb_typeSam.Text = "Пассажирский";
                    BuySam.SelfRef.lb_probegSam.Text = Convert.ToString(passPlanes.Rows[listB_pass.SelectedIndex][5]);
                    BuySam.SelfRef.ShowDialog();
                }
            }
            if (listB_cargo.SelectedIndex > -1)
            {
                if (int.Parse(UserPlay.SelfRef.lb_budget.Text) < Convert.ToInt32(cargoPlanes.Rows[listB_cargo.SelectedIndex][8]))
                {
                    const string message = "Вам не хватает денег для совершения операции!";
                    const string caption = "Операция покупки отменена";
                    var result = MessageBox.Show(message, caption,
                                                 MessageBoxButtons.OK,
                                                 MessageBoxIcon.Error);
                }
                else
                {
                    BuySam.SelfRef.lb_id.Text = Convert.ToString(cargoPlanes.Rows[listB_cargo.SelectedIndex][0]);
                    BuySam.SelfRef.lb_nameSam.Text = Convert.ToString(cargoPlanes.Rows[listB_cargo.SelectedIndex][1]);
                    BuySam.SelfRef.lb_stoimostSam.Text = Convert.ToString(cargoPlanes.Rows[listB_cargo.SelectedIndex][8]);
                    BuySam.SelfRef.lb_typeSam.Text = "Грузовой";
                    BuySam.SelfRef.lb_probegSam.Text = Convert.ToString(cargoPlanes.Rows[listB_cargo.SelectedIndex][5]);
                    BuySam.SelfRef.ShowDialog();
                }
               
            }               
        }

        private void tb_arendaMonths_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && e.KeyChar != 8)
                e.Handled = true;
        }

        private void tb_leasingMonths_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && e.KeyChar != 8)
                e.Handled = true;
        }

        private void btn_arenda_Click(object sender, EventArgs e)
        {
            if ((listB_pass.SelectedIndex > -1)&&(tb_arendaMonths.Text!=""))
            {
                if (int.Parse(tb_arendaMonths.Text) > 0)
                {
                    if (int.Parse(UserPlay.SelfRef.lb_budget.Text) < Convert.ToInt32(passPlanes.Rows[listB_pass.SelectedIndex][8]) / 10 * int.Parse(tb_arendaMonths.Text))
                    {
                        const string message = "Вам не хватает денег для совершения операции!";
                        const string caption = "Операция покупки отменена";
                        var result = MessageBox.Show(message, caption,
                                                     MessageBoxButtons.OK,
                                                     MessageBoxIcon.Error);
                    }
                    else
                    {
                        ArendaSam.SelfRef.lb_id.Text = Convert.ToString(passPlanes.Rows[listB_pass.SelectedIndex][0]);
                        ArendaSam.SelfRef.lb_nameSam.Text = Convert.ToString(passPlanes.Rows[listB_pass.SelectedIndex][1]);
                        ArendaSam.SelfRef.lb_stoimostSam.Text = Convert.ToString(Convert.ToInt32(passPlanes.Rows[listB_pass.SelectedIndex][8]) / 10);
                        ArendaSam.SelfRef.lb_typeSam.Text = "Пассажирский";
                        ArendaSam.SelfRef.lb_probegSam.Text = Convert.ToString(passPlanes.Rows[listB_pass.SelectedIndex][5]);
                        ArendaSam.SelfRef.lb_arendaSam.Text = tb_arendaMonths.Text;
                        ArendaSam.SelfRef.ShowDialog();
                    }
                }
            }
            if ((listB_cargo.SelectedIndex > -1) && (tb_arendaMonths.Text != ""))
            {
                if (int.Parse(tb_arendaMonths.Text) > 0)
                {
                    if (int.Parse(UserPlay.SelfRef.lb_budget.Text) < Convert.ToInt32(cargoPlanes.Rows[listB_cargo.SelectedIndex][8]) / 10 * int.Parse(tb_arendaMonths.Text))
                    {
                        const string message = "Вам не хватает денег для совершения операции!";
                        const string caption = "Операция покупки отменена";
                        var result = MessageBox.Show(message, caption,
                                                     MessageBoxButtons.OK,
                                                     MessageBoxIcon.Error);
                    }
                    else
                    {
                        ArendaSam.SelfRef.lb_id.Text = Convert.ToString(cargoPlanes.Rows[listB_cargo.SelectedIndex][0]);
                        ArendaSam.SelfRef.lb_nameSam.Text = Convert.ToString(cargoPlanes.Rows[listB_cargo.SelectedIndex][1]);
                        ArendaSam.SelfRef.lb_stoimostSam.Text = Convert.ToString(Convert.ToInt32(cargoPlanes.Rows[listB_cargo.SelectedIndex][8]) / 10);
                        ArendaSam.SelfRef.lb_typeSam.Text = "Грузовой";
                        ArendaSam.SelfRef.lb_probegSam.Text = Convert.ToString(cargoPlanes.Rows[listB_cargo.SelectedIndex][5]);
                        ArendaSam.SelfRef.lb_arendaSam.Text = tb_arendaMonths.Text;
                        ArendaSam.SelfRef.ShowDialog();
                    }
                }
            }               
        }

        private void btn_leasing_Click(object sender, EventArgs e)
        {
            if ((listB_pass.SelectedIndex > -1) && (tb_leasingMonths.Text != ""))
            {
                if (int.Parse(tb_leasingMonths.Text) > 0)
                {
                    if (int.Parse(UserPlay.SelfRef.lb_budget.Text) < Convert.ToInt32(Math.Floor(Convert.ToDouble(passPlanes.Rows[listB_pass.SelectedIndex][8]) / 100) * int.Parse(tb_leasingMonths.Text)))
                    {
                        const string message = "Вам не хватает денег для совершения операции!";
                        const string caption = "Операция покупки отменена";
                        var result = MessageBox.Show(message, caption,
                                                     MessageBoxButtons.OK,
                                                     MessageBoxIcon.Error);
                    }
                    else
                    {
                        LeasingSam.SelfRef.lb_id.Text = Convert.ToString(passPlanes.Rows[listB_pass.SelectedIndex][0]);
                        LeasingSam.SelfRef.lb_nameSam.Text = Convert.ToString(passPlanes.Rows[listB_pass.SelectedIndex][1]);
                        LeasingSam.SelfRef.lb_stoimostSam.Text = Convert.ToString(Math.Floor(Convert.ToDouble(passPlanes.Rows[listB_pass.SelectedIndex][8]) / 100));
                        LeasingSam.SelfRef.lb_typeSam.Text = "Пассажирский";
                        LeasingSam.SelfRef.lb_probegSam.Text = Convert.ToString(passPlanes.Rows[listB_pass.SelectedIndex][5]);
                        LeasingSam.SelfRef.lb_leasingSam.Text = tb_leasingMonths.Text;
                        LeasingSam.SelfRef.lb_OstStoimost.Text = Convert.ToString(Convert.ToInt32(passPlanes.Rows[listB_pass.SelectedIndex][8]) - Convert.ToInt32(passPlanes.Rows[listB_pass.SelectedIndex][8]) / 100 * int.Parse(tb_leasingMonths.Text));
                        LeasingSam.SelfRef.lb_AllCost.Text = Convert.ToString(int.Parse(LeasingSam.SelfRef.lb_stoimostSam.Text) * int.Parse(tb_leasingMonths.Text));
                        LeasingSam.SelfRef.ShowDialog();
                    }
                }
            }
            if ((listB_cargo.SelectedIndex > -1) && (tb_leasingMonths.Text != ""))
            {
                if (int.Parse(tb_leasingMonths.Text) > 0)
                {
                    if (int.Parse(UserPlay.SelfRef.lb_budget.Text) < Convert.ToInt32(Math.Floor(Convert.ToDouble(cargoPlanes.Rows[listB_cargo.SelectedIndex][8]) / 100) * int.Parse(tb_leasingMonths.Text)))
                    {
                        const string message = "Вам не хватает денег для совершения операции!";
                        const string caption = "Операция покупки отменена";
                        var result = MessageBox.Show(message, caption,
                                                     MessageBoxButtons.OK,
                                                     MessageBoxIcon.Error);
                    }
                    else
                    {
                        LeasingSam.SelfRef.lb_id.Text = Convert.ToString(cargoPlanes.Rows[listB_cargo.SelectedIndex][0]);
                        LeasingSam.SelfRef.lb_nameSam.Text = Convert.ToString(cargoPlanes.Rows[listB_cargo.SelectedIndex][1]);
                        LeasingSam.SelfRef.lb_stoimostSam.Text = Convert.ToString(Math.Floor(Convert.ToDouble(cargoPlanes.Rows[listB_cargo.SelectedIndex][8]) / 100 ));
                        LeasingSam.SelfRef.lb_typeSam.Text = "Грузовой";
                        LeasingSam.SelfRef.lb_probegSam.Text = Convert.ToString(cargoPlanes.Rows[listB_cargo.SelectedIndex][5]);
                        LeasingSam.SelfRef.lb_leasingSam.Text = tb_leasingMonths.Text;
                        LeasingSam.SelfRef.lb_OstStoimost.Text = Convert.ToString(Convert.ToInt32(cargoPlanes.Rows[listB_cargo.SelectedIndex][8]) - Convert.ToInt32(cargoPlanes.Rows[listB_cargo.SelectedIndex][8]) / 100 * int.Parse(tb_leasingMonths.Text));
                        LeasingSam.SelfRef.lb_AllCost.Text = Convert.ToString(int.Parse(LeasingSam.SelfRef.lb_stoimostSam.Text) * int.Parse(tb_leasingMonths.Text));
                        LeasingSam.SelfRef.ShowDialog();
                    }
                }
            }               
        }

        private void tb_arendaMonths_TextChanged(object sender, EventArgs e)
        {
            tb_leasingMonths.Text = "";
        }

        private void tb_leasingMonths_TextChanged(object sender, EventArgs e)
        {
            tb_arendaMonths.Text = "";
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void Shop_Load(object sender, EventArgs e)
        {

        }
    }
}
