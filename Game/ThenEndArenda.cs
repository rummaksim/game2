﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Game
{
    public partial class ThenEndArenda : Form
    {
        public static ThenEndArenda SelfRef
        {
            get;
            set;
        }
        public ThenEndArenda(Form form)
        {
            InitializeComponent();
            SelfRef = this;
        }

        private void ThenEndArenda_Load(object sender, EventArgs e)
        {

        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            UserPlay.SelfRef.change_plusTime();
            this.Close();
        }
    }
}
