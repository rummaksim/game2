﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.IO;
using System.Reflection;
namespace Game
{
    public partial class zagruzka : Form
    {
        string pathToDB = "Data Source=" + string.Concat(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"\Planes.db");
        public zagruzka()
        {
            InitializeComponent();
            pictureBox1.Image = Properties.Resources.sam;
            progressBar1.Visible = false;
            progressBar1.Maximum = 999999;
            
            label1.BackColor = Color.Transparent;
            pictureBox1.BackColor = Color.Transparent;
            

        }

        private void zagruzka_Load(object sender, EventArgs e)
        {
 
        }

        private void btn_play_Click(object sender, EventArgs e)
        {
            label1.Visible = false;
            progressBar1.Visible = true;
            btn_exit.Visible = false;
            btn_play.Visible = false;
            btn_help.Visible = false;
            pictureBox1.Visible = false;
            btn_newgame.Visible = false;
            pictureBox2.Image = Properties.Resources.fon;
            pictureBox2.Refresh();
            tick_progressBar();
        }
        public void tick_progressBar()
        {
            for (int i = 0; i < 999999; i++)//999999
                progressBar1.Increment(1);
            this.Hide();
            UserPlay form_user_play = new UserPlay();
            form_user_play.Show();
            
        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_help_Click(object sender, EventArgs e)
        {
            const string message = "Гайд по игре записан на видео!";
            const string caption = "Помощь";
            var result = MessageBox.Show(message, caption,
                                         MessageBoxButtons.OK,
                                         MessageBoxIcon.Information);
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {
 
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void btn_newgame_Click(object sender, EventArgs e)
        {
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            SQLiteCommand com = new SQLiteCommand();
            com = new SQLiteCommand("delete from ArendaPlanes",sqlite);
            com.ExecuteNonQuery();
            com = new SQLiteCommand("delete from BuyPlanes", sqlite);
            com.ExecuteNonQuery();
            com = new SQLiteCommand("delete from LeasingPlanes", sqlite);
            com.ExecuteNonQuery();
            com = new SQLiteCommand("delete from TakenCargoFlights", sqlite);
            com.ExecuteNonQuery();
            com = new SQLiteCommand("delete from TakenPassFlights", sqlite);
            com.ExecuteNonQuery();
            com = new SQLiteCommand("delete from TimetableCargoFlights", sqlite);
            com.ExecuteNonQuery();
            com = new SQLiteCommand("delete from TimetablePassFlights", sqlite);
            com.ExecuteNonQuery();
            com = new SQLiteCommand("delete from TodayCargoFlights", sqlite);
            com.ExecuteNonQuery();
            com = new SQLiteCommand("delete from TodayPassFlights", sqlite);
            com.ExecuteNonQuery();
            com = new SQLiteCommand("delete from UseWorkers", sqlite);
            com.ExecuteNonQuery();
            com = new SQLiteCommand("update Workers set countWorkers=2146000000 where id =1", sqlite);
            com.ExecuteNonQuery();
            com = new SQLiteCommand("update Workers set countWorkers=20 where id =2", sqlite);
            com.ExecuteNonQuery();
            com = new SQLiteCommand("update Workers set countWorkers=10 where id =3", sqlite);
            com.ExecuteNonQuery();
            com = new SQLiteCommand("update Info set myBudget=2146000000", sqlite);
            com.ExecuteNonQuery();
            com = new SQLiteCommand("update Info set gameTime = 0", sqlite);
            com.ExecuteNonQuery();
            com = new SQLiteCommand("update Info set benzCost = 25", sqlite);
            com.ExecuteNonQuery();
            com = new SQLiteCommand("update Info set myImidg = 15000", sqlite);
            com.ExecuteNonQuery();
            com = new SQLiteCommand("update Info set botZabastovka = 'Нет'", sqlite);
            com.ExecuteNonQuery();
            com = new SQLiteCommand("update Info set myZabastovka = 'Нет'", sqlite);
            com.ExecuteNonQuery();
            com = new SQLiteCommand("update Planes set purchasePrice=changeCost*10", sqlite);
            com.ExecuteNonQuery();
            
            label1.Visible = false;
            progressBar1.Visible = true;
            btn_exit.Visible = false;
            btn_play.Visible = false;
            btn_help.Visible = false;
            pictureBox1.Visible = false;
            btn_newgame.Visible = false;
            pictureBox2.Image = Properties.Resources.fon;
            pictureBox2.Refresh();
            tick_progressBar();
            
        }
    }
}
