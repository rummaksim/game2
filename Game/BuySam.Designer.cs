﻿namespace Game
{
    partial class BuySam
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_buy = new System.Windows.Forms.Button();
            this.btn_exit = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lb_nameSam = new System.Windows.Forms.Label();
            this.lb_typeSam = new System.Windows.Forms.Label();
            this.lb_probegSam = new System.Windows.Forms.Label();
            this.lb_stoimostSam = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lb_id = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(44, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Самолет";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(44, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Тип";
            // 
            // btn_buy
            // 
            this.btn_buy.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_buy.Location = new System.Drawing.Point(47, 145);
            this.btn_buy.Name = "btn_buy";
            this.btn_buy.Size = new System.Drawing.Size(139, 31);
            this.btn_buy.TabIndex = 2;
            this.btn_buy.Text = "Подтвердить";
            this.btn_buy.UseVisualStyleBackColor = true;
            this.btn_buy.Click += new System.EventHandler(this.btn_buy_Click);
            // 
            // btn_exit
            // 
            this.btn_exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_exit.Location = new System.Drawing.Point(203, 145);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(128, 31);
            this.btn_exit.TabIndex = 3;
            this.btn_exit.Text = "Отмена";
            this.btn_exit.UseVisualStyleBackColor = true;
            this.btn_exit.Click += new System.EventHandler(this.btn_exit_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(44, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Пробег";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(44, 116);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 16);
            this.label4.TabIndex = 5;
            this.label4.Text = "Стоимость";
            // 
            // lb_nameSam
            // 
            this.lb_nameSam.AutoSize = true;
            this.lb_nameSam.Location = new System.Drawing.Point(226, 42);
            this.lb_nameSam.Name = "lb_nameSam";
            this.lb_nameSam.Size = new System.Drawing.Size(0, 13);
            this.lb_nameSam.TabIndex = 6;
            // 
            // lb_typeSam
            // 
            this.lb_typeSam.AutoSize = true;
            this.lb_typeSam.Location = new System.Drawing.Point(226, 65);
            this.lb_typeSam.Name = "lb_typeSam";
            this.lb_typeSam.Size = new System.Drawing.Size(0, 13);
            this.lb_typeSam.TabIndex = 7;
            // 
            // lb_probegSam
            // 
            this.lb_probegSam.AutoSize = true;
            this.lb_probegSam.Location = new System.Drawing.Point(226, 89);
            this.lb_probegSam.Name = "lb_probegSam";
            this.lb_probegSam.Size = new System.Drawing.Size(0, 13);
            this.lb_probegSam.TabIndex = 8;
            // 
            // lb_stoimostSam
            // 
            this.lb_stoimostSam.AutoSize = true;
            this.lb_stoimostSam.Location = new System.Drawing.Point(226, 116);
            this.lb_stoimostSam.Name = "lb_stoimostSam";
            this.lb_stoimostSam.Size = new System.Drawing.Size(0, 13);
            this.lb_stoimostSam.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(44, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(114, 16);
            this.label5.TabIndex = 10;
            this.label5.Text = "Идентификатор";
            // 
            // lb_id
            // 
            this.lb_id.AutoSize = true;
            this.lb_id.Location = new System.Drawing.Point(226, 18);
            this.lb_id.Name = "lb_id";
            this.lb_id.Size = new System.Drawing.Size(0, 13);
            this.lb_id.TabIndex = 11;
            // 
            // BuySam
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(382, 188);
            this.Controls.Add(this.lb_id);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lb_stoimostSam);
            this.Controls.Add(this.lb_probegSam);
            this.Controls.Add(this.lb_typeSam);
            this.Controls.Add(this.lb_nameSam);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btn_exit);
            this.Controls.Add(this.btn_buy);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "BuySam";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "BuySam";
            this.Load += new System.EventHandler(this.BuySam_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_buy;
        private System.Windows.Forms.Button btn_exit;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label lb_nameSam;
        public System.Windows.Forms.Label lb_typeSam;
        public System.Windows.Forms.Label lb_probegSam;
        public System.Windows.Forms.Label lb_stoimostSam;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label lb_id;
    }
}