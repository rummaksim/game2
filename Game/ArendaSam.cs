﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.IO;
using System.Reflection;

namespace Game
{
    public partial class ArendaSam : Form
    {
        string pathToDB = "Data Source=" + string.Concat(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"\Planes.db");
        public static ArendaSam SelfRef
        {
            get;
            set;
        }
        public ArendaSam(Form form)
        {
            InitializeComponent();
            SelfRef = this;
            
        }

        private void ArendaSam_Load(object sender, EventArgs e)
        {
            lb_AllCost.Text = Convert.ToString(int.Parse(lb_arendaSam.Text) * int.Parse(lb_stoimostSam.Text));
        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

   

        private void btn_buy_Click(object sender, EventArgs e)
        {
            UserPlay.SelfRef.lb_budget.Text = Convert.ToString(int.Parse(UserPlay.SelfRef.lb_budget.Text) - int.Parse(lb_stoimostSam.Text));//обновление игрового бюджета (не в БД)
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            SQLiteCommand com = new SQLiteCommand();
            if (lb_probegSam.Text == "Без пробега")
            {
                int ticks = int.Parse(lb_arendaSam.Text); //срок аренды
                string Endtime = UserPlay.SelfRef.EndArendaOrLeasing(ticks); //время окончания аренды          
                string NextPayment = UserPlay.SelfRef.NextPayArendaOrLeasing(); //время следующей оплаты аренды
                com = new SQLiteCommand("Insert into ArendaPlanes (currentLocation, dateEndArenda, idPlane, typeVladSam, vzlets) Values ('Пермь', '" + Endtime + "', '" + int.Parse(lb_id.Text) + "', '1', '0')", sqlite);
                com.ExecuteNonQuery();
                com = new SQLiteCommand("Select max(id) from ArendaPlanes", sqlite);
                int idAsc = Convert.ToInt32(com.ExecuteScalar()); //получили id последнего арендованного самолета
                com = new SQLiteCommand("select distinct needWorkers from PassengerPlanes, ArendaPlanes where ArendaPlanes.id= '" + idAsc + "' and (ArendaPlanes.idPlane=PassengerPlanes.idPlane)", sqlite);
                int needWorkers = Convert.ToInt32(com.ExecuteScalar());
                if (needWorkers == 0)
                {
                    com = new SQLiteCommand("select distinct needWorkers from CargoPlanes, ArendaPlanes where ArendaPlanes.id= '" + idAsc + "' and (ArendaPlanes.idPlane=CargoPlanes.idPlane)", sqlite);
                    needWorkers = Convert.ToInt32(com.ExecuteScalar());
                }
                for (int i = 0; i < needWorkers; i++)
                {
                    com = new SQLiteCommand("Insert into UseWorkers(typeVladSam, idSamASC, countWorkers) values ('1', '" + idAsc + "', '0')", sqlite);
                    com.ExecuteNonQuery();
                }
                com = new SQLiteCommand("Update Planes set purchasePrice = PurchasePrice + ChangeCost where id = '" + int.Parse(lb_id.Text) + "' ", sqlite);
                com.ExecuteNonQuery();
                UserPlay.SelfRef.lb_imidg.Text = Convert.ToString(int.Parse(UserPlay.SelfRef.lb_imidg.Text) + 100); //обновление имиджа без запроса
                UserPlay.SelfRef.refresh_my_Arendacargo_planes();
                UserPlay.SelfRef.refresh_my_Arendapass_planes();
            }
            else
            {
                int ticks = int.Parse(lb_arendaSam.Text); //срок аренды
                string Endtime = UserPlay.SelfRef.EndArendaOrLeasing(ticks); //время окончания аренды
                string NextPayment = UserPlay.SelfRef.NextPayArendaOrLeasing(); //время следующей оплаты аренды
                com = new SQLiteCommand("Insert into ArendaPlanes (currentLocation, dateEndArenda, idPlane, typeVladSam, vzlets) Values ('Пермь', '" + Endtime + "', '" + int.Parse(lb_id.Text) + "', '1', '100')", sqlite);
                com.ExecuteNonQuery();
                com = new SQLiteCommand("Select max(id) from ArendaPlanes", sqlite);
                int idAsc = Convert.ToInt32(com.ExecuteScalar()); //получили id последнего арендованного самолета
                com = new SQLiteCommand("select distinct needWorkers from PassengerPlanes, ArendaPlanes where ArendaPlanes.id= '" + idAsc + "' and (ArendaPlanes.idPlane=PassengerPlanes.idPlane)", sqlite);
                int needWorkers = Convert.ToInt32(com.ExecuteScalar());
                if (needWorkers == 0)
                {
                    com = new SQLiteCommand("select distinct needWorkers from CargoPlanes, ArendaPlanes where ArendaPlanes.id= '" + idAsc + "' and (ArendaPlanes.idPlane=CargoPlanes.idPlane)", sqlite);
                    needWorkers = Convert.ToInt32(com.ExecuteScalar());
                }
                for (int i = 0; i < needWorkers; i++)
                {
                    com = new SQLiteCommand("Insert into UseWorkers(typeVladSam, idSamASC, countWorkers) values ('1', '" + idAsc + "', '0')", sqlite);
                    com.ExecuteNonQuery();
                }
                com = new SQLiteCommand("Update Planes set purchasePrice = PurchasePrice + ChangeCost where id = '" + int.Parse(lb_id.Text) + "' ", sqlite);
                com.ExecuteNonQuery();
                UserPlay.SelfRef.lb_imidg.Text = Convert.ToString(int.Parse(UserPlay.SelfRef.lb_imidg.Text) - 100); //обновление имиджа без запроса
                UserPlay.SelfRef.refresh_my_Arendacargo_planes();
                UserPlay.SelfRef.refresh_my_Arendapass_planes();
            }
            sqlite.Close();
            this.Close();
            Shop.SelfRef.Close();
        }


    }
}
